<%@page import="com.safetyequipment.beans.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.safetyequipment.beans.Company"%>
<%@page import="com.safetyequipment.beans.SubCategory"%>
<%@page import="com.safetyequipment.beans.Category"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
    <!-- begin::Head -->
    
<!-- Mirrored from keenthemes.com/metronic/preview/demo11/crud/forms/controls/base.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Oct 2019 04:54:58 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<%
		Product product = (Product) (request.getAttribute("product"));
	%>
	<%
		List<Category> categorylist = (ArrayList) (request.getAttribute("categorylist"));
	%>
	<%-- <% List<SubCategory> subcategorylist = (ArrayList)(request.getAttribute("subcategorylist"));  %> --%>
	<%
		List<Company> companylist = (ArrayList) (request.getAttribute("companylist"));
	%>
        <meta charset="utf-8"/>

        <title>Dipen Safety | Edit Product</title>
        <meta name="description" content="Base form control examples">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  		<link rel="icon" href="images/logo.png" type="image/x-icon">


        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Asap+Condensed:500">        <!--end::Fonts -->
		  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  		  <link rel="icon" href="images/logo.png" type="image/x-icon">

        
        
        <!--begin::Global Theme Styles(used by all pages) -->
                    <link href="css/plugins.bundle.css" rel="stylesheet" type="text/css" />
                    <link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
                <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->
                <!--end::Layout Skins -->

        <link rel="shortcut icon" href="https://keenthemes.com/metronic/themes/metronic/theme/default/demo11/dist/assets/media/logos/favicon.ico" />

        <!-- Hotjar Tracking Code for keenthemes.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1070954,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-37564768-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-37564768-1');
</script>    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body  class="kt-page-content-white kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading"  >

       
    	<!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed " >
	<div class="kt-header-mobile__logo">
		<a href="../../../index.html">
			<img alt="Logo" src="../../../../../themes/metronic/theme/default/demo11/dist/assets/media/logos/logo-11-sm.png"/>
		</a>
	</div>
	<div class="kt-header-mobile__toolbar">
					<button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		
		<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
	</div>
</div>
<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				<!-- begin:: Header -->
<%@include file="dashboardheader.jsp" %>
<!-- end:: Header -->
				<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
											<div class="kt-container  kt-container--fluid  kt-grid kt-grid--ver">
							<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

<!-- end:: Aside -->							
<%@include file="navigation.jsp" %>				
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">					
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">

<!--                             <span class="kt-subheader__separator kt-hidden"></span> -->
<!--                 <div class="kt-subheader__breadcrumbs"> -->
<!--                     <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a> -->
<!--                                             <span class="kt-subheader__breadcrumbs-separator"></span> -->
<!--                         <a href="#" class="kt-subheader__breadcrumbs-link"> -->
<!--                             Forms                        </a> -->
<!--                                             <span class="kt-subheader__breadcrumbs-separator"></span> -->
<!--                         <a href="#" class="kt-subheader__breadcrumbs-link"> -->
<!--                             Form Controls                        </a> -->
<!--                                             <span class="kt-subheader__breadcrumbs-separator"></span> -->
<!--                         <a href="#" class="kt-subheader__breadcrumbs-link"> -->
<!--                             Base Inputs                        </a> -->
<!--                                         <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
<!--                 </div> -->
                    </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
<!--                                     <a href="#" class="btn kt-subheader__btn-primary"> -->
<!--                         Actions &nbsp; -->
<!--                         <i class="flaticon2-calendar-1"></i> -->
<!--                     </a> -->
                
                <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
                    <a href="#" class="btn btn-icon"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
<!--     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> -->
<!--         <polygon points="0 0 24 0 24 24 0 24"/> -->
<!--         <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/> -->
<!--         <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"/> -->
<!--     </g> -->
</svg>                        <!--<i class="flaticon2-plus"></i>-->
                    </a>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
                        <!--begin::Nav-->
                        <ul class="kt-nav">
                            <li class="kt-nav__head">
                                Add anything or jump to:
                                <i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
                            </li>
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-drop"></i>
                                    <span class="kt-nav__link-text">Order</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
                                    <span class="kt-nav__link-text">Ticket</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
                                    <span class="kt-nav__link-text">Goal</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="#" class="kt-nav__link">
                                    <i class="kt-nav__link-icon flaticon2-new-email"></i>
                                    <span class="kt-nav__link-text">Support Case</span>
                                    <span class="kt-nav__link-badge">
                                        <span class="kt-badge kt-badge--success">5</span>
                                    </span>
                                </a>
                            </li>
                            <li class="kt-nav__separator"></li>
                            <li class="kt-nav__foot">
                                <a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
                                <a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
                            </li>
                        </ul>
                        <!--end::Nav-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-left: 200px;">
		<div class="row">
	<div class="col-md-6">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit Product
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form action="UpdateProductServlet"
							method="post" onsubmit="return epformvalidation()"
							enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<div class="form-group form-group-last">
					</div>

<!-- 						<label for="exampleSelectd">Default Select</label> -->

					
						<div class="form-group">
								<input type="hidden" name="id"
									value="<%=product.getProductid()%>"> 
									<label>Category :</label>
									 <select class="form-control" name="categoryoption" onchange="getval(this);" id="categoryoption">
									<option value="0" <%if (product.getCategoryid() == 0) {%>
										selected="selected" <%}%>>Select Category</option>
									<%
										for (Category category : categorylist) {
									%>
									<option value="<%=category.getCategoryid()%>"
										<%if (product.getCategoryid() == category.getCategoryid()) {%>
										selected="selected" <%}%>><%=category.getCategoryname()%></option>
									<%
										}
									%>
								</select><span id="category_option" style="color:red;"></span>
							</div>


							<div class="form-group"
								>
								<label>Sub Category :</label> 
								<select class="form-control" name="subcategoryoption" id="subcategoryoption" onchange="subcategoryvalidation()">
									<option value="0" <%if (product.getSubcategoryid() == 0) {%>
										selected="selected" <%}%>>Select Sub Category</option>
									<option value="<%=product.getSubcategoryid()%>"
										selected="selected"><%=product.getSubcategoryname()%></option>
								</select> <span id="subcategory_option" style="color:red;"></span>
							</div>

<!-- 							<div class="loader" id="loading" -->
<!-- 								style="position: fixed; top: 50%; left: 50%; display: none;"></div> -->
							<!--             style="display:none;" -->
							<div class="form-group">
								<label>Company :</label> 
								<select class="form-control" name="companyoption" id="companyoption" onchange="companyvalidation()">
									<option value="0" <%if (product.getCompanyid() == 0) {%>
										selected="selected" <%}%>>Select Company</option>
									<%
										for (Company company : companylist) {
									%>
									<option value="<%=company.getCompanyid()%>"
										<%if (product.getCompanyid() == company.getCompanyid()) {%>
										selected="selected" <%}%>><%=company.getCompanyname()%></option>
									<%
										}
									%>
								</select><span id="company_option" style="color:red;"></span>
							</div>


							<div class="form-group">
								<label>Product name :</label> 
								<input type="text" class="form-control"
									value="<%=product.getProductname()%>"
									placeholder="Product Name" id="productname" name="productname"
									onchange="return productnamevalidation()">
								<span id="product_name" style="color:red;"></span>
							</div>

							<div class="form-group">
								<input type="file" class="form-control" 
									accept=".png, .jpg, .jpeg" placeholder="Product  Image"
									id="productimage" name="productimage"> <img
									src="data:image/jpeg;base64,<%=product.getImagestring()%>"
									height="100" width="100">
							</div>


							<div class="form-group">
							<label>Features :</label> 
								<textarea  class="form-control"
									value="<%=product.getDescription()%>" placeholder="Features"
									id="productdescription" name="productdescription" onchange="return descriptionvalidation()">
									</textarea><span id="product_description" style="color:red;"></span>
							</div>


							<div class="form-group">
								<label>Description :</label> 
								<textarea  class="form-control"
									value="<%=product.getAboutproduct()%>"
									placeholder="Product Description" id="aboutproduct"
									name="aboutproduct" onchange="return aboutproductvalidation()">
									</textarea><span id="about_product" style="color:red;"></span>
							</div>

							

							<div class="form-group">
		<label>Quantity :</label> 
								<input type="text" class="form-control"
									onkeypress="javascript:return isNumber(event)"
									value="<%=product.getQuantity()%>"
									placeholder="Product Quantity" id="productquantity"
									name="productquantity" onchange="quantityvalidation()">
									<span id="product_quantity" style="color:red;"></span>
							</div>

							<div class="form-group">
						<label>Price :</label> 
								<input type="text" class="form-control"
									onkeypress="javascript:return isNumber(event)"
									value="<%=product.getProductprice()%>"
									placeholder="Product Price" id="productprice"
									name="productprice" onchange="pricevalidation()">
									<span id="product_price" style="color:red;"></span>
							</div>

							<div class="text-center">
								<button type="submit" id="epsubmit" class="btn btn-primary">Save Changes</button>
							</div>	
					
					
									</form>
			<!--end::Form-->			
		</div>
		<!--end::Portlet-->

		<!--begin::Portlet-->
<!-- 		<div class="kt-portlet"> -->
<!-- 			<div class="kt-portlet__head"> -->
<!-- 				<div class="kt-portlet__head-label"> -->
<!-- 					<h3 class="kt-portlet__head-title"> -->
<!-- 						Textual HTML5 Inputs -->
<!-- 					</h3> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<!--begin::Form--> 
<!-- 			<form class="kt-form kt-form--label-right"> -->
<!-- 				<div class="kt-portlet__body"> -->
<!-- 					<div class="form-group form-group-last"> -->
<!-- 						<div class="alert alert-secondary" role="alert"> -->
<!-- 							<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div> -->
<!-- 						  	<div class="alert-text"> -->
<!-- 								Here are examples of <code>.form-control</code> applied to each textual HTML5 input type: -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-text-input" class="col-2 col-form-label">Text</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="text" value="Artisanal kale" id="example-text-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-search-input" class="col-2 col-form-label">Search</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="search" value="How do I shoot web" id="example-search-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-email-input" class="col-2 col-form-label">Email</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="email" value="bootstrap@example.com" id="example-email-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-url-input" class="col-2 col-form-label">URL</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="url" value="https://getbootstrap.com" id="example-url-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-tel-input" class="col-2 col-form-label">Telephone</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="tel" value="1-(555)-555-5555" id="example-tel-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-password-input" class="col-2 col-form-label">Password</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="password" value="hunter2" id="example-password-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-number-input" class="col-2 col-form-label">Number</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="number" value="42" id="example-number-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-datetime-local-input" class="col-2 col-form-label">Date and time</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-date-input" class="col-2 col-form-label">Date</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="date" value="2011-08-19" id="example-date-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-month-input" class="col-2 col-form-label">Month</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="month" value="2011-08" id="example-month-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-week-input" class="col-2 col-form-label">Week</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="week" value="2011-W33" id="example-week-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-time-input" class="col-2 col-form-label">Time</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="time" value="13:45:00" id="example-time-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-color-input" class="col-2 col-form-label">Color</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="color" value="#563d7c" id="example-color-input"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group row"> -->
<!-- 						<label for="example-email-input" class="col-2 col-form-label">Range</label> -->
<!-- 						<div class="col-10"> -->
<!-- 							<input class="form-control" type="range"> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="kt-portlet__foot"> -->
<!-- 					<div class="kt-form__actions"> -->
<!-- 						<div class="row"> -->
<!-- 							<div class="col-2"> -->
<!-- 							</div> -->
<!-- 							<div class="col-10"> -->
<!-- 								<button type="reset" class="btn btn-success">Submit</button> -->
<!-- 								<button type="reset" class="btn btn-secondary">Cancel</button> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</form> -->
<!-- 		</div> -->
		<!--end::Portlet-->
	</div>
<!-- 	<div class="col-md-6"> -->
<!-- 		<!--begin::Portlet--> 
<!-- 		<div class="kt-portlet"> -->
<!-- 			<div class="kt-portlet__head"> -->
<!-- 				<div class="kt-portlet__head-label"> -->
<!-- 					<h3 class="kt-portlet__head-title"> -->
<!-- 						Input States -->
<!-- 					</h3> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<!--begin::Form--> 
<!-- 			<form class="kt-form"> -->
<!-- 				<div class="kt-portlet__body"> -->
<!-- 					<div class="form-group form-group-last"> -->
<!-- 						<div class="alert alert-secondary" role="alert"> -->
<!-- 							<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div> -->
<!-- 						  	<div class="alert-text"> -->
<!-- 								Add the disabled or readonly boolean attribute on an input to prevent user interactions.  -->
<!-- 								Disabled inputs appear lighter and add a <code>not-allowed</code> cursor. -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Disabled Input</label> -->
<!-- 						<input type="email" class="form-control" disabled="disabled" placeholder="Disabled input"> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Disabled select</label> -->
<!-- 						<select class="form-control" disabled="disabled"> -->
<!-- 							<option>1</option> -->
<!-- 							<option>2</option> -->
<!-- 							<option>3</option> -->
<!-- 							<option>4</option> -->
<!-- 							<option>5</option> -->
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label for="exampleTextarea">Disabled textarea</label> -->
<!-- 						<textarea class="form-control" disabled="disabled" rows="3"></textarea> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Readonly Input</label> -->
<!-- 						<input type="email" class="form-control" readonly placeholder="Readonly input"> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label for="exampleTextarea">Readonly textarea</label> -->
<!-- 						<textarea class="form-control" readonly rows="3"></textarea> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="kt-portlet__foot"> -->
<!-- 					<div class="kt-form__actions"> -->
<!-- 						<button type="reset" class="btn btn-brand">Submit</button> -->
<!-- 						<button type="reset" class="btn btn-secondary">Cancel</button> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</form> -->
<!-- 			<!--end::Form-->			 
<!-- 		</div> -->
<!-- 		<!--end::Portlet--> 

<!-- 		<!--begin::Portlet--> 
<!-- 		<div class="kt-portlet"> -->
<!-- 			<div class="kt-portlet__head"> -->
<!-- 				<div class="kt-portlet__head-label"> -->
<!-- 					<h3 class="kt-portlet__head-title"> -->
<!-- 						Input Sizing -->
<!-- 					</h3> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<!--begin::Form--> 
<!-- 			<form class="kt-form"> -->
<!-- 				<div class="kt-portlet__body"> -->
<!-- 					<div class="form-group form-group-last"> -->
<!-- 						<div class="alert alert-secondary" role="alert"> -->
<!-- 							<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div> -->
<!-- 						  	<div class="alert-text"> -->
<!-- 								Set heights using classes like <code>.form-control-lg</code>, and set widths using grid column classes like <code>.col-lg-*</code> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Large Input</label> -->
<!-- 						<input type="email" class="form-control form-control-lg" aria-describedby="emailHelp" placeholder="Large input"> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Default Input</label> -->
<!-- 						<input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Large input"> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Small Input</label> -->
<!-- 						<input type="email" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="Large input"> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label for="exampleSelectl">Large Select</label> -->
<!-- 						<select class="form-control form-control-lg" id="exampleSelectl"> -->
<!-- 							<option>1</option> -->
<!-- 							<option>2</option> -->
<!-- 							<option>3</option> -->
<!-- 							<option>4</option> -->
<!-- 							<option>5</option> -->
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label for="exampleSelectd">Default Select</label> -->
<!-- 						<select class="form-control" id="exampleSelectd"> -->
<!-- 							<option>1</option> -->
<!-- 							<option>2</option> -->
<!-- 							<option>3</option> -->
<!-- 							<option>4</option> -->
<!-- 							<option>5</option> -->
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label for="exampleSelects">Small Select</label> -->
<!-- 						<select class="form-control form-control-sm" id="exampleSelects"> -->
<!-- 							<option>1</option> -->
<!-- 							<option>2</option> -->
<!-- 							<option>3</option> -->
<!-- 							<option>4</option> -->
<!-- 							<option>5</option> -->
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="kt-portlet__foot"> -->
<!-- 					<div class="kt-form__actions"> -->
<!-- 						<button type="reset" class="btn btn-success">Submit</button> -->
<!-- 						<button type="reset" class="btn btn-secondary">Cancel</button> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</form> -->
<!-- 			<!--end::Form-->			 
<!-- 		</div> -->
<!-- 		<!--end::Portlet--> 

<!-- 		<!--begin::Portlet--> 
<!-- 		<div class="kt-portlet"> -->
<!-- 			<div class="kt-portlet__head"> -->
<!-- 				<div class="kt-portlet__head-label"> -->
<!-- 					<h3 class="kt-portlet__head-title"> -->
<!-- 						Custom Controls -->
<!-- 					</h3> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<!--begin::Form--> 
<!-- 			<form class="kt-form"> -->
<!-- 				<div class="kt-portlet__body"> -->
<!-- 					<div class="form-group form-group-last"> -->
<!-- 						<div class="alert alert-secondary" role="alert"> -->
<!-- 							<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div> -->
<!-- 						  	<div class="alert-text"> -->
<!-- 								For even more customization and cross browser consistency, use our completely custom form elements to replace the browser defaults. They’re built on top of semantic and accessible markup, so they’re solid replacements for any default form control. -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Custom Range</label> -->
<!-- 						<div></div> -->
<!-- 						<input type="range" class="custom-range" min="0" max="5" id="customRange2"> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>Custom Select</label> -->
<!-- 						<div></div> -->
<!-- 						<select class="custom-select form-control"> -->
<!-- 						  	<option selected>Open this select menu</option> -->
<!-- 						  	<option value="1">One</option> -->
<!-- 						  	<option value="2">Two</option> -->
<!-- 						  	<option value="3">Three</option> -->
<!-- 						</select> -->
<!-- 					</div> -->
<!-- 					<div class="form-group"> -->
<!-- 						<label>File Browser</label> -->
<!-- 						<div></div> -->
<!-- 						<div class="custom-file"> -->
<!-- 						  	<input type="file" class="custom-file-input" id="customFile"> -->
<!-- 						  	<label class="custom-file-label" for="customFile">Choose file</label> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				<div class="kt-portlet__foot"> -->
<!-- 					<div class="kt-form__actions"> -->
<!-- 						<button type="reset" class="btn btn-primary">Submit</button> -->
<!-- 						<button type="reset" class="btn btn-secondary">Cancel</button> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</form> -->
<!-- 			<!--end::Form-->			 
<!-- 		</div> -->
<!-- 		<!--end::Portlet--> 
<!-- 	</div> -->
</div>	</div>
<!-- end:: Content -->

							</div>
						</div>
									</div>

<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item" id="kt_footer">	
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-footer__wrapper">
<!-- 			<div class="kt-footer__copyright"> -->
<!-- 				2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a> -->
<!-- 			</div> -->
<!-- 			<div class="kt-footer__menu"> -->
<!-- 				<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">About</a> -->
<!-- 				<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Team</a> -->
<!-- 				<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Contact</a> -->
<!-- 			</div> -->
		</div>
	</div>		
</div>
<!-- end:: Footer -->							</div>
		</div>
	</div>
	
<!-- end:: Page -->

            <!-- begin::Quick Panel -->
<div id="kt_quick_panel" class="kt-quick-panel">
    <a href="#" class="kt-quick-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></a>

    <div class="kt-quick-panel__nav">
        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
            <li class="nav-item active">
                <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_logs" role="tab">Audit Logs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
            </li>
        </ul>
    </div>

    <div class="kt-quick-panel__content">
        <div class="tab-content">
            <div class="tab-pane fade show kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
                <div class="kt-notification">
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-line-chart kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New order has been received
                            </div>
                            <div class="kt-notification__item-time">
                                2 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-box-1 kt-font-brand"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New customer is registered
                            </div>
                            <div class="kt-notification__item-time">
                                3 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-chart2 kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                Application has been approved
                            </div>
                            <div class="kt-notification__item-time">
                                3 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-image-file kt-font-warning"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New file has been uploaded
                            </div>
                            <div class="kt-notification__item-time">
                                5 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-drop kt-font-info"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New user feedback received
                            </div>
                            <div class="kt-notification__item-time">
                                8 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-pie-chart-2 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                System reboot has been successfully completed
                            </div>
                            <div class="kt-notification__item-time">
                                12 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-favourite kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New order has been placed
                            </div>
                            <div class="kt-notification__item-time">
                                15 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item kt-notification__item--read">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-safe kt-font-primary"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                Company meeting canceled
                            </div>
                            <div class="kt-notification__item-time">
                                19 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-psd kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New report has been received
                            </div>
                            <div class="kt-notification__item-time">
                                23 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-download-1 kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                Finance report has been generated
                            </div>
                            <div class="kt-notification__item-time">
                                25 hrs ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-security kt-font-warning"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New customer comment recieved
                            </div>
                            <div class="kt-notification__item-time">
                                2 days ago
                            </div>
                        </div>
                    </a>
                    <a href="#" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-pie-chart kt-font-warning"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title">
                                New customer is registered
                            </div>
                            <div class="kt-notification__item-time">
                                3 days ago
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade kt-scroll" id="kt_quick_panel_tab_logs" role="tabpanel">
                <div class="kt-notification-v2">
                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon-bell kt-font-brand"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                5 new user generated report
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                Reports based on sales
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon2-box kt-font-danger"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                2 new items submited
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                by Grog John
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon-psd kt-font-brand"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                79 PSD files generated
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                Reports based on sales
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon2-supermarket kt-font-warning"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                $2900 worth producucts sold
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                Total 234 items
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon-paper-plane-1 kt-font-success"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                4.5h-avarage response time
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                Fostest is Barry
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon2-information kt-font-danger"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                Database server is down
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                10 mins ago
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon2-mail-1 kt-font-brand"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                System report has been generated
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                Fostest is Barry
                            </div>
                        </div>
                    </a>

                    <a href="#" class="kt-notification-v2__item">
                        <div class="kt-notification-v2__item-icon">
                            <i class="flaticon2-hangouts-logo kt-font-warning"></i>
                        </div>
                        <div class="kt-notification-v2__itek-wrapper">
                            <div class="kt-notification-v2__item-title">
                                4.5h-avarage response time
                            </div>
                            <div class="kt-notification-v2__item-desc">
                                Fostest is Barry
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="tab-pane kt-quick-panel__content-padding-x fade kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
                <form class="kt-form">
                    <div class="kt-heading kt-heading--sm kt-heading--space-sm">Customer Care</div>

                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable Notifications:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--success kt-switch--sm">
								<label>
									<input type="checkbox" checked="checked" name="quick_panel_notifications_1">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable Case Tracking:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--success kt-switch--sm">
								<label>
									<input type="checkbox"  name="quick_panel_notifications_2">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-last form-group-xs row">
                        <label class="col-8 col-form-label">Support Portal:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--success kt-switch--sm">
								<label>
									<input type="checkbox" checked="checked" name="quick_panel_notifications_2">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>

                    <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                    <div class="kt-heading kt-heading--sm kt-heading--space-sm">Reports</div>

                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Generate Reports:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--sm kt-switch--danger">
								<label>
									<input type="checkbox" checked="checked" name="quick_panel_notifications_3">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable Report Export:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--sm kt-switch--danger">
								<label>
									<input type="checkbox"  name="quick_panel_notifications_3">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-last form-group-xs row">
                        <label class="col-8 col-form-label">Allow Data Collection:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--sm kt-switch--danger">
								<label>
									<input type="checkbox" checked="checked" name="quick_panel_notifications_4">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>

                    <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                    <div class="kt-heading kt-heading--sm kt-heading--space-sm">Memebers</div>

                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable Member singup:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--sm kt-switch--brand">
								<label>
									<input type="checkbox" checked="checked" name="quick_panel_notifications_5">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Allow User Feedbacks:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--sm kt-switch--brand">
								<label>
									<input type="checkbox"  name="quick_panel_notifications_5">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-group-last form-group-xs row">
                        <label class="col-8 col-form-label">Enable Customer Portal:</label>
                        <div class="col-4 kt-align-right">
                            <span class="kt-switch kt-switch--sm kt-switch--brand">
								<label>
									<input type="checkbox" checked="checked" name="quick_panel_notifications_6">
									<span></span>
                            </label>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end::Quick Panel -->
    
    <!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
</div>

<!--Begin:: Chat-->
<!--ENd:: Chat-->

<script src="js/script.js"></script>
<script type="text/javascript">
		function getval(categoryoption) {
			alert(categoryoption.value);
			if (categoryoption.value == "0") {
				document.getElementById('category_option').innerHTML = "*Please select  category";
				$('#subcategoryoption').find('option').remove().end().append(
						'<option value="0">Select Sub  Category</option>').val(
						'0');
			} else {
				$
						.get(
								"SubCategoryMasterData",
								{
									categoryId : categoryoption.value
								},
								function(data) {
									$('#subcategoryoption')
											.find('option')
											.remove()
											.end()
											.append(
													'<option value="0">Select Sub Category</option>')
											.val('0');

									$.each(JSON.parse(data), function(i, obj) {
										//use obj.id and obj.name here, for example:
										//              	   alert(obj.subcategoryid + "-" + obj.subcategoryname);   
										//             	   alert(obj);
										$("#subcategoryoption").append(
												new Option(obj.subcategoryname,
														obj.subcategoryid));
									});
									$("#category_option").text("");
									$('#epsubmit').attr("disabled", false);
								});
			}
		}

		function subcategoryvalidation() {
			var subcategory = document.getElementById('subcategoryoption').value;
			if (subcategory == "0") {
				document.getElementById('subcategory_option').innerHTML = "*Please select sub category";
				//$('#ascsubmit').attr("disabled", true);
			} else {
				document.getElementById('subcategory_option').innerHTML = "";
				$('#epsubmit').attr("disabled", false);
			}
		}

		function companyvalidation() {
			var company = document.getElementById('companyoption').value;
			if (company == "0") {
				document.getElementById('company_option').innerHTML = "*Please select company";
				//$('#ascsubmit').attr("disabled", true);
			} else {
				document.getElementById('company_option').innerHTML = "";
				$('#epsubmit').attr("disabled", false);
			}
		}

		function productnamevalidation() {
			var productname = document.getElementById('productname').value;
			var pattern = /^[A-Za-z 0-9.-]{1,150}$/;
			//alert("subcategorynamevalidation() called");
			if (productname == "") {
				//alert("subcategorynamevalidation() 1 if called");
				document.getElementById('product_name').innerHTML = "*Please enter product name";
				//$('#ascsubmit').attr("disabled", true);
				return false;
			} else {
				if ((productname.length < 1) || (productname.length > 150)) {
					//alert("subcategorynamevalidation() 2 if called");
					document.getElementById('product_name').innerHTML = "*Please enter valid  product name between 1 and 150  ";
					//$('#ascsubmit').attr("disabled", true);
					return false;
				}
				if (pattern.test(productname)) {
					//alert("subcategorynamevalidation() 2 if called");
					document.getElementById('product_name').innerHTML = "";
					$('#apsubmit').attr("disabled", false);
					return true;
				} else {
					//alert("subcategorynamevalidation() else called");
					document.getElementById('product_name').innerHTML = "*Please enter valid product name  ";
					//$('#ascsubmit').attr("disabled", true);
					return false;
				}
			}

		}

		function aboutproductvalidation() {
			var abpr = document.getElementById('aboutproduct').value;
			//var pattern = /^[A-Za-z0-9 ]{1,50}$/;
			//alert("descriptionvalidation() called");
			if (abpr == "") {
				//alert("descriptionvalidation() 1 if called");
				document.getElementById('about_product').innerHTML = "*Please enter product  features";
				//$('#ascsubmit').attr("disabled", true);
				return false;
			} else {
				if (abpr.length <= 3) {
					//alert("descriptionvalidation() 2 if called");
					document.getElementById('about_product').innerHTML = "*Please enter valid product  features of atleast 3 characters  ";
					//$('#ascsubmit').attr("disabled", true);
					return false;
				}
				else
					{
					document.getElementById('about_product').innerHTML = "";
					$('#apsubmit').attr("disabled", false);
					return true;
					}
				
// 				if (pattern.test(abpr)) {
// 					//alert("descriptionvalidation() 3 if called");
					
// 				} else {
// 					//alert("descriptionvalidation() else called");
// 					document.getElementById('about_product').innerHTML = "*Please enter valid product description ";
// 					//$('#ascsubmit').attr("disabled", true);
// 					return false;
// 				}
			}

		}
		
		function descriptionvalidation() {
			var description = document.getElementById('productdescription').value;
			//var pattern = /^[A-Za-z0-9 ]{1,50}$/;
			//alert("descriptionvalidation() called");
			if (description == "") {
				//alert("descriptionvalidation() 1 if called");
				document.getElementById('product_description').innerHTML = "*Please enter product description";
				//$('#ascsubmit').attr("disabled", true);
				return false;
			} else {
				if (description.length <= 3) {
					//alert("descriptionvalidation() 2 if called");
					document.getElementById('product_description').innerHTML = "*Please enter valid product  description of atleast 3 characters  ";
					//$('#ascsubmit').attr("disabled", true);
					return false;
				}
				else
					{
					document.getElementById('product_description').innerHTML = "";
					$('#epsubmit').attr("disabled", false);
					return true;
					}
				
// 				if (pattern.test(description)) {
// 					//alert("descriptionvalidation() 3 if called");
					
// 				} else {
// 					//alert("descriptionvalidation() else called");
// 					document.getElementById('product_description').innerHTML = "*Please enter valid product description ";
// 					//$('#ascsubmit').attr("disabled", true);
// 					return false;
// 				}
			}

		}

		function quantityvalidation() {
			if (document.getElementById('productquantity').value == "") {
				document.getElementById('product_quantity').innerHTML = "*Please enter product quantity";
			} else {
				document.getElementById('product_quantity').innerHTML = "";
				$('#epsubmit').attr("disabled", false);
			}
		}
		function pricevalidation() {
			if (document.getElementById('productprice').value == "") {
				document.getElementById('product_price').innerHTML = "*Please enter product price";
			} else {
				document.getElementById('product_price').innerHTML = "";
				$('#epsubmit').attr("disabled", false);
			}
		}

		function epformvalidation() {
			if (document.getElementById('categoryoption').value == "0") {
				document.getElementById('category_option').innerHTML = "*Please select category";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('subcategoryoption').value == "0") {
				document.getElementById('subcategory_option').innerHTML = "*Please select category";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('companyoption').value == "0") {
				document.getElementById('company_option').innerHTML = "*Please select compay";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('productname').value == "") {
				document.getElementById('product_name').innerHTML = "*Please enter product name";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('aboutproduct').value == "") {
				document.getElementById('about_product').innerHTML = "*Please enter about product";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('productdescription').value == "") {
				document.getElementById('product_description').innerHTML = "*Please enter description";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('productquantity').value == "") {
				document.getElementById('product_quantity').innerHTML = "*Please enter product quantity";
				$('#epsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('productprice').value == "") {
				document.getElementById('product_price').innerHTML = "*Please enter product price";
				$('#epsubmit').attr("disabled", true);
				return false;
			}

			if ((document.getElementById('category_option').innerHTML == "")
					&& (document.getElementById('subcategory_option').innerHTML == "")
					&& (document.getElementById('company_option').innerHTML == "")
					&& (document.getElementById('product_name').innerHTML == "")
					&& (document.getElementById('about_product').innerHTML == "")
					&& (document.getElementById('product_description').innerHTML == "")
					&& (document.getElementById('product_quantity').innerHTML == "")
					&& (document.getElementById('product_price').innerHTML == "")) {
				if (confirm("Do you want to edit product?") == true) {
					//alert("ok called");
					return true;
				} else {
					//alert("ok else condition called");
					return false;
				}
			} else {
				$('#epsubmit').attr("disabled", true);
				return false;
			}

		}
	</script>        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {"colors":{"state":{"brand":"#5d78ff","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
        </script>
        <!-- end::Global Config -->

    	<!--begin::Global Theme Bundle(used by all pages) -->
    	    	   <script src="js/plugins.bundle.js" type="text/javascript"></script>
		    	   <script src="js/scripts.bundle.js" type="text/javascript"></script>
				   <script src="js/dropzonejs.js" type="text/javascript"></script>
			
				<!--end::Global Theme Bundle -->

        
            </body>
    <!-- end::Body -->

<!-- Mirrored from keenthemes.com/metronic/preview/demo11/crud/forms/controls/base.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Oct 2019 04:55:00 GMT -->
</html>
