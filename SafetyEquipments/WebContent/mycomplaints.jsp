<%@page import="com.safetyequipment.beans.Complaint"%>
<%@page import="java.util.List"%>
<%@page import="com.safetyequipment.beans.SubCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Complaints List</title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  <link rel="icon" href="images/logo.png" type="image/x-icon">

</head>
<body>
<% List<Complaint> complaintlist = (ArrayList)(request.getAttribute("complaintlist")); %>
<%int i = complaintlist.size(); %>
<%@include file="header.jsp" %>
<!-- <a href="AddSubCategoryServlet">Add Subcategory</a> -->
<div>
<%if(i!=0){ %>
<table  id="example" class="display" style="width:100%" border="1">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>User Compalint ID</th>
                <th>Complaint Name</th>
                <th>Product Name</th>
                <th>Order ID</th>
                <th>Complaint Description</th>
                <th> Active Status </th>
                <th>Edit</th>
<!--                 <th>Delete</th> -->
            </tr> 
        </thead>
        <tbody>
        
        <%int cnt =0; %>
        
        <% for(Complaint complaint : complaintlist){ %>
        <tr>   
     	<td><%= ++cnt %></td>
     	<td><%=complaint.getUsercomplaintid()%></td>
     	<td><%=complaint.getName() %></td>
     	<td><%=complaint.getProductname() %></td>
     	<td><%=complaint.getOrderkey() %></td>
     	<td><%=complaint.getDescription() %></td>
     	<td><%=complaint.getActivestatus() %></td>
     	
     	<td><a href="EditUserComplaintServlet?usercomplaint_id=<%=complaint.getUsercomplaintid() %>" class="fa fa-edit" style="font-size:36px;"></a></td>	
<%--      	<td><a href="DeleteSubCategoryServlet?sub_category_id=<%=subcategory.getSubcategoryid() %>" class="fa fa-trash-o" style="font-size:36px" onclick="return validation()"></a></td>	    	 --%>
     	</tr>
        <%}%>
        
<!--                 </tfoot> -->
    </table>
    <%}else { %>
    <li style="align-content: center;margin-left: 30%; color: red;font-family: cursive;font-size:large;">NO COMPLAINTS IN YOUR LIST</li>
    <%} %>
    </div>
    <%@include file="footer.jsp" %>
    	<!-- /main wrapper -->

	
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
	function validation() {
		if (confirm("Do you want to delete subcategory?") == true) {
			//alert("ok called");
			return true;
		} else {
			//alert("ok else condition called");
			return false;
		}
	}
	
	</script>
</body>
</html>