
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
<head>
<meta charset="utf-8">
<title>Dipen Safety</title>

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- <style> -->
<link rel="stylesheet" href="css/passwordvalidation.css">
<!-- * Plugins Needed for the Project * -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">
<link href="css/all.css" rel="stylesheet">
<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
</head>
<body>
	<!-- <i class="material-icons">delete</i>  -->
	<a class="fas fa-trash-alt"></a>


	<!-- 	<!-- preloader start -->
	<!-- 	<div class="preloader"> -->
	<!-- 		<img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- 			width="160px"> -->
	<!-- 	</div> -->
	<!-- 	<!-- preloader end -->
	<!-- preloader start -->
	<!-- 	<div class="preloader"> -->
	<!-- 		<img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- 			width="160px"> -->
	<!-- 	</div> -->
	<!-- preloader end -->

	<section class="signin-page account">
		<div class="container" style="margin-top:-130px;">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index-2.jsp"> <img src="images/DLOGO.png" alt="logo">
						</a>
						<h2 class="text-center">Create Your Account</h2>
						<form class="text-left clearfix" action="UserRegServlet"
							method="post" onsubmit="return formvalidation()">


							<!--             <div class="form-group"> -->
							<!--               <input type="text" class="form-control"  placeholder="First Name" name="firstname"> -->
							<!--             </div> -->
							<!--             <div class="form-group"> -->
							<!--               <input type="text" class="form-control"  placeholder="Last Name" name="lastname"> -->
							<!--             </div> -->


							<div class="form-group">
								<input type="text" class="form-control" placeholder="First name"
									name="firstname" id="firstname"
									onkeypress="javascript:return isCharacter(event)"
									onblur="return firstnamevalidation()"> <span
									id="first_name" style="color: red;"></span>
							</div>

							<div class="form-group">
								<input type="text" class="form-control" placeholder="Last name"
									name="lastname" id="lastname"
									onkeypress="javascript:return isCharacter(event)"
									onblur="return lastnamevalidation()"> <span
									id="last_name" style="color: red;"></span>
							</div>

							<div class="form-group">
							 	<input type="email" class="form-control" placeholder="Email"
									name="emailid" id="emailid" onblur="return emailvalidation()">
								<span style="color: red;" id="email_id"></span> 

							</div>

  							<div class="form-group">  
  								<input type="password" class="form-control"  
  									placeholder="Password" name="upassword" 
  									id="upassword"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
  									onblur="return passwordvalidation()"> <span  
  									id="pass_word" style="color: red;"></span>  
  							</div>  
	
						<div id="message">
  						<h3>Password must contain the following:</h3>
  						<p id="letter" class="invalid">A <b>lowercase</b> letter</p>
					  	<p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
					  	<p id="number" class="invalid">A <b>number</b></p>
 					 	<p id="length" class="invalid">Minimum <b>8 characters</b></p>
						</div>

							<div class="form-group">
								<input type="password" class="form-control"
									placeholder="Confirm Password" name="confirmpassword"
									id="confirmpassword"
									onblur="return confirmpasswordvalidation()"> <span
									id="confirm_password" style="color: red;"></span>
							</div>

							<div class="text-center">
								<button type="submit" id="signsubmit" name="signsubmit"
									class="btn btn-primary">Sign In</button>
							</div>
						</form>
						<p class="mt-3">
							Already have an account ?<a href="login.jsp"> Login</a>
						</p>
						<p>
							<a href="forget-password.jsp"> Forgot your password?</a>
						</p>						
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
				
	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
		function firstnamevalidation() {
			var firstname = document.getElementById('firstname').value;
			var pattern = /^[A-Za-z]{1,20}$/;

			if (firstname == "") {
				document.getElementById('first_name').innerHTML = "*Please enter first name";
				return false;
			}
			if ((firstname.length < 1) || (firstname.length > 20)) {
				document.getElementById('first_name').innerHTML = "*Please enter last name between 1 to 20 characters";
				return false;
			}
			if (pattern.test(firstname)) {
				document.getElementById('first_name').innerHTML = "";
				$('#signsubmit').attr("disabled", false);
				return true;
			} else {
				document.getElementById('first_name').innerHTML = "*Please enter valid first name  ";
				return false;
			}

		}

		function lastnamevalidation() {
			var lastname = document.getElementById('lastname').value;
			var pattern = /^[A-Za-z]{1,20}$/;

			if (lastname == "") {
				document.getElementById('last_name').innerHTML = "*Please enter last name";
				return false;
			}
			if ((lastname.length < 1) || (lastname.length > 20)) {
				document.getElementById('last_name').innerHTML = "*Please enter last name between 1 to 20 characters";
				return false;
			}
			if (pattern.test(lastname)) {
				document.getElementById('last_name').innerHTML = "";
				$('#signsubmit').attr("disabled", false);
				return true;
			} else {
				document.getElementById('last_name').innerHTML = "*Please enter valid  last name  ";
				return false;
			}

		}

		function emailvalidation() {

			var emailid = document.getElementById('emailid').value;
			var pattern = /^[A-Za-z0-9_.-]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/;
			if (emailid == "") {
				document.getElementById('email_id').innerHTML = "*Please enter emailid  ";
				return false;
			}
			if (pattern.test(emailid)) {
				document.getElementById('email_id').innerHTML = "";
				$('#signsubmit').attr("disabled", false);
				return true;
			} else {
				document.getElementById('email_id').innerHTML = "*Please enter valid emailid ";
				return false;
			}
		}

		function passwordvalidation() {
			//alert("password validation start");
			var password = document.getElementById('upassword').value;
			//alert(" password : " + password);
			if (password == "") {
				//alert(" fill password");
				document.getElementById('pass_word').innerHTML = "*Please enter password  ";
				return false;
			}
			if(password.length < 8) {
				document.getElementById('pass_word').innerHTML = "* the length of  password should at least 8 characters ";
				return false;
			} else {
				document.getElementById('pass_word').innerHTML = "";
				$('#signsubmit').attr("disabled", false);
				return true;
			}
		}

		function confirmpasswordvalidation() {
			var password = document.getElementById('upassword').value;
			var confirmpassword = document.getElementById('confirmpassword').value;
			if (confirmpassword == "") {
				document.getElementById('confirm_password').innerHTML = "*Please enter confirm password  ";
				return false;
			}
			if ((password.length > 8) && (password.length < 8)) {
				document.getElementById('confirm_password').innerHTML = "*length of confirm password must be 8";
				return false;
			}
			if (password != confirmpassword) {
				document.getElementById('confirm_password').innerHTML = "* Password and Confirm password does not match";
				return false;
			} else {
				document.getElementById('confirm_password').innerHTML = "";
				$('#signsubmit').attr("disabled", false);
				return true;
			}
		}

		function formvalidation() {
			if (document.getElementById('firstname').value == "") {
				document.getElementById('first_name').innerHTML = "*Please enter first name";
				$('#signsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('lastname').value == "") {
				document.getElementById('last_name').innerHTML = "*Please enter last name";
				$('#signsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('emailid').value == "") {
				document.getElementById('email_id').innerHTML = "*Please enter email id";
				$('#signsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('upassword').value == "") {
				document.getElementById('pass_word').innerHTML = "*Please enter password  ";
				$('#signsubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('confirmpassword').value == "") {
				document.getElementById('confirm_password').innerHTML = "*Please enter confirm password  ";
				$('#signsubmit').attr("disabled", true);
				return false;
			}			
			if ((document.getElementById('first_name').innerHTML == "")
					&& (document.getElementById('last_name').innerHTML == "")
					&& (document.getElementById('email_id').innerHTML == "")
					&& (document.getElementById('pass_word').innerHTML == "")
					&& (document.getElementById('confirm_password').innerHTML == "")) {
				//alert("registration successful");
				$('#signsubmit').attr("disabled", false);
				if (confirm("Do you want to submit your details?") == true) {
		  			return true;
		  		} else {
		  			return false;
		  		}
			} else {
				//alert("Please fill details ");
				$('#signsubmit').attr("disabled", true);
				return false;
			}
		}

		$(document).ready(function() {
			$('#emailid').blur(function(event) {
				var name = $('#emailid').val();
				$.get('CheckUserRegistrationServlet', {
					useremail : name
				}, function(responseText) {
					$('#CheckUserRegistrationServlet').text(responseText);
					if (responseText.length > 3) {
						$("#email_id").html(responseText);
						$('#signsubmit').attr("disabled", true);
					} else {
						$('#signsubmit').attr("disabled", false);
					}

				});
			});

		});
		
		
		
		var myInput = document.getElementById("upassword");
		var letter = document.getElementById("letter");
		alert(document.getElementById("letter"));
		var capital = document.getElementById("capital");
		var number = document.getElementById("number");
		var length = document.getElementById("length");

		// When the user clicks on the password field, show the message box
		myInput.onfocus = function() {
		  document.getElementById("message").style.display = "block";
		}

		// When the user clicks outside of the password field, hide the message box
		myInput.onblur = function() {
		  document.getElementById("message").style.display = "none";
		}

		// When the user starts to type something inside the password field
		myInput.onkeyup = function() {
		  // Validate lowercase letters
		  var lowerCaseLetters = /[a-z]/g;
		  if(myInput.value.match(lowerCaseLetters)) {  
		    letter.classList.remove("invalid");
		    letter.classList.add("valid");
		  } else {
		    letter.classList.remove("valid");
		    letter.classList.add("invalid");
		  }
		  
		  // Validate capital letters
		  var upperCaseLetters = /[A-Z]/g;
		  if(myInput.value.match(upperCaseLetters)) {  
		    capital.classList.remove("invalid");
		    capital.classList.add("valid");
		  } else {
		    capital.classList.remove("valid");
		    capital.classList.add("invalid");
		  }

		  // Validate numbers
		  var numbers = /[0-9]/g;
		  if(myInput.value.match(numbers)) {  
		    number.classList.remove("invalid");
		    number.classList.add("valid");
		  } else {
		    number.classList.remove("valid");
		    number.classList.add("invalid");
		  }
		  
		  // Validate length
		  if(myInput.value.length >= 8) {
		    length.classList.remove("invalid");
		    length.classList.add("valid");
		  } else {
		    length.classList.remove("valid");
		    length.classList.add("invalid");
		  }
		}

	</script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
</html>