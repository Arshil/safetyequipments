<%@page import="com.safetyequipment.beans.Category"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Category</title>
</head>
<body>
<%Category category =(Category)(request.getAttribute("category")); %>
<%@include file="header.jsp" %>
<section class="signin-page account">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mx-auto">
        <div class="block text-center">
          <a class="logo" href="index-2.jsp">
            <img src="images/logo.png" alt="logo">
          </a>
          <h2 class="text-center">update Category</h2>
          <form class="text-left clearfix" action="UpdateCategoryDetailsServlet" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <input type="text" class="form-control"  value="<%= category.getCategoryname() %>" id="categoryname" name="categoryname">
            </div>
            <div class="form-group">
              <input type="file" class="form-control"  accept=".png, .jpg, .jpeg" id="categoryimage" name="categoryimage">
              <img src="data:image/jpeg;base64,<%=category.getCategoryImageString()%>" height="100" width="100" >  
            </div>
            <div class="form-group">
              <input type="text" class="form-control"  value="<%=category.getDescription() %>" id="categorydescription" name="categorydescription">
            </div>
           
                    
           <div class="form-group">
           <input type="hidden" class="form-control" value="<%=category.getCategoryid() %>" id="categoryid" name="categoryid" >
            </div>
                 
   
            <div class="text-center">
              <button type="submit" class="btn btn-primary">Update Changes</button>
           </div>  
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="footer.jsp" %>
</body>
</html>