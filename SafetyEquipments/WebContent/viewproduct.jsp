<%@page import="com.safetyequipment.beans.Product"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Product List</title>
  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<link rel="icon" href="images/logo.png" type="image/x-icon">
</script>
</head>
<body>
<% List<Product> productlist = (ArrayList<Product>)(request.getAttribute("productlist")); %>
<%@include file="header.jsp" %>
<!-- <a href="AddProductServlet">Add Product</a> -->
<button style="margin-left:45%;" ><a href="AddProductServlet" class="fa fa-plus-square"> Add new product</a></button>
<div>
<table  id="example" class="display" style="width:100%" border="1">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Image</th>
                <th>Product Name</th>
                <th>Category Name</th>
                <th>Sub Category Name</th>
                <th>Company Name</th>
                <th>Product Price</th>
                <th>Product Description</th>
                <th>Product Features</th>
                <th>Product Quantity</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr> 
        </thead>
        <tbody>
        <%int cnt =0; %>
        
        <% for(Product product : productlist){ %>
        <tr>   
     	<td><%= ++cnt %></td>
     	<% if(product.getImagestring()!=null){ %>
     	<td><img src="data:image/jpeg;base64,<%=product.getImagestring()%>" height="100" width="100"></td>
     	<%} else { %>
     	<td> </td>
     	<%} %>
<%--      	<td><%=product.getproductid() %></td> --%>
<%--      	<td><%=product.getCategoryid() %></td> --%>
     	<td><%=product.getProductname() %></td>
     	<td><%=product.getCategoryname() %></td>
     	<td><%=product.getSubcategoryname() %></td>
     	<td><%=product.getCompanyname() %></td>
     	<td><%=product.getProductprice() %></td>
     	<%if(null!=product.getAboutproduct()){ %>
     	<td><%=product.getAboutproduct() %></td>
     	<%}else{ %>
     	<td>---</td>
     	<%} %>
     	<td><%=product.getDescription() %></td>
     	<td><%=product.getQuantity() %></td>
     	<td><a href="UpdateProductServlet?product_id=<%=product.getProductid() %>" class="fa fa-edit" style="font-size:36px;"></a></td>	
     	<td><a href="DeleteProductServlet?product_id=<%=product.getProductid() %>" class="fa fa-trash-o" style="font-size:36px;" onclick="return validation()"></a></td>	    	
<!-- 	     <td><a id="deleteproduct" ><i  class="fa fa-trash-o" style="font-size:36px"></i></a></td> -->
     	</tr>
        <%}%>
        
<!--                 </tfoot> -->
    </table>
    </div>
    <%@include file="footer.jsp" %>
    <script>
    function validation() {
		if (confirm("Do you want to delete product?") == true) {
			//alert("ok called");
			return true;
		} else {
			//alert("ok else condition called");
			return false;
		}
	}
    
//     $("deleteproduct").click(function(){
//     	alert("delete clicked");
//     });
    </script>
</body>
</html>