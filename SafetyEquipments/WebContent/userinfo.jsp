<%@page import="com.safetyequipment.beans.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
<head>
<meta charset="utf-8">
<title>My Account</title>

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- * Plugins Needed for the Project * -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">

<!--Favicon-->
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  <link rel="icon" href="images/logo.png" type="image/x-icon">

</head>

<body>
	<%
		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userlogin");
	%>
	<!-- 4 <!-- preloader start -->
	<!-- <div class="preloader"> -->
	<!-- <img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- width="160px"> -->
	<!-- </div> -->
	<!-- <!-- preloader end -->
	<!-- preloader start -->
	<!-- <div class="preloader"> -->
	<!-- <img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- width="160px"> -->
	<!-- </div> -->
	<!-- preloader end -->

	<section class="signin-page account">
		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index-2.jsp"> <img src="images/logo.png"
							alt="logo">
						</a>
						<h2 class="text-center">Your Account</h2>
						<form class="text-left clearfix" action="EditUserDetails"
							method="post" enctype="multipart/form-data" onsubmit="return formvalidation()">
							 
							<div class="form-group">
						
						
						<%if(null!=user.getImagestring()){ %>
								<img align="top" tabindex="1"
									src="data:image/jpeg;base64,<%=user.getImagestring()%>"
									height="100" width="100" id="profileimage" name="profileimage">
					<%} else{%>
									<img align="top" tabindex="1"
									src="images/avtarimg.png"
									height="100" width="100" id="profileimage" name="profileimage">
						
						<%} %>
								<input type="file" id="profileimage" name="profileimage">
							</div>

							<div class="form-group">
								<input type="text" class="form-control" placeholder="First name"
									value="<%=user.getFirstname()%>" name="firstname"
									id="firstname"
									onkeypress="javascript:return isCharacter(event)"
									onblur="return firstnamevalidation()"> <span
									id="first_name"></span>
							</div>


							<div class="form-group" align="right">
								<input type="text" class="form-control" placeholder="Last name"
									value="<%=user.getLastname()%>" name="lastname" id="lastname"
									onkeypress="javascript:return isCharacter(event)"
									onblur="return lastnamevalidation()"> <span
									id="last_name"></span>
							</div>



							<div class="form-group">
								Gender : <input type="radio" name="gender" id="gender" value="F"
									<%if (user.getGender().equalsIgnoreCase("F")) {%>
									checked="checked" <%}%>>Female <input type="radio"
									name="gender" value="M"
									<%if (user.getGender().equalsIgnoreCase("M")) {%>
									checked="checked" <%}%>>Male
									
							</div>



							<div class="form-group">
								<input type="text" class="form-control" placeholder="Mobile No."
									value="<%=user.getMobileno()%>" name="mobile" id="mobile"
									required="required"
									onkeypress="javascript:return isNumber(event)"
									onblur="return numbervalidation()"> <span
									id="mo_number"></span>
							</div>

							<div class="form-group">
								<input type="email" class="form-control" placeholder="Email"
									required="required" readonly="readonly"
									value="<%=user.getEmailid()%>" name="emailid" id="emailid"
									onblur="return emailvalidation()"> <span id="email_id"></span>
							</div>

							<div class="form-group">
								<textarea rows="3" cols="52" placeholder="Address"
								id="address" name="address" onblur="return addressvalidation()"><%=user.getAddress()%></textarea>
									<span id="user_address"></span>
							</div>

							<div class="form-group">
								<input type="text" class="form-control" placeholder="Pincode"
									onkeypress="javascript:return isNumber(event)" onblur="return pincodevalidation()"
									value="<%=user.getPincode()%>" name="pincode" id="pincode"
									pattern=".{6}">
									<span id="user_pincode"></span>
							</div>

							<div class="text-center">
								<button type="submit" id="uisubmit" class="btn btn-primary">Save
									Changes</button>
								<a href="index.jsp"><button type="button" id="backhome"
										class="btn btn-primary">Back to Home</button></a>
							</div>

							<div class="form-group">
								<input type="hidden" class="form-control"
									value="<%=user.getUserid()%>" id="userid" name="userid">
								<%
									System.out.print("user id  jsp :" + user.getUserid());
								%>
							</div>

						</form>


					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
		function firstnamevalidation() {
			//alert("called");
			var firstname = document.getElementById('firstname').value;
			var pattern = /^[A-Za-z]{3,20}$/;

			if (firstname == "") {
				document.getElementById('first_name').innerHTML = "*Please enter first name";
				return false;
			}
			if((firstname.length < 3 )|| (firstname.length > 20))
				{
				document.getElementById('first_name').innerHTML = "*length of first name should betwwn 3 to 20";
				return false;
				}
			if (pattern.test(firstname)) {
				document.getElementById('first_name').innerHTML = "";
				$('#uisubmit').attr("disabled", false);
				return true;
			} else {
				document.getElementById('first_name').innerHTML = "*Please enter valid first name  ";
				return false;
			}

		}

		function lastnamevalidation() {
			var lastname = document.getElementById('lastname').value;
			var pattern = /^[A-Za-z]{3,20}$/;

			if (lastname == "") {
				document.getElementById('last_name').innerHTML = "*Please enter last name";
				return false;
			}
			if ((lastname.length < 3) || (lastname.length > 20)) {
				document.getElementById('last_name').innerHTML = "*length of last name should betwwn 3 to 20";
				return false;
			}
			if (pattern.test(lastname)) {
				document.getElementById('last_name').innerHTML = "";
				$('#uisubmit').attr("disabled", false);
				return true;
			} else {
				document.getElementById('last_name').innerHTML = "*Please enter valid  last name  ";
				return false;
			}

		}

		function numbervalidation() {
			var mno = document.getElementById('mobile').value;
			
			if (mno == "") {
				document.getElementById('mo_number').innerHTML = "*Please enter  mobile number  ";
				return false;
			}
			if ((mno.length > 10) || (mno.length < 10)) {
				document.getElementById('mo_number').innerHTML = "*Please enter 10 digit mobile number ";
				return false;
				
			} else {
				document.getElementById('mo_number').innerHTML = "";
				$('#uisubmit').attr("disabled", false);
				return true;
			}
		}
		
		
		function pincodevalidation() {
			var pincode = document.getElementById('pincode').value;
			
			if (pincode == "") {
				document.getElementById('user_pincode').innerHTML = "*Please enter  pincode  ";
				return false;
			}
			if ((pincode.length > 6) || (pincode.length < 6)) {
				document.getElementById('user_pincode').innerHTML = "*Please enter 6 digit pincode ";
				return false;
				
			} else {
				document.getElementById('user_pincode').innerHTML = "";
				$('#uisubmit').attr("disabled", false);
				return true;
			}
		}
		
		

		function addressvalidation() {
			var address = document.getElementById('address').value;
			//var pattern = /^[A-Za-z0-9_.-/, ]{10,200}$/;
			if (address == "") {
				document.getElementById('user_address').innerHTML = "*Please enter address  ";
				return false;
			}
			else {
			if (address.length <= 10) {
				document.getElementById('user_address').innerHTML = "* the length of  address  must greater then 10 ";
				return false;
			} 
			else
				{
				document.getElementById('user_address').innerHTML = "";
				$('#uisubmit').attr("disabled", false);
				return true;
				}
			}
// 			if (pattern.test(address)) {
// 				document.getElementById('user_address').innerHTML = "";
// 				$('#uisubmit').attr("disabled", false);
// 				return true;
// 			}
// 			else {
// 				document.getElementById('user_address').innerHTML = "*Please enter  valid address  ";
// 				return false;
// 			}
		}



		function formvalidation() {
			if (document.getElementById('firstname').value == "") {
				document.getElementById('first_name').innerHTML = "*Please enter first name";
				$('#uisubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('lastname').value == "") {
				document.getElementById('last_name').innerHTML = "*Please enter last name";
				$('#uisubmit').attr("disabled",  true);
				return false;
			}
			if (document.getElementById('mobile').value == "") {
				document.getElementById('mo_number').innerHTML = "*Please enter mobile number";
				$('#uisubmit').attr("disabled",  true);
				return false;
			}
			if (document.getElementById('address').value == "") {
				document.getElementById('user_address').innerHTML = "*Please enter address";
				$('#uisubmit').attr("disabled",  true);
				return false;
			}
			if (document.getElementById('pincode').value == "") {
				document.getElementById('user_pincode').innerHTML = "*Please enter address";
				$('#uisubmit').attr("disabled",  true);
				return false;
			}
			
			if ((document.getElementById('first_name').innerHTML == "")
					&& (document.getElementById('last_name').innerHTML == "")
					&& (document.getElementById('mo_number').innerHTML == "")
					&& (document.getElementById('user_pincode').innerHTML == "")
					&& (document.getElementById('user_address').innerHTML == "")) {
				$('#uisubmit').attr("disabled", false);
				
				if (confirm("Do you want to save your updated  details?") == true) {
		  			return true;
		  		} else {
		  			return false;
		  		}
			} else {
				$('#uisubmit').attr("disabled",  true);
				return false;
			}
		}
	</script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
</html>

