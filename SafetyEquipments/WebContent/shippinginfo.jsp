<%@page import="com.safetyequipment.beans.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
<head>
<meta charset="utf-8">
<title>Shipping Information</title>

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- * Plugins Needed for the Project * -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<link rel="icon" href="images/logo.png" type="image/x-icon">

</head>

<body>
	<%
		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userlogin");
	%>
	<!-- 4 <!-- preloader start -->
	<!-- <div class="preloader"> -->
	<!-- <img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- width="160px"> -->
	<!-- </div> -->
	<!-- <!-- preloader end -->
	<!-- preloader start -->
	<!-- <div class="preloader"> -->
	<!-- <img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- width="160px"> -->
	<!-- </div> -->
	<!-- preloader end -->

	<section class="signin-page account">
		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index-2.jsp"> <img src="images/logo.png"
							alt="logo">
						</a>
						<h2 class="text-center">Add / Edit Shipping Information</h2>
						<form class="text-left clearfix" action="revieworder.jsp"
							enctype="multipart/form-data" onsubmit="return formvalidation()">

							<div class="form-group">
								<textarea rows="3" cols="52" placeholder="Address" id="address"
									required="required" onblur="addaddress(this)" name="address"><%=user.getAddress()%></textarea>
								<span id="user_address"></span>
							</div>

							<!-- 							<div class="form-group"> -->
							<!-- 								<input type="text"  placeholder="Address" id="address" name="address" -->
							<%-- 								required="required" onblur="addaddress(this)"  value="<%=user.getAddress()%>"> --%>
							<!-- 								<span id="user_address"></span> -->
							<!-- 							</div> -->

							<div class="form-group">
								<input type="text" class="form-control" placeholder="Pincode"
									onkeypress="javascript:return isNumber(event)"
									value="<%=user.getPincode()%>" onblur="addpincode(this)"
									name="pincode" id="pincode" pattern=".{6}"> <span
									id="user_pincode"></span>
							</div>

							<div class="form-group">
								<input type="text" class="form-control" placeholder="Mobile No."
									value="<%=user.getMobileno()%>" name="mobile" id="mobile"
									required="required" onblur="addnumber(this)"
									onkeypress="javascript:return isNumber(event)"> <span
									id="mo_number"></span>
							</div>

							<!-- 							<div class="form-group"> -->
							<!-- 								<input type="email" class="form-control" placeholder="Email" -->
							<!-- 									required="required" readonly="readonly" -->
							<%-- 									value="<%=user.getEmailid()%>" name="emailid" id="emailid" --%>
							<!-- 									onblur="return emailvalidation()"> <span id="email_id"></span> -->
							<!-- 							</div> -->

							<div class="form-group">
								<input type="hidden" class="form-control"
									value="<%=user.getUserid()%>" id="userid" name="userid">
								<%
									System.out.print("user id  jsp :" + user.getUserid());
								%>
							</div>

							<div class="text-center">
								<button type="submit" id="sisubmit" class="btn btn-primary">Save
									Changes</button>
								<a href="index.jsp"><button type="button" id="backhome"
										class="btn btn-primary">Back to Home</button></a>
							</div>



						</form>


					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
		// $(document).ready(function() {
		//  $('#address').blur(function(event) {
		//  	alert(called);
		//		var address = document.getElementById('address').value;
		//		//var pattern = /^[A-Za-z0-9_.-/, ]{10,200}$/;
		//		alert("called");
		//		if (address == "") {
		//			$("#user_address").text("*Please enter address  ");
		//			//document.getElementById('user_address').innerHTML = "*Please enter address  ";
		//			//return false;
		//		} else {
		//			if (address.length <= 10) {
		//				$("#user_address").text(
		//						"* the length of  address  must greater then 10 ");
		//				//document.getElementById('user_address').innerHTML = "* the length of  address  must greater then 10 ";
		//				//return false;
		//			} else {
		//				alert(address  called);
		//				var addr = $('#address').val();
		//				var uid = $('input[name=userid]').val();
		//				var call = "address";
		//				alert(addr);
		//				alert(uid);
		//				alert(call);
		//				$.get('AddAddressServlet', {
		//					useraddress : addr,
		//					userid : uid,
		//					callfor : call
		//				}, function(responseText) {
		//					//$('#ajaxGetUserServletResponse').text(responseText);
		//					alert(responseText);
		//					$("#user_address").text("");
		//					$('#sisubmit').attr("disabled", false);
		//				});
		//			}
		//		}
		//  });
		//});
		function addaddress(address) {
			//alert(called);
			//var address = document.getElementById('address').value;
			//var pattern = /^[A-Za-z0-9_.-/, ]{10,200}$/;
			//alert("called");
			if (address.value == "") {
				$("#user_address").text("*Please enter address  ");
				//document.getElementById('user_address').innerHTML = "*Please enter address  ";
				//return false;
			} else {
				if (address.value.length <= 10) {
					$("#user_address").text(
							"* the length of  address  must greater then 10 ");
					//document.getElementById('user_address').innerHTML = "* the length of  address  must greater then 10 ";
					//return false;
				} else {
					alert("address else  called");
					//var addr = $('#address').val();
					var addr = address.value;
					var uid = $('input[name=userid]').val();
					//var call = "address";
					//alert(addr);
					//alert(uid);
					//alert(call);
					$.get('AddAddressServlet', {
						useraddress : addr,
						userid : uid
					//callfor : call
					}, function(responseText) {
						//$('#ajaxGetUserServletResponse').text(responseText);
						//alert(responseText);
						$("#user_address").text("");
						$('#sisubmit').attr("disabled", false);
					});
				}
			}
		}

		// 	$(document).ready(function() {
		//         $('#mobile').blur(function(event) {
		//         	alert("called");
		// 			var mno = document.getElementById('mobile').value;
		// 			if (mno == "") {

		// 				$("#mo_number").text("*Please enter  mobile number  ");
		// 				//document.getElementById('mo_number').innerHTML = "*Please enter  mobile number  ";
		// 				//return false;
		// 			} else {
		// 				if ((mno.length > 10) || (mno.length < 10)) {
		// 					$("#mo_number").text(
		// 							"*Please enter 10 digit mobile number ");
		// 					//document.getElementById('mo_number').innerHTML = "*Please enter 10 digit mobile number ";
		// 					//return false;

		// 				} else {
		// 					var mono = $('#mobile').val();
		// 					var uid = $('input[name=userid]').val();
		// 					var call = "mobile";
		// 					alert(mono);
		// 					alert(uid);
		// 					alert(call);
		// 					$.get('AddMobileServlet', {
		// 						mobile : mono,
		// 						userid : uid,
		// 						callfor : call
		// 					}, function(responseText) {
		// 						//$('#ajaxGetUserServletResponse').text(responseText);
		// 						alert(responseText);
		// 						$("#mo_number").text("");
		// 						$('#sisubmit').attr("disabled", false);
		// 					});
		// 				}
		// 			}

		//         });
		// });

		function addnumber(mobile) {
			//alert("called");
			//var mono = document.getElementById('mobile').value;
			if (mobile.value == "") {

				$("#mo_number").text("*Please enter  mobile number  ");
				//document.getElementById('mo_number').innerHTML = "*Please enter  mobile number  ";
				//return false;
			} else {
				if ((mobile.value.length > 10) || (mobile.value.length < 10)) {
					$("#mo_number").text(
							"*Please enter 10 digit mobile number ");
					//document.getElementById('mo_number').innerHTML = "*Please enter 10 digit mobile number ";
					//return false;

				} else {
					//var mno = $('#mobile').val();
					var mno = mobile.value;
					var uid = $('input[name=userid]').val();
					//var call = "mobile";
					//alert(mno);
					//alert(uid);
					//alert(call);
					$.get('AddMobileServlet', {
						mobile : mno,
						userid : uid
					//callfor : call
					}, function(responseText) {
						//$('#ajaxGetUserServletResponse').text(responseText);
						//alert(responseText);
						$("#mo_number").text("");
						$('#sisubmit').attr("disabled", false);
					});
				}
			}
		}

		// $(document).ready(function() {
		//         $('#pincode').blur(function(event) {
		//         	alert("called");
		// 			var pincode = document.getElementById('pincode').value;

		// 			if (pincode == "") {
		// 				$("#user_pincode").text("*Please enter  pincode  ");
		// 				//document.getElementById('user_pincode').innerHTML = "*Please enter  pincode  ";
		// 				//return false;
		// 			} else {
		// 				if ((pincode.length > 6) || (pincode.length < 6)) {
		// 					$("#user_pincode").text("*Please enter 6 digit pincode ");
		// 					//document.getElementById('user_pincode').innerHTML = "*Please enter 6 digit pincode ";
		// 					//return false;

		// 				} else {
		// 					var pin = $('#pincode').val();
		// 					var uid = $('i	nput[name=userid]').val();
		// 					var call = "pincode";
		// 					alert(pin);
		// 					alert(uid);
		// 					alert(call);
		// 					$.get('AddPincodeServlet', {
		// 						pincode : pin,
		// 						userid : uid,
		// 						callfor : call
		// 					}, function(responseText) {
		// 						//$('#ajaxGetUserServletResponse').text(responseText);
		// 						alert(responseText);
		// 						$("#user_pincode").text("");
		// 						$('#sisubmit').attr("disabled", false);
		// 					});
		// 				}
		// 			}

		//         });
		// });
		function addpincode(pincode) {
			//alert("called");
			//var pincode = document.getElementById('pincode').value;

			if (pincode.value == "") {
				$("#user_pincode").text("*Please enter  pincode  ");
				//document.getElementById('user_pincode').innerHTML = "*Please enter  pincode  ";
				//return false;
			} else {
				if ((pincode.value.length > 6) || (pincode.value.length < 6)) {
					$("#user_pincode").text("*Please enter 6 digit pincode ");
					//document.getElementById('user_pincode').innerHTML = "*Please enter 6 digit pincode ";
					//return false;

				} else {
					//var pin = $('#pincode').val();
					var pin = pincode.value;
					var uid = $('input[name=userid]').val();
					//var call = "pincode";
					//alert(pin);
					//alert(uid);
					//alert(call);
					$.get('AddPincodeServlet', {
						pincode : pin,
						userid : uid
					//callfor : call
					}, function(responseText) {
						//$('#ajaxGetUserServletResponse').text(responseText);
						//alert(responseText);
						$("#user_pincode").text("");
						$('#sisubmit').attr("disabled", false);
					});
				}
			}
		}

		function formvalidation() {
			//alert("called");
			//var uaddress  = document.getElementById('address').value;
			//alert(uaddress);
			if (document.getElementById('address').value == "") {
				document.getElementById('user_address').innerHTML = "*Please enter address";
				$('#sisubmit').attr("disabled", true);
				return false;
			}
			
// 			if (uaddress.length > 10) {
// 				document.getElementById('user_address').innerHTML = "* the length of  address  must greater then 10 ";
// 				$('#sisubmit').attr("disabled", true);
// 				return false;
// 			}
		   if(document.getElementById('address').value == "null")
			   {
				document.getElementById('user_address').innerHTML = "* the length of  address  must greater then 10 ";
				$('#sisubmit').attr("disabled", true);
				return false;
			   }
			
			if (document.getElementById('pincode').value == "") {
				document.getElementById('user_pincode').innerHTML = "*Please enter address";
				$('#sisubmit').attr("disabled", true);
				return false;
			}
			if (document.getElementById('mobile').value == "") {
				document.getElementById('mo_number').innerHTML = "*Please enter mobile number";
				$('#sisubmit').attr("disabled", true);
				return false;
			}
			if ((document.getElementById('mo_number').innerHTML == "")
					&& (document.getElementById('user_pincode').innerHTML == "")
					&& (document.getElementById('user_address').innerHTML == "")) {
				$('#sisubmit').attr("disabled", false);

				if (confirm("Do you want to save your details?") == true) {
					return true;
				} else {
					return false;
				}
			} else {
				$('#sisubmit').attr("disabled", true);
				return false;
			}
		}
	</script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
</html>

