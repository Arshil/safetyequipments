<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.safetyequipment.beans.SubCategory"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.safetyequipment.beans.Category"%>
<%@page import="java.util.List"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>footer</title>

  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>
<jsp:include page="/FetchMenuServlet"/>
<% List<Category> menu = (ArrayList)(request.getAttribute("categorylist")); %>

 <footer class="bg-light"> 
  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6 mb-5 mb-md-0 text-center text-sm-left">
          <h4 class="mb-4">Contact</h4>
<!--           <p>20464 Hirthe Curve Suite, Emardton, CT 12471-4107</p> -->
				<p>508-509 Aakruti Complex,Nr.Sahyadri Flats, Stadium Cross Road, Navrangpura, Ahmedabad-380009, Gujarat</p>
          <p>079-40055545</p>
          <p>dipen_security@rediffmail.com</p>
          <ul class="list-inline social-icons">
            <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-vimeo-alt"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-google"></i></a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 mb-5 mb-md-0 text-center text-sm-left">
          <h4 class="mb-4">Category</h4>
          <ul class="pl-0 list-unstyled">
          <%for(Category categ : menu) { %>
<%--           <a href="GetProductsOfSelectedCategoryServlet?category_id=<%= categorymenu.getCategoryid() %>"> --%>
            <li class="mb-2"><a class="text-color" href="GetProductsOfSelectedCategoryServlet?category_id=<%= categ.getCategoryid() %>"><%=categ.getCategoryname() %></a></li>
          <%} %>         
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 mb-5 mb-md-0 text-center text-sm-left">
          <h4 class="mb-4">Useful Link</h4>
          <ul class="pl-0 list-unstyled">
            <li class="mb-2"><a class="text-color" href="about.jsp">News & Tips</a></li>
            <li class="mb-2"><a class="text-color" href="about.jsp">About Us</a></li>
            <li class="mb-2"><a class="text-color" href="address.jsp">Support</a></li>
            <li class="mb-2"><a class="text-color" href="shop.jsp">Our Shop</a></li>
            <li class="mb-2"><a class="text-color" href="contact.jsp">Contact Us</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 text-center text-sm-left">
          <h4 class="mb-4">Our Policies</h4>
          <ul class="pl-0 list-unstyled">
            <li class="mb-2"><a class="text-color" href="404.jsp">Privacy Policy</a></li>
            <li class="mb-2"><a class="text-color" href="404.jsp">Terms & Conditions</a></li>
            <li class="mb-2"><a class="text-color" href="404.jsp">Cookie Policy</a></li>
            <li class="mb-2"><a class="text-color" href="404.jsp">Terms of Sale</a></li>
            <li class="mb-2"><a class="text-color" href="dashboard.jsp">Free Shipping & Returns</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-dark py-4">
    <div class="container">
      <div class="row">
        <div class="col-md-5 text-center text-md-left mb-4 mb-md-0 align-self-center">
          <p class="text-white mb-0">Logo &copy; 2018 all right reserved</p>
        </div>
        <div class="col-md-2 text-center text-md-left mb-4 mb-md-0">
          <img src="images/DLOGO.png" alt="logo">
        </div>
        <div class="col-md-5 text-center text-md-right mb-4 mb-md-0">
          <ul class="list-inline">
            <li class="list-inline-item"><img class="img-fluid rounded atm-card-img" src="images/payment-card/card-4.jpg" alt="card"></li>
            <li class="list-inline-item"><img class="img-fluid rounded atm-card-img" src="images/payment-card/card-2.jpg" alt="card"></li>
            <li class="list-inline-item"><img class="img-fluid rounded atm-card-img" src="images/payment-card/card-3.jpg" alt="card"></li>
            <li class="list-inline-item"><img class="img-fluid rounded atm-card-img" src="images/payment-card/PaytmLogo.jpg" alt="card"></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
 </footer> 
<!-- /footer -->
</body>
</html>