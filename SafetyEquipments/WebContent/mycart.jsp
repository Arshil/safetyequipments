<%@page import="com.safetyequipment.beans.User"%>
<%@page import="com.safetyequipment.beans.Cart"%>
<%@page import="com.safetyequipment.beans.Product"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Cart</title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</head>
<body>
<%-- <%HttpSession httpSession = request.getSession(false); User user = (User)httpSession.getAttribute("userlogin"); %> --%>
<% List<Cart> usercartlist = (ArrayList<Cart>)(request.getAttribute("usercartlist")); %>
<%@include file="header.jsp" %>
<div>
<table  id="example" class="display" style="width:100%" border="1">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Image</th>
<!--                 <th>Sub Category ID</th> -->
<!--                 <th>Category ID</th> -->
                <th>Product Name</th>
                <th>Category Name</th>
                <th>Sub Category Name</th>
                <th>Company Name</th>
                <th>Product Price</th>
                <th>Product Description</th>
                <th>Delete</th>
            </tr> 
        </thead>
        <tbody>
        
        <%int cnt =0; %>
        <% for(Cart cart : usercartlist){ %>
        <tr>   
     	<td><%= ++cnt %></td>
     	<% if(cart.getProduct().getImagestring()!=null){ %>
     	<td><img src="data:image/jpeg;base64,<%=cart.getProduct().getImagestring()%>" height="100" width="100"></td>
     	<%} else { %>
     	<td> </td>
     	<%} %>
<%--      	<td><%=cart.getProduct.getproductid() %></td> --%>
<%--      	<td><%=cart.getProduct.getCategoryid() %></td> --%>
     	<td><%=cart.getProduct().getProductname() %></td>
     	<td><%=cart.getProduct().getCategoryname() %></td>
     	<td><%=cart.getProduct().getSubcategoryname() %></td>
     	<td><%=cart.getProduct().getCompanyname() %></td>
     	<td><%=cart.getProduct().getProductprice() %></td>
     	<td><%=cart.getProduct().getDescription() %></td>
     	
<%--      	<td><a href="UpdateProductServlet?product_id=<%=product.getProductid() %>">Edit</a></td>	 --%>
     	<td><a href="DeleteUserCartProduct?cart_id=<%=cart.getCartid() %>">Delete</a></td>	    	
     	</tr>
        <% }%>
        
        
<!--                 </tfoot> -->
    </table>
    </div>
    <%@include file="footer.jsp" %>
</body>
</html>