<%@page import="com.safetyequipment.beans.Company"%>
<%@page import="com.safetyequipment.beans.Category"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Company</title>
</head>
<body>
<%Company company =(Company)(request.getAttribute("company")); %>
<%@include file="header.jsp" %>
<section class="signin-page account">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mx-auto">
        <div class="block text-center">
          <a class="logo" href="index-2.jsp">
            <img src="images/logo.png" alt="logo">
          </a>
          <h2 class="text-center">update company</h2>
          <form class="text-left clearfix" action="UpdateCompanyServlet" method="post" enctype="multipart/form-data">
            
            <div class="form-group">
              <input type="text" class="form-control"  value="<%= company.getCompanyname() %>" id="companyname" name="companyname">
            </div>
            
            <div class="form-group">
              <input type="text" class="form-control"  value="<%=company.getCompanyemail() %>" id="companyemailid" name="companyemailid">
            </div>
           
            <div class="form-group">
              <input type="text" class="form-control"  value="<%=company.getCompanycontact() %>" id="companycontact" name="companycontact">
            </div>
           
           
            <div class="form-group">
              <input type="text" class="form-control"  value="<%=company.getCompanyaddress() %>" id="companyaddress" name="companyaddress">
            </div>
           
         
                 
           <div class="form-group">
           <input type="hidden" class="form-control" value="<%=company.getCompanyid() %>" id="companyid" name="companyid" >
            </div>
                 
                 
                 
   
            <div class="text-center">
              <button type="submit" class="btn btn-primary">Update Changes</button>
           </div>  
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<%@include file="footer.jsp" %>
</body>
</html>