<%@page import="com.safetyequipment.beans.Complaint"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.safetyequipment.beans.Category"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="ISO-8859-1">
<title>Edit Complaint</title>
<script src="plugins/jQuery/jquery.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>

</head>
<body>


	<%@include file="header.jsp"%>
	<%
		HttpSession httpSessionuser1 = request.getSession(false);
		User userobj2 = (User) httpSessionuser1.getAttribute("userlogin");
		Complaint complaint = (Complaint)(request.getAttribute("complaint"));
		List<Complaint> productcomplaintlist = (ArrayList<Complaint>)(request.getAttribute("productcomplaintlist"));
	%>
	<section class="signin-page account">
		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index-2.jsp"> <img src="images/logo.png"
							alt="logo">
						</a>
						<h2 class="text-center">Edit Complaint</h2>
						<form class="text-left clearfix" action="EditUserComplaintServlet"
							method="post" enctype="multipart/form-data"
							onsubmit="return formvalidation()">
<!-- 							<input type="hidden" id="userid" name="userid" -->
<%-- 								value="<%=userobj2.getUserid()%>"> <input type="hidden" --%>
<!-- 								id="useremail" name="useremail" -->
<%-- 								value="<%=userobj2.getEmailid()%>"> --%>
							<input type="hidden"
								id="usercomplaintid" name="usercomplaintid"
								value="<%=complaint.getUsercomplaintid()%>">
								
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Order Id "
									required="required" id="orderid" name="orderid" value="<%=complaint.getOrderkey() %>" readonly="readonly"> <span
									id="order_id"></span>
							</div>



							<div class="form-group">
								<select name="productoption" id="productoption"
									onchange="getvalc(this);" onblur="return productvalidation()" disabled="disabled"> 
								<option value="<%=complaint.getProductid() %>"><%=complaint.getProductname() %></option>	
								
									<!-- getvalc(productoption) -->
								</select> <span id="product_name"></span>
							</div>


							<!-- 							<div class="form-group"> -->
							<!-- 								<input type="text" class="form-control" -->
							<!-- 									placeholder="Prouct quantity " id="productquantity" -->
							<!-- 									name="productquantity"> -->
							<!-- 							</div> -->

							<div class="form-group">
								<select name="complaintoption" id="complaintoption"
									onchange="return complaintvalidation()">
									<% for(Complaint complaint2 : productcomplaintlist){ %>
									<option value="<%=complaint2.getComplaintid() %>" <%if(complaint.getComplaintid() == complaint2.getComplaintid()){ %> selected="selected" <%} %>><%=complaint2.getName() %></option>
<!-- 									<option value="0" selected="selected">Select Product</option> -->
									<%} %>
									<!-- getvalp(subcategoryoption) -->
								</select> <span id="complaint_name"></span>
							</div>


							<div class="form-group">
								<input type="text" class="form-control"
									placeholder="Complaint Description" id="description"
									name="description">
								<p>*if you select "other" in complaint options then you have
									to complusarily write complaint description , otherwise it is
									mandatory .</p>
							</div>

							<div class="text-center">
								<button type="submit" id="eucsubmit" class="btn btn-primary">Make
									Complaint</button>
							</div>



						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@include file="footer.jsp"%>


	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('#orderid')
									.blur(
											function(event) {
												var oid = $('#orderid').val();
												var uid = $(
														'input:hidden[name=userid]')
														.val();
												//alert("oid : " + oid);
												//alert("user id :" + uid);
												$
														.get(
																'OrderIdValidationServlet',
																{
																	orderId : oid,
																	userId : uid
																},
																function(
																		responseText) {
																	//alert(responseText);
																	$(
																			'#OrderIdValidationServlet')
																			.text(
																					responseText);
																	if (responseText == "You can't make complaint because you have not paid payment") {
																		//alert(responseText);
																		//$( "#x" ).prop( "disabled", true );
																		$(
																				"#order_id")
																				.text(
																						responseText);

																	} //You can't make complaint because your order has been cancelled
																	else if (responseText == "You can't make complaint because your order has been cancelled") {
																		alert(responseText);
																		//order_id
																		$(
																				"#order_id")
																				.text(
																						responseText);
																	}
																	
																	else if (responseText == "You can't make complaint because your order has not shipped yet") {
																		alert(responseText);
																		//order_id
																		$(
																				"#order_id")
																				.text(
																						responseText);
																	} else if (responseText == "Invalid order id") {
																		//alert(responseText);
																		$(
																				"#order_id")
																				.text(
																						responseText);
																	} else {
																		$(
																				"#order_id")
																				.text(
																						"");

																		$(
																				'#productoption')
																				.find(
																						'option')
																				.remove()
																				.end()
																				.append(
																						'<option value="0">Select Product</option>')
																				.val(
																						'0');

																		$
																				.each(
																						JSON
																								.parse(responseText),
																						function(
																								i,
																								obj) {
																							$(
																									"#productoption")
																									.append(
																											new Option(
																													obj.productname,
																													obj.productid));
																						});

																	}
																});
											});
						});

		// 		$('#select_embed').prop('required',true);
		// if (document.getElementById("embed").checked){
		//             $('#div_embed_bundles').show('fast')
		//             
		//         }
		//         else {
		//             $('#div_embed_bundles').hide('fast')
		//             $('#select_embed').removeAttr('required');
		//         }
		function getvalc(productoption) {
			alert(productoption.value);
			$.get("GetComplaintListOfSelectedProduct", {
				productId : productoption.value
			}, function(data) {

				if (data == "select product option") {
					$('#complaintoption').find('option').remove().end().append(
							'<option value="0">Select Complaint</option>').val(
							'0');

				} else {
					$('#complaintoption').find('option').remove().end().append(
							'<option value="0">Select Complaint</option>').val(
							'0');

					$.each(JSON.parse(data), function(i, obj) {
						//use obj.id and obj.name here, for example:
						//              	   alert(obj.subcategoryid + "-" + obj.subcategoryname);   
						//             	   alert(obj);
						$("#complaintoption").append(
								new Option(obj.name, obj.complaintid));
					});
				}
			});
		}

		function productvalidation() {
			var productText = $("#productoption option:selected").html();
			if (productText == "Select Product") {
				document.getElementById('product_name').innerHTML == "* Please select any product";
				return false;
			} else {
				document.getElementById('product_name').innerHTML == "";
				return true;
			}
		}

		function complaintvalidation() {
			var complaintText = $("#complaintoption option:selected").html();
			if (complaintText == "Select Complaint") {
				document.getElementById('complaint_name').innerHTML == "* Please select any product";
				return false;
				//$('#description').prop('required',false);
			} else {
				if (complaintText == "Others") {
					$('#description').prop('required', true);
					//return true;
				}
				if (complaintText != "Others") {
					$('#description').prop('required', false);
					//return true;
				}
				document.getElementById('complaint_name').innerHTML == "";
				return true;
				//$('#description').prop('required',false);
			}

		}

		function formvalidation() {
			var complaintText = $("#complaintoption option:selected").html();
			var productText = $("#productoption option:selected").html();
			alert("complaint : " + complaintText);
			alert("product : " + productText);
			if ((document.getElementById('order_id').innerHTML == "")
					&& (document.getElementById('product_name').innerHTML == "")
					&& (document.getElementById('complaint_name').innerHTML == "")) {
				return true;
			} else if (productText == "Select Product") {
				document.getElementById('product_name').innerHTML == "* Please select any product";
				return false;
			} else if (complaintText == "Select Complaint") {
				document.getElementById('complaint_name').innerHTML == "* Please select any product";
				return false;
			}

		}
	</script>
</body>
</html>
