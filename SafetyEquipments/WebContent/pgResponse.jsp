<%@page import="com.safetyequipment.util.PaytmConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.util.*,com.paytm.pg.merchant.CheckSumServiceHelper"%>
<%
	Enumeration<String> paramNames = request.getParameterNames();
	String str = new String("");
	Map<String, String[]> mapData = request.getParameterMap();
	TreeMap<String, String> parameters = new TreeMap<String, String>();
	String paytmChecksum = "";
	while (paramNames.hasMoreElements()) {
		String paramName = (String) paramNames.nextElement();
		if (paramName.equals("CHECKSUMHASH")) {
			paytmChecksum = mapData.get(paramName)[0];
		} else {
			parameters.put(paramName, mapData.get(paramName)[0]);
		}
	}
	boolean isValideChecksum = false;
	String outputHTML = "";
	try {
		isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper()
				.verifycheckSum(PaytmConstants.MERCHANT_KEY, parameters, paytmChecksum);
		if (isValideChecksum && parameters.containsKey("RESPCODE")) {
			if (parameters.get("RESPCODE").equals("01")) {
				outputHTML = parameters.toString();

				System.out.println(outputHTML);
			} else {
				outputHTML = "<b>Payment Failed.</b>";
			}
		} else {
			outputHTML = "<b>Checksum mismatched.</b>";
		}
	} catch (Exception e) {
		outputHTML = e.toString();
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Page Response</title>
</head>
<body>
<% String orderid = ""; %>
	<form id="pgresponse" name="pgresponse">
		<p id="paymentDet"><%=outputHTML%></p>
		<%
			System.out.println("length : " + outputHTML.length());
		%>
		<%
			for (int i = 1; i < outputHTML.length() - 1; i++) {
				str += outputHTML.charAt(i);
		%>
		<%
			}
			System.out.println("str : " + str);
		%>
		<%
			String str1[] = str.split(",");
		%>
		<table border="1" style="color: green;" id="resonseTable" align="center">
<!-- 			<caption>Payment Details</caption> -->
		<th colspan="2"><center>Payment Details</center></th>
			<%
				for (int j = 0; j < str1.length; j++) {
			%>
			<%
				System.out.println("str1[" + j + "] : " + str1[j]);
			%>
			<tr>
				<%
					String values[] = str1[j].split("=");
				%>
				<td><%=values[0]%></td>
				<%if(values[0].equalsIgnoreCase(" ORDERID")){ %>
				<% orderid = values[1].trim(); %>
				<%} %>
				<td><%=values[1]%></td>
				<input type="hidden" name="<%=values[0].trim()%>" id="<%=values[0].trim()%>"
					value="<%=values[1].trim()%>">
			</tr>
			<%
				}
			%>
		</table>
		<!-- main wrapper -->
<div class="main-wrapper">

<!-- Page Wrapper -->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mx-auto">
        <div class="block text-center">
          <h3 class="text-center mb-3">Thank you! For your payment</h3>
          <p class="text-color">Your order has been placed and will be processed as soon as possible. Make sure you make note of your order number, which is <span class="text-primary"><%=orderid %></span> You will be receiving an email shortly with confirmation of your order. You can now:</p>
          <a href="shop.jsp" class="btn btn-primary mt-3 mx-2">Go To Shopping</a>
          <a href="track.jsp" class="btn btn-dark mt-3 mx-2">Track order</a>
        </div>
      </div>
    </div>
  </div>
</section><!-- /.page-warpper -->
	</form>
	<script src="plugins/jQuery/jquery.min.js"></script>
	<script type="text/javascript">
		// $("form").load(function() {
		// 			$.get('PaymentServlet', 
		// 				var outputhtml = document.getElementById("paymentDet").textContent;
		// 			}, function(responseText) {
		// 				$('#PaymentServlet').text(responseText);
		// 			});
		// });

		// $("form").load(function() {
		// 	alert("alert called in pgresponse");
		// 	var outputHtml = document.getElementById("paymentDet").textContent;
		// 	alert(outputHtml);
		// 	$.get('PaymentServlet', {
		// 		outputhtml : outputHtml
		// 	}, function(responseText) {
		// 		$('#PaymentServlet').text(responseText);
		// 	});
		// });

		$(window).on('load', function() {

			// 			 $('#resonseTable').find('tr').each(function (i, el) {
			// 			        var $tds = $(this).find('td'),
			// 			        KEY = $tds.eq(0).text(),
			// 		            VALUE = $tds.eq(1).text();

			// 			        alert(KEY);
			// 			        alert(VALUE);
			// 			        // do something with productId, product, Quantity
			// 			    });

			var bankName = $('input:hidden[name=BANKNAME]').val();
			//alert(bankName);

			var bankTxnId = $('input:hidden[name=BANKTXNID]').val();
			//alert(bankTxnId);

			var currenCy = $('input:hidden[name=CURRENCY]').val();
			//alert(currenCy);

			var getwayName = $('input:hidden[name=GATEWAYNAME]').val();
			//alert(getwayName);

			var merchantId = $('input:hidden[name=MID]').val();
			//alert(merchantId);

			var orderId = $('input:hidden[name=ORDERID]').val();
			//alert(orderId);

			var paymentMode = $('input:hidden[name=PAYMENTMODE]').val();
			//alert(paymentMode);

			var responseCode = $('input:hidden[name=RESPCODE]').val();
			//alert(responseCode);

			var responseMsg = $('input:hidden[name=RESPMSG]').val();
			//alert(responseMsg);

			var staTus = $('input:hidden[name=STATUS]').val();
			//alert(staTus);

			var txnAmt = $('input:hidden[name=TXNAMOUNT]').val();
			//alert(txnAmt);

			var txnId = $('input:hidden[name=TXNID]').val();
			//alert(txnId);

			$.get('PaymentServlet', {
				bankname : bankName,
				banktxnid : bankTxnId,
				currency : currenCy,
				gatewayname : getwayName,
				merchantid : merchantId,
				orderid : orderId,
				paymentmode : paymentMode,
				responsecode : responseCode,
				responsemsg : responseMsg,
				status : staTus,
				txnamount : txnAmt,
				txnid : txnId				
			}, function(responseText) {
				$('#PaymentServlet').text(responseText);
			});

			// code here
		});

		// 		$(document).ready(function() {
		// 				$("form").load(function() {
		// 					alert("called");
		// 						var outputHtml = document.getElementById("paymentDet").textContent;
		// 							$.get('PaymentServlet', {
		// 								outputhtml : outputHtml
		// 						}, function(responseText) {
		// 							 $('#PaymentServlet').text(responseText);
		// 					});
		// 				});
		// 	 	});
	</script>
</body>
</html>