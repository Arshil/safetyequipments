<%@page import="com.safetyequipment.beans.Cart"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html lang="zxx">
<!-- Mirrored from demo.themefisher.com/elite-shop/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:41 GMT -->
<head>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
<title>My Cart</title>
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script
	src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function() {
		$('#example').DataTable();
	});
</script>
</head>

<body>
	<%
		List<Cart> usercartlist = (ArrayList<Cart>) (request.getAttribute("usercartlist"));
	%>
	<%int i = usercartlist.size();%>

	<%
		int mainTotal = 0;
	%>
	<!-- <form method="post" action="pgRedirect.jsp"> -->


	<%@include file="header.jsp"%>
	<!--   <!-- preloader start -->

	<!--   <div class="preloader"> -->
	<!--     <img src="images/preloader.gif" alt="preloader"> -->
	<!--   </div> -->
	<!--   <!-- preloader end -->



	<!-- main wrapper -->
	<div class="main-wrapper">

		<!-- breadcrumb -->
		<nav class="bg-gray py-3">
			<div class="container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.jsp">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Cart</li>
				</ol>
			</div>
		</nav>
		<!-- /breadcrumb -->

		<div class="section">
			<div class="cart shopping">
				<div class="container">
					<div class="row">
						<div class="col-md-10 mx-auto">
							<div class="block">
								<div class="product-list">
									
										<div class="table-responsive">
											<%
												int cnt = 0;
											%>
									<%if(i!=0){ %>
											<table class="table cart-table" id="cartTable">
									
												<thead>
													<tr>
														<th>Sr.No</th>
														<th>Product</th>
														<th>Product Name</th>
														<th>Price</th>
														<th>Quantity</th>
														<th>Sub Total</th>
														
													</tr>
												</thead>
												<%
													for (Cart cart : usercartlist) {
												%>

												<input type="hidden" id="userid" name="userid"
													value="<%=cart.getUserid()%>">

												<input type="hidden" class="cartid" id="cartid"
													name="cartid" value="<%=cart.getCartid()%>">
												<tbody>
												<tr class="cartdata">
													<td><%=++cnt%></td>
													<td><div class="product-info"
															style="height: 100px; width: 100px;">
															<img class="img-fluid"
																src="data:image/jpeg;base64,<%=cart.getProduct().getImagestring()%>"
																alt="product-img" />
														</div></td>

													<td><label><%=cart.getProduct().getProductname()%></label></td>

													<td><label class="productPrice"><%=cart.getProduct().getProductprice()%></label></td>

													<td>
<!-- 													<div class="input-group bootstrap-touchspin bootstrap-touchspin-injected"> -->
<!-- 													<span class="input-group-btn input-group-prepend"> -->
<!-- 													<input class="btn btn-primary bootstrap-touchspin-down" type="button" value="-"></button> -->
<!-- 													</span> -->
													<input type="number" class="productuserquantity"
														value="<%=cart.getQuatity()%>"
														id="<%=cart.getProductid()%>" min="1"
														max="<%=cart.getProduct().getQuantity()%>"
														name="productuserquantity" />
<!-- 													<span class="input-group-btn input-group-append"> -->
<!-- 													<input class="btn btn-primary bootstrap-touchspin-up" type="button" value="+"></button> -->
<!-- 													</span>	 -->
<!-- 													</div> -->
														</td>


													<td><label class="subTotal"><%=(cart.getQuatity()) * (cart.getProduct().getProductprice())%></label></td>

												<td><a class="product-remove"
														href="DeleteUserCartProduct?cart_id=<%=cart.getCartid()%>">&times;</a></td>
													<%mainTotal += (cart.getQuatity()) * (cart.getProduct().getProductprice());%>

													<%
														}
													%>	
																					
												</tr>
												<!-- 					<td><label>txnAmount*</label></td> -->

											</tbody>

											</table>
										</div>
										<!-- 										<hr> -->
										<!-- 										<div class="d-flex flex-column flex-md-row align-items-center"> -->
										<!-- 											<input type="text" -->
										<!-- 												class="form-control text-md-left text-center mb-3 mb-md-0" -->
										<!-- 												name="coupon" id="coupon" -->
										<!-- 												placeholder="I have a discout coupon"> -->
										<!-- 											<button -->
										<!-- 												class="btn btn-outline-primary ml-md-3 w-100 mb-3 mb-md-0">Apply -->
										<!-- 												Coupon</button> -->
										<!-- 											<a href="#" class="btn ml-md-4 btn-dark w-100">Update -->
										<!-- 												Cart</a> -->
										<!-- 										</div> -->
										<!-- 										<hr> -->
										<hr>
										<div class="row">
											<div class="col-12">
												<ul class="list-unstyled text-right">

													<li>Sub Total &#x20b9; <label name="subtotal" id="subtotal"
														value="0"></label></li>
													<!-- 												<span class="d-inline-block w-100px"></span>   <span class="d-inline-block w-100px"></span>-->

													<!-- 													<li>UK Vat <span class="d-inline-block w-100px">$10.00</span></li> -->
													<li class="grandTotal"><label class="grandtotal"
														id="grandtotal">Grand Total &#x20b9;<%=mainTotal%></label></li>


												</ul>
											</div>
										</div>

										<input type="hidden" class="hiddentotal" id="hiddentotal"
											name="TXN_AMOUNT" value="<%=mainTotal%>">
										<a href="shippinginfo.jsp"><input
											type="submit" class="btn btn-primary float-right" 
											value="Checkout"></a>
									
											<%}else{ %>
											<li style="align-content: center;margin-left: 30%; color: red;font-family: cursive;font-size:large;">NO ITEMS IN YOUR CART...START SHOPPING</li>
											<%} %>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer -->
		<footer class="bg-light">
			<%@include file="footer.jsp"%>
		</footer>
		<!-- /footer -->

	</div>
	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
		$(document).ready(
				function() {

					var grandtotal = 0;
					$('.productuserquantity').change(
							function() {
								var productId = $(this).attr('id');
								var productQty = $(this).val();
								var label = $("label");
								var userId = $('input:hidden[name=userid]')
										.val();
								//var cartId = $('input:hidden[name=cartid]').val();
								var productPrice = $(this).closest('td').prev(
										'td').find(label).text();
								var products = $('.totalProducts').val();
								console.log('product id :' + productId);
								console.log(productQty);
								console.log(productPrice);
								// console.log(products);
								var quantity = 0;
								var productprice = 0;
								var subTotal = productPrice * productQty;
								console.log(subTotal);
								console.log('user id :' + userId);
								//console.log('cart id :'+cartId);
								//grandtotal = grandtotal + subTotal ;

								$(this).closest('td').next('td').find(label)
										.text(subTotal);

								var grossTotal = 0;

								$("#cartTable tr.cartdata").each(
										function() {
											var rowItemPrice = $(this).find(
													'td').eq(3).text();

											var qty = $(this).find('td').find(
													'.productuserquantity')
													.val();
											grossTotal += parseInt(rowItemPrice
													* qty);

										});

								console.log('hello' + grossTotal);

								$('input[name="TXN_AMOUNT"]').attr('value',
										grossTotal);
								$('#grandtotal').val(grossTotal)
								$("#grandtotal").html(grossTotal)
								//productId = $(this).attr('id');
								console.log('product id 2 :' + productId);
								$.get("UpdateCartQuantityServlet", {
									productID : productId,
									userID : userId,
									productQTY : productQty
								}, function(responseText) {
									// $('#UpdateCartQuantityServlet').text(responseText);
								});

							});

					$("input").blur(function() {
					//	alert("The text has been changed.");

					});

				});
	</script>


	</form>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:42 GMT -->
</html>