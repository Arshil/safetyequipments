<%@page import="java.util.ArrayList"%>
<%@page import="com.safetyequipment.beans.Cart"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Review Order</title>
<link href="css/style.css" rel="stylesheet">

</head>
<body>
<jsp:include page="/ReviewOrderServlet" />
<% List<Cart> cartlist = (ArrayList)(request.getAttribute("cartlist")); %>
<% int c = 0; %>
<%@include file="header.jsp"%>
 <!-- Main Stylesheet -->



<%-- 					<h1><%=cartlist.size() %></h1> --%>

                    <!-- reeview -->
                    <h3>Order Review</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                	<td>No. </td>
                                    <td></td>
                                    <td>Product Name</td>
                                    <td>Quantity</td>
                                    <td>Price</td>
                                    <td>Sub Total</td>
                                </tr>
                            </thead>
                            <tbody>
                            
                             <% for(Cart cart : cartlist) {%> 
                                 <tr> 
                                 
                                 <td><%=++c %></td>
<%--                                      <td class="align-middle"><img class="img-fluid" src="data:image/jpeg;base64,<%=cart.getProduct().getImagestring()%>" alt="product-img" /></td>  --%>
                                    <td><div class="product-info"
															style="height: 100px; width: 100px;">
															<img class="img-fluid"
																src="data:image/jpeg;base64,<%=cart.getProduct().getImagestring()%>"
																alt="product-img" />
														</div></td>
                                     <td class="align-middle"><%=cart.getProduct().getProductname() %></td> 
                                     <td class="align-middle"><%=cart.getQuatity() %></td> 
                                     <td class="align-middle"><%=cart.getProduct().getProductprice() %></td> 
                                     <td class="align-middle"><%= (cart.getQuatity())*(cart.getProduct().getProductprice()) %></td> 
                                 </tr> 
                                 <%} %> 
<!--                                 <tr> -->
<!--                                     <td class="align-middle"><img class="img-fluid" src="images/cart/product-2.jpg" alt="product-img" /></td> -->
<!--                                     <td class="align-middle">Jacket</td> -->
<!--                                     <td class="align-middle">1</td> -->
<!--                                     <td class="align-middle">$520.00</td> -->
<!--                                 </tr> -->
<!--                                 <tr> -->
<!--                                     <td class="align-middle"><img class="img-fluid" src="images/cart/product-1.jpg" alt="product-img" /></td> -->
<!--                                     <td class="align-middle">Tops</td> -->
<!--                                     <td class="align-middle">1</td> -->
<!--                                     <td class="align-middle">$220.00</td> -->
<!--                                 </tr> -->
<!--                                 <tr> -->
<!--                                     <td class="align-middle"><img class="img-fluid" src="images/cart/product-2.jpg" alt="product-img" /></td> -->
<!--                                     <td class="align-middle">Jacket</td> -->
<!--                                     <td class="align-middle">1</td> -->
<!--                                     <td class="align-middle">$520.00</td> -->
<!--                                 </tr> -->
                            </tbody>
                        </table>
                    </div>
                    
                    	
                    <!-- /review -->
<input type="button" onclick="return CallCartServlet();" value="Update Cart" class="btn btn-primary float-right">

<nav class="bg-gray py-3">

<!-- <div style="position: absolute; width: 80%;" > -->

  <div class="container" align="justify"  >
<ol class="breadcrumb">
<li class="breadcrumb-item active" aria-current="page" >Shipping Information</li>
 </ol>
</div>
</nav>
<!-- class="col-md-6" -->
                    <!-- shipping-information -->
                    <h3 class="mb-5 border-bottom pb-2" align="center">Shipping Information</h3>
                    <div class="row mb-5" align="right">
                        <div  class="border-box p-4" align="justify" >
                            <h4 class="mb-3">Shipping Address</h4>
                            <ul class="list-unstyled">
                            
                                 <li><%=userobj.getFirstname() + " " + userobj.getLastname() %></li> 
 								
    							<li><input type="checkbox" checked="checked" name="chkAddress" ><%=userobj.getAddress() %></li> 
                         	 <li> <%=userobj.getPincode() %></li> 
   							
   							<li><input type="checkbox" id="chkAddress" name="chkAddress"><b>change delivery address?</b></li>   
        					 <input type="text" id="dvAddress" style="display: none" placeholder="Enter new shipping address">                    
                             <br><input type="text"  id="dvPincode" style="display: none" placeholder="Enter Pincode">                    
                             
                             <li>Mo.<%=userobj.getMobileno() %></li>
                             <li><%=userobj.getEmailid() %></li>
                            </ul>
                        </div>
                                            
                        <div class="border-box p-4" >
                            <h4 class="mb-3">Shipping Method</h4>
                            <ul class="list-unstyled">
                                <li>Delivered in 8-10 business days. </li>
                            </ul>
                        </div>
                    </div>
                    
                    
                 <div class="col-md-4">
                <div class="border-box p-4">
                    <h4>Order Summary</h4>
                    <p>Excepteur sint occaecat cupidat non proi dent sunt.officia.</p>
                    <ul class="list-unstyled">
                        <li class="d-flex justify-content-between">
                            <span>Subtotal</span>
                            <span>$237.00</span>
                        </li>
                        <li class="d-flex justify-content-between">
                            <span>Shipping & Handling</span>
                            <span>$15.00</span>
                        </li>
                        <li class="d-flex justify-content-between">
                            <span>Estimated Tax</span>
                            <span>$0.00</span>
                        </li>
                    </ul>
                    <hr>
                    <div class="d-flex justify-content-between">
                        <span>Total</span>
                        <strong>USD $253.00</strong>
                    </div>
                </div>
            </div>                
            
           
			
<!-- </div> -->
                   
<!--           <div id="dvAddress" style="display: none"> -->
<!--         New Shipping Address: -->
<!--         <input type="text" id="txtPassportNumber" /> -->
<!--     </div> -->
<div>
<%@include file="footer.jsp" %>
</div>
</div>

<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
<script type="text/javascript">
$(function () {
    $("#chkAddress").click(function () {
        if ($(this).is(":checked")) {
            $("#dvAddress").show();
            $("#dvPincode").show(); 
            } else {
            $("#dvAddress").hide();
            $("#dvPincode").hide();
        }
    });
});
//
function CallCartServlet()
{
	document.location.href="UserCartListServlet"; 
}
</script>
</body>
</html>