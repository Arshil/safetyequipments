<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/about.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:42 GMT -->
<head>
  <meta charset="utf-8">
  <title>Dipen Safety</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>

  <!-- preloader start -->
<!--   <div class="preloader"> -->
<!--     <img src="images/preloader.gif" alt="preloader"> -->
<!--   </div> -->
  <!-- preloader end -->

<!-- header -->
<%@include file="header.jsp" %>
<!-- /header -->

<!-- /navigation -->

<!-- main wrapper -->
<div class="main-wrapper">

<!-- breadcrumb -->
<nav class="bg-gray py-3">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index-2.jsp">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">About Us</li>
    </ol>
  </div>
</nav>
<!-- /breadcrumb -->

<!-- about -->
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <!-- image circle background -->
        <div class="about-img-bg"></div>
        <img class="img-fluid mb-4 mb-md-0" src="images/about/product.jpg" alt="product-img">
      </div>
      <div class="col-md-6">
        <h2 class="section-title">Welcome to Our World</h2>
        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
        <ul class="pl-0">
          <li class="d-flex">
            <i class="ti-check mr-3 mt-1"></i>
            <div>
              <h4>Free Gift Card</h4>
              <p>Gift cards are free. Claim them now!</p>
            </div>
          </li>
          <li class="d-flex">
            <i class="ti-check mr-3 mt-1"></i>
            <div>
              <h4>Best Delivery</h4>
              <p>Free Shipping On All US Orders</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- /about -->

<section class="section overlay cta" data-background="images/backgrounds/cta.jpg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="text-white mb-2">End of Season Sale</h1>
        <p class="text-white mb-4">Take 25% off all sweaters and knits. Discount applied at checkout.</p>
        <a href="shop.jsp" class="btn btn-light">shop now</a>
      </div>
    </div>
  </div>
</section>

<!-- team -->
<section class="section">
  <div class="container">
    <div class="row justify-content-center">
      <!-- team member -->
      <div class="col-lg-4 col-sm-6 mb-5 mb-lg-0">
        <div class="card border-0 text-center team-member">
          <img class="card-img-top rounded-0 mb-1" src="images/team/team-member-1.jpg" alt="team-member">
          <div class="card-body">
            <h4 class="card-title">Monica Gilbert</h4>
            <p class="card-text">Founder</p>
          </div>
          <ul class="list-inline social-icons">
            <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-vimeo-alt"></i></a></li>
          </ul>
        </div>
      </div>
      <!-- team member -->
      <div class="col-lg-4 col-sm-6 mb-5 mb-lg-0">
        <div class="card border-0 text-center team-member">
          <img class="card-img-top rounded-0 mb-1" src="images/team/team-member-2.jpg" alt="team-member">
          <div class="card-body">
            <h4 class="card-title">Danial bolt</h4>
            <p class="card-text">Designer</p>
          </div>
          <ul class="list-inline social-icons">
            <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-vimeo-alt"></i></a></li>
          </ul>
        </div>
      </div>
      <!-- team member -->
      <div class="col-lg-4 col-sm-6 mb-5 mb-lg-0">
        <div class="card border-0 text-center team-member">
          <img class="card-img-top rounded-0 mb-1" src="images/team/team-member-3.jpg" alt="team-member">
          <div class="card-body">
            <h4 class="card-title">Rendy Howl</h4>
            <p class="card-text">Developer</p>
          </div>
          <ul class="list-inline social-icons">
            <li class="list-inline-item"><a href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="ti-vimeo-alt"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /team -->

<!-- footer -->
 <%@include file="footer.jsp"%>
<!-- /footer -->

</div>
<!-- /main wrapper -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/about.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:43 GMT -->
</html>