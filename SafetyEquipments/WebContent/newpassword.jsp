<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/login.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:18:55 GMT -->
<head>
<meta charset="utf-8">
<title>Dipen Safety</title>

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- ** Plugins Needed for the Project ** -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>
<% int userid = (int)(request.getAttribute("userid")); %>
	<!-- preloader start -->
	<!--   <div class="preloader"> -->
	<!--     <img src="images/preloader.gif" alt="preloader"> -->
	<!--   </div> -->
	<!-- preloader end -->

	<section class="signin-page account">

		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index-2.jsp"> <img src="images/logo.png"
							alt="logo">
						</a>
						<!--           <h2 class="text-center">Welcome Back</h2> -->
						<form class="text-left clearfix" action="NewPasswordServlet"
							method="post"  onsubmit="return formvalidation()">
							
							<div class="form-group">
								<input type="hidden" class="form-control"
									value="<%=userid %>" name="userid" id="userid">
							</div>


							<div class="form-group">
								<input type="password" class="form-control"
									placeholder="New Password" name="newpassword" id="newpassword"
									onblur="return newpasswordvalidation()"> <span
									id="new_pass_word"></span>
							</div>
							
							<div class="form-group">
								<input type="password" class="form-control"
									placeholder="Confirm New Password" name="newconfirmpassword"
									id="newconfirmpassword"
									onblur="return confirmnewpasswordvalidation()"> <span
									id="new_confirm_password"></span>
							</div>

							<div class="text-center">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</form>
						<!--           <p class="mt-3">New in this site ?<a href="signin.jsp"> Create New Account</a></p> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	
	<script type="text/javascript">


		function newpasswordvalidation() {
			var password = document.getElementById('newpassword').value;
			if (password == "") {
				document.getElementById('new_pass_word').innerHTML = "*Please enter new password  ";
				return false;
			}
			if ((password.length > 8) || (password.length < 8)) {
				document.getElementById('new_pass_word').innerHTML = "* the length of new password must be 8 ";
				return false;
			} else {
				document.getElementById('new_pass_word').innerHTML = "";
				return true;
			}
		}

		function confirmnewpasswordvalidation() {
			var password = document.getElementById('newpassword').value;
			var confirmpassword = document.getElementById('newconfirmpassword').value;
			if (confirmpassword == "") {
				document.getElementById('new_confirm_password').innerHTML = "*Please enter confirm new password  ";
				return false;
			}
			if ((password.length > 8) | (password.length < 8)) {
				document.getElementById('new_confirm_password').innerHTML = "*the length of confirm new password must be 8 ";
				return false;
			}
			if (password != confirmpassword) {
				document.getElementById('new_confirm_password').innerHTML = "* new password and confirm new password should be same .";
				return false;
			} else {
				document.getElementById('new_confirm_password').innerHTML = "";
				return true;
			}
		}

		function formvalidation() {
			if (((document.getElementById('new_pass_word').innerHTML == "")
					&& (document.getElementById('new_confirm_password').innerHTML == "")) {
				return true;
			} else {
				return false;
			}
		}
	</script>
	
	
	
	
	
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/login.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:18:55 GMT -->
</html>