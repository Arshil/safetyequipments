<%@page import="com.safetyequipment.beans.OrderDetails"%>
<%@page import="com.safetyequipment.beans.MyOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.String"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Orders</title>
<link href="css/style.css" rel="stylesheet">
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  <link rel="icon" href="images/logo.png" type="image/x-icon">

</head>
<body>
	<%@include file="header.jsp"%>
	<%
		MyOrder myOrder = (MyOrder) (request.getAttribute("myorder"));
	%>
	<%
		HttpSession httpSessionuser1 = request.getSession(false);
		User userobj1 = (User) httpSessionuser.getAttribute("userlogin");
	%>

	<h5 style="margin-left: 18%; color: green;"><%=myOrder.getOrder().getOrderkey() %></h5>
	<div>
		<div class="table-responsive"
			style="width: 23cm; left: 50%; display: inline-block; margin-left: 18%; margin-top: 0%;">
			<table class="table">
<!-- 				style="border-color: black; border-style: solid;" -->
				<thead>
					<tr>
						<td>No.</td>
						<td>Product Image</td>
						<td>Product Name</td>
						<td>Quantity</td>
						<td>Price (In &#x20b9; )</td>
						<td>Sub Total</td>
					</tr>
				</thead>
				<tbody>
					<%
						int count = 0;
					%>
					<%
						float total = 0;
					%>
					<%
						for (OrderDetails orderDetails : myOrder.getOrderdetailslist()) {
					%>
					<tr>
						<td><%=++count%></td>
						<td><div class="product-info"
								style="height: 100px; width: 100px;">
								<img class="img-fluid"
									src="data:image/jpeg;base64,<%=orderDetails.getImagestring()%>"
									alt="product-img" />
							</div></td>
						<td><%=orderDetails.getProductname()%></td>
						<td><%=orderDetails.getProductquantity()%></td>
						<td><%=orderDetails.getProductprice()%></td>
						<td><%=orderDetails.getProductquantity() * orderDetails.getProductprice()%></td>
					</tr>
					<%
						total += orderDetails.getProductquantity() * orderDetails.getProductprice();
					%>
					<%
						}
					%>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
		    			<td>Grand Total </td>
						<td>&#x20b9;<%=total%></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>


	<div class="border-box p-4" style="width: 450px;">
		<h4 class="mb-3">Order Summary</h4>
		<ul class="list-unstyled">
			<li class="d-flex justify-content-between"><span>Order Id</span>
				<span><%=myOrder.getOrder().getOrderkey()%></span></li>
			<li class="d-flex justify-content-between"><span>Order
					Date</span> <span><%=myOrder.getOrder().getDatetime()%></span></li>

			<li class="d-flex justify-content-between"><span>Amount
					(In &#x20b9;)</span> <span><%=myOrder.getOrder().getTransaction_amount()%></span></li>
		</ul>
	</div>

				<%-- 					<li>Mo.<%=userobj.getMobileno()%></li> --%>
				<%-- 					<li><%=userobj.getEmailid()%></li> --%>


		<div class="border-box p-4" align="justify" style="width: 350px;margin-top: -170px;margin-left: 460px;">
			<h4 class="mb-3">Shipping Address</h4>
			<ul class="list-unstyled">
				<li><%=myOrder.getShipping().getAddress()%></li>
			</ul>
		</div>
		
			
	<div class="row mb-5" align="right" style="width: 450px;margin-top: -140px;margin-left: 820px;">
		<div class="border-box p-4" align="justify">
			<h4 class="mb-2">Shipping Date</h4>
			<ul class="list-unstyled">
				<% 	if (myOrder.getShipping().getShippingdate().equalsIgnoreCase("In 7-10 working days")) { %>
				<%System.out.print(" user type : "+userobj1.getUsertype()); %>
					<li>Delivered <%=myOrder.getShipping().getShippingdate() %>.</li> 	
<!-- 				<li>Delivered in 8-10 business days.</li> -->
				<% if (userobj1.getUsertype().equalsIgnoreCase("A")) { %>
				<li><a href="AddShippingServlet?orderkey=<%=myOrder.getShipping().getOrderkey() %>">Add to Shipping </a></li>
				<% 	} } else { 	%>
				<li><%=myOrder.getShipping().getShippingdate()  + " "+userobj1.getUsertype() %></li>
				<%if(userobj1.getUsertype().equalsIgnoreCase("A")) { %>
				<li><a href="EditShippingDateServlet?orderkey=<%=myOrder.getShipping().getOrderkey() %>">Edit Shipping </a></li>
				<% }} %>
			</ul>
			</div>
		</div>
			
		<div class="row mb-5" align="right" style="width: 450px;margin-top: -186px;margin-left: 1107px;">
		<div class="border-box p-4" align="justify">
			
			<h4 class="mb-3">Shipping Status</h4>
			<ul>
				<li><%=myOrder.getShipping().getShippingstatus()%></li>
				<%
					if ((myOrder.getShipping().getShippingstatus().equalsIgnoreCase("pending")) && (userobj1.getUsertype().equalsIgnoreCase("A"))) {
				%>
				<li><a 
					href="UpdateShippingStatusServlet?orderkey=<%=myOrder.getShipping().getOrderkey()%>">Update
						Status</a></li>
				<%
					}
				%>	
			</ul>
		</div>
	</div>

	<%-- 		<%if(myOrder.getShipping().getPaymentstutus().equals("completed")) {%> --%>

	<%
		if ((myOrder.getShipping().getPaymentstutus().equalsIgnoreCase("completed"))) {
	%>
	<div class="border-box p-4" style="margin-left: 1308px;width: 200px;margin-top: -167px;">
		<h4 class="mb-3">Payment Status</h4>
		<ul class="list-unstyled">
			<li><%=myOrder.getShipping().getPaymentstutus()%></li>
		</ul>
	</div>
	<%
		}
	%>
	</div>
	<%
		if (myOrder.getShipping().getPaymentstutus().equals("completed")) {
			if (myOrder.getPayment().getPaymentmode().equals("COD")) {
	%>

	<div class="table-responsive"
		style="width: 23cm; left: 50%; display: inline-block; margin-left: 18%; margin-top: 0%;">
		<table class="table" style="border-color: black; border-style: solid;">
			<thead>
				<tr>
					<td>Currency</td>
					<td>Order Id</td>
					<td>Payment Mode</td>
					<td>Status</td>
					<td>Amount (In &#x20b9; )</td>
					<td>Txn Date</td>
					<td>Txn Id</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=myOrder.getPayment().getCurrency()%></td>
					<td><%=myOrder.getPayment().getOrderid()%></td>
					<%-- 						<td><%=myOrder.getPayment().getPaymentmode() %></td> --%>
					<td>Cash On Delievery</td>
					<td><%=myOrder.getPayment().getStatus()%></td>
					<td><%=myOrder.getPayment().getTxnamount()%></td>
					<td><%=myOrder.getPayment().getTxndatetime()%></td>
					<td><%=myOrder.getPayment().getTxnid()%></td>
				</tr>
			</tbody>
		</table>
	</div>

	<%
		} else {
	%>

	<!-- 	bank_name, bank_txn_id, currency, gateway_name, merchant_id, order_id,  -->	
<div style="display: block;margin-left: 550px;margin-top: 5%;">
<h4 class="mb-3" style="color: green;font-family: sans-serif;">Gateway Transaction Summary</h4>
	<table style="border: thin;border-style: solid;border-color: threedface;">
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<th>Field</th>
			<th>Value</th>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Bank_name</td>
			<td><%=myOrder.getPayment().getBankname()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Bank_txn_id</td>
			<td><%=myOrder.getPayment().getBanktxnid()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">currency</td>
			<td><%=myOrder.getPayment().getCurrency()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Gateway_name</td>
			<td><%=myOrder.getPayment().getGatewayname()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Merchant_id</td>
			<td><%=myOrder.getPayment().getMerchantid()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Order_id</td>
			<td><%=myOrder.getPayment().getOrderid()%></td>
		</tr>
		<!-- 	payment_mode, response_code, response_message, status, txn_amount, txn_date, txn_id -->
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Payment_mode</td>
			<td><%=myOrder.getPayment().getPaymentmode()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Response_code</td>
			<td><%=myOrder.getPayment().getResponsecode()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Response_message</td>
			<td><%=myOrder.getPayment().getResponsemsg()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">status</td>
			<td><%=myOrder.getPayment().getStatus()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Txn_amount</td>
			<td><%=myOrder.getPayment().getTxnamount()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Txn_date</td>
			<td><%=myOrder.getPayment().getTxndatetime()%></td>
		</tr>
		<tr style="border: thin;border-style: solid;border-color: threedface;">
			<td style="border: thin;border-style: solid;border-color: threedface;">Txn_id</td>
			<td><%=myOrder.getPayment().getTxnid()%></td>
		</tr>
	</table>
</div>
	<%
		}
		}
	%>
	<div>
		<%@include file="footer.jsp"%>
	</div>
	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
</body>
</html>