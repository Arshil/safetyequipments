<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/review.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:42 GMT -->
<head>
  <meta charset="utf-8">
  <title>Dipen Safety</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>

  <!-- preloader start -->
<!--   <div class="preloader"> -->
<!--     <img src="images/preloader.gif" alt="preloader"> -->
<!--   </div> -->
  <!-- preloader end -->

<!-- header -->
<%@include file="header.jsp" %>
<!-- /top-header -->
<!-- /navigation -->
<!-- main wrapper -->


<div class="main-wrapper">

<!-- breadcrumb -->
<nav class="bg-gray py-3">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index-2.jsp">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Shipping Information</li>
    </ol>
  </div>
</nav>
<!-- /breadcrumb -->

<!-- ORDER-REVIEW -->
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="inner-wrapper border-box">
                    <!-- navbar -->
                    <div class="justify-content-between nav mb-5">
                        <a href="shipping.jsp" class="text-center d-inline-block nav-item">
                            <i class="ti-truck d-block mb-2"></i>
                            <span class="d-block h4">Shipping Method</span>
                        </a>
                        <a href="payment.jsp" class="text-center d-inline-block nav-item">
                            <i class="ti-wallet d-block mb-2"></i>
                            <span class="d-block h4">Payment Method</span>
                        </a>
                        <a href="review.jsp" class="text-center d-inline-block nav-item active">
                            <i class="ti-eye d-block mb-2"></i>
                            <span class="d-block h4">Review</span>
                        </a>
                    </div>
                    <!-- /navbar -->

                    <!-- review -->
                    <h3>Order Review</h3>
                    <div class="table-responsive" >
                        <table class="table">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Product Name</td>
                                    <td>Quantity</td>
                                    <td>Sub Total</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="align-middle"><img class="img-fluid" src="images/cart/product-1.jpg" alt="product-img" /></td>
                                    <td class="align-middle">Tops</td>
                                    <td class="align-middle">1</td>
                                    <td class="align-middle">$220.00</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"><img class="img-fluid" src="images/cart/product-2.jpg" alt="product-img" /></td>
                                    <td class="align-middle">Jacket</td>
                                    <td class="align-middle">1</td>
                                    <td class="align-middle">$520.00</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"><img class="img-fluid" src="images/cart/product-1.jpg" alt="product-img" /></td>
                                    <td class="align-middle">Tops</td>
                                    <td class="align-middle">1</td>
                                    <td class="align-middle">$220.00</td>
                                </tr>
                                <tr>
                                    <td class="align-middle"><img class="img-fluid" src="images/cart/product-2.jpg" alt="product-img" /></td>
                                    <td class="align-middle">Jacket</td>
                                    <td class="align-middle">1</td>
                                    <td class="align-middle">$520.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /review -->

                    <!-- shipping-information -->
                    <h3 class="mb-5 border-bottom pb-2">Shipping Information</h3>
                    <div class="row mb-5">
                        <div class="col-md-4">
                            <h4 class="mb-3">Shipping Address</h4>
                            <ul class="list-unstyled">
                                <li>Somrat</li>
                                <li>Mohammadpur, Dhaka 120, Bangladesh </li>
                                <li>248-321-5879 </li>
                                <li>example.site@email.com</li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h4 class="mb-3">Shipping Method</h4>
                            <ul class="list-unstyled">
                                <li>Standard Ground (USPS) - $9.50 </li>
                                <li>Delivered in 8-10 business days. </li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h4 class="mb-3">Payment Method</h4>
                            <ul class="list-unstyled">
                                <li>Credit Card: </li>
                                <li>**** **** **** 2637</li>
                            </ul>
                        </div>
                    </div>

                    <!-- buttons -->
                    <div class="p-4 bg-gray d-flex justify-content-between">
                            <a href="payment.jsp" class="btn btn-dark">Back</a>
                        <a href="confirmation.jsp" class="btn btn-primary">Continue</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="border-box p-4">
                    <h4>Order Summery</h4>
                    <p>Excepteur sint occaecat cupidat non proi dent sunt.officia.</p>
                    <ul class="list-unstyled">
                        <li class="d-flex justify-content-between">
                            <span>Subtotal</span>
                            <span>$237.00</span>
                        </li>
                        <li class="d-flex justify-content-between">
                            <span>Shipping & Handling</span>
                            <span>$15.00</span>
                        </li>
                        <li class="d-flex justify-content-between">
                            <span>Estimated Tax</span>
                            <span>$0.00</span>
                        </li>
                    </ul>
                    <hr>
                    <div class="d-flex justify-content-between">
                        <span>Total</span>
                        <strong>USD $253.00</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /ORDER-REVIEW -->

<!-- footer -->
 <%@include file="footer.jsp"%> 
<!-- /footer -->

</div>
<!-- /main wrapper -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/review.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:42 GMT -->
</html>