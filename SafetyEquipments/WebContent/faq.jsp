<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/faq.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:55 GMT -->
<head>
  <meta charset="utf-8">
  <title>Dipen Safety</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>

  <!-- preloader start -->
<!--   <div class="preloader"> -->
<!--     <img src="images/preloader.gif" alt="preloader"> -->
<!--   </div> -->
  <!-- preloader end -->

<!-- header -->
<%@include file="header.jsp" %>
<!-- /top-header -->

<!-- navigation -->
<!-- <nav class="navbar navbar-expand-lg navbar-light bg-white w-100" id="navbar"> -->
<!--   <a class="navbar-brand order-2 order-lg-1" href="index-2.jsp"><img class="img-fluid" src="images/logo.png" alt="logo"></a> -->
<!--   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" -->
<!--     aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
<!--     <span class="navbar-toggler-icon"></span> -->
<!--   </button> -->

<!--   <div class="collapse navbar-collapse order-1 order-lg-2" id="navbarSupportedContent"> -->
<!--     <ul class="navbar-nav mx-auto"> -->
<!--       <li class="nav-item"> -->
<!--         <a class="nav-link" href="index-2.jsp">home</a> -->
<!--       </li> -->
<!--       <li class="nav-item dropdown view"> -->
<!--         <a class="nav-link dropdown-toggle" href="shop.jsp" role="button" data-toggle="dropdown" aria-haspopup="true" -->
<!--           aria-expanded="false"> -->
<!--           shop -->
<!--         </a> -->
<!--         <div class="dropdown-menu"> -->
<!--           <a class="dropdown-item" href="shop.jsp">Shop</a> -->
<!--           <a class="dropdown-item" href="shop-list.jsp">Shop List</a> -->
<!--           <a class="dropdown-item" href="product-single.jsp">Product Single</a> -->
<!--           <a class="dropdown-item" href="cart.jsp">Cart</a> -->
<!--           <a class="dropdown-item" href="shipping.jsp">Shipping Method</a> -->
<!--           <a class="dropdown-item" href="payment.jsp">Payment Method</a> -->
<!--           <a class="dropdown-item" href="review.jsp">Review</a> -->
<!--           <a class="dropdown-item" href="confirmation.jsp">Confirmation</a> -->
<!--           <a class="dropdown-item" href="track.jsp">Track Order</a> -->
<!--         </div> -->
<!--       </li> -->
<!--       <li class="nav-item dropdown view"> -->
<!--         <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" -->
<!--           aria-expanded="false"> -->
<!--           pages -->
<!--         </a> -->
<!--         <div class="dropdown-menu"> -->
<!--           <a class="dropdown-item" href="about.jsp">About Us</a> -->
<!--           <a class="dropdown-item" href="contact.jsp">Contact Us</a> -->
<!--           <a class="dropdown-item" href="login.jsp">Login</a> -->
<!--           <a class="dropdown-item" href="signin.jsp">Signup</a> -->
<!--           <a class="dropdown-item" href="forget-password.jsp">Forget Password</a> -->
<!--           <a class="dropdown-item" href="dashboard.jsp">Dashboard</a> -->
<!--           <a class="dropdown-item" href="faq.jsp">FAQ</a> -->
<!--           <a class="dropdown-item" href="404.jsp">404 Page</a> -->
<!--           <a class="dropdown-item" href="coming-soon.jsp">Coming Soon</a> -->
<!--           <a class="dropdown-item" href="blog-grid.jsp">Blog</a> -->
<!--           <a class="dropdown-item" href="blog-single.jsp">Blog Single</a> -->
<!--         </div> -->
<!--       </li> -->
<!--       <li class="nav-item dropdown mega-dropdown"> -->
<!--         <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" -->
<!--           aria-expanded="false"> -->
<!--           Mega Menu -->
<!--         </a> -->
<!--         <div class="dropdown-menu mega-menu"> -->
<!--           <div class="mx-3 mega-menu-item"> -->
<!--             <h6>Men</h6> -->
<!--             <ul class="pl-0"> -->
<!--               <li><a href="shop.jsp">Jackets & Coats</a></li> -->
<!--               <li><a href="shop.jsp">Jeans</a></li> -->
<!--               <li><a href="shop.jsp">Top & T-Shirts</a></li> -->
<!--               <li><a href="shop.jsp">Dresses</a></li> -->
<!--               <li><a href="shop.jsp">Men Shirts</a></li> -->
<!--             </ul> -->
<!--           </div> -->
<!--           <div class="mx-3 mega-menu-item"> -->
<!--             <h6>Women</h6> -->
<!--             <ul class="pl-0"> -->
<!--               <li><a href="shop.jsp">Blouses & Shirts</a></li> -->
<!--               <li><a href="shop.jsp">Dresses</a></li> -->
<!--               <li><a href="shop.jsp">Top & T-Shirts</a></li> -->
<!--               <li><a href="shop.jsp">Jeans & Trousers</a></li> -->
<!--               <li><a href="shop.jsp">Jackets & Coats</a></li> -->
<!--             </ul> -->
<!--           </div> -->
<!--           <div class="mx-3 mega-menu-item"> -->
<!--             <h6>Shoes & Bags</h6> -->
<!--             <ul class="pl-0"> -->
<!--               <li><a href="shop.jsp">Backpacks</a></li> -->
<!--               <li><a href="shop.jsp">Bum Bags</a></li> -->
<!--               <li><a href="shop.jsp">Accross Bags</a></li> -->
<!--               <li><a href="shop.jsp">Shoes</a></li> -->
<!--               <li><a href="shop.jsp">Heeled Shoes</a></li> -->
<!--             </ul> -->
<!--           </div> -->
<!--           <div class="mx-3 mega-menu-item"> -->
<!--             <h6>Accessories</h6> -->
<!--             <ul class="pl-0"> -->
<!--               <li><a href="shop.jsp">Sunglasses</a></li> -->
<!--               <li><a href="shop.jsp">Watches</a></li> -->
<!--               <li><a href="shop.jsp">Gloves</a></li> -->
<!--               <li><a href="shop.jsp">Capes & Hats</a></li> -->
<!--               <li><a href="shop.jsp">Belts</a></li> -->
<!--             </ul> -->
<!--           </div> -->
<!--           <div class="mx-3 mega-megu-image"> -->
<!--             <img class="img-fluid h-100" src="images/mega-megu.jpg" alt="feature-img"> -->
<!--           </div> -->
<!--         </div> -->
<!--       </li> -->
<!--       <li class="nav-item"> -->
<!--         <a class="nav-link" href="contact.jsp">Contact Us</a> -->
<!--       </li> -->
<!--     </ul> -->
<!--   </div> -->
<!--   <div class="order-3 navbar-right-elements"> -->
<!--     <div class="search-cart"> -->
<!--       search -->
<!--       <div class="search"> -->
<!--         <button id="searchOpen" class="search-btn"><i class="ti-search"></i></button> -->
<!--         <div class="search-wrapper"> -->
<!--           <form action="#"> -->
<!--             <input class="search-box" id="search" type="search" placeholder="Enter Keywords..."> -->
<!--             <button class="search-icon" type="submit"><i class="ti-search"></i></button> -->
<!--           </form> -->
<!--         </div> -->
<!--       </div> -->
<!--       cart -->
<!--       <div class="cart"> -->
<!--         <button id="cartOpen" class="cart-btn"><i class="ti-bag"></i><span class="d-xs-none">CART</span> 3</button> -->
<!--         <div class="cart-wrapper"> -->
<!--           <i id="cartClose" class="ti-close cart-close"></i> -->
<!--           <h4 class="mb-4">Your Cart</h4> -->
<!--           <ul class="pl-0 mb-3"> -->
<!--             <li class="d-flex border-bottom"> -->
<!--               <img src="images/cart/product-1.jpg" alt="product-img"> -->
<!--               <div class="mx-3"> -->
<!--                 <h6>Eleven Paris Skinny Jeans</h6> -->
<!--                 <span>1</span> X <span>$79.00</span> -->
<!--               </div> -->
<!--               <i class="ti-close"></i> -->
<!--             </li> -->
<!--             <li class="d-flex border-bottom"> -->
<!--               <img src="images/cart/product-2.jpg" alt="product-img"> -->
<!--               <div class="mx-3"> -->
<!--                 <h6>Eleven Paris Skinny Jeans top</h6> -->
<!--                 <span>1 X</span> <span>$79.00</span> -->
<!--               </div> -->
<!--               <i class="ti-close"></i> -->
<!--             </li> -->
<!--           </ul> -->
<!--           <div class="mb-3"> -->
<!--             <span>Cart Total</span> -->
<!--             <span class="float-right">$79.00</span> -->
<!--           </div> -->
<!--           <div class="text-center"> -->
<!--             <a href="cart.jsp" class="btn btn-dark btn-mobile rounded-0">view cart</a> -->
<!--             <a href="shipping.jsp" class="btn btn-dark btn-mobile rounded-0">check out</a> -->
<!--           </div> -->
<!--         </div> -->
<!--       </div> -->
<!--     </div> -->
<!--   </div> -->
<!-- </nav> -->
<!-- /navigation -->

<!-- main wrapper -->
<div class="main-wrapper">

<!-- breadcrumb -->
<nav class="bg-gray py-3">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index-2.jsp">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Frequently Asked Questions</li>
    </ol>
  </div>
</nav>
<!-- /breadcrumb -->

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>Frequently Asked Questions</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, repudiandae.</p>
				<p>admin@mail.com</p>
			</div>
			<div class="col-md-8">
				<!-- accordion -->
				<div class="accordion">
					<div class="card">
						<div class="card-header cursor-pointer" data-toggle="collapse" data-target="#collapseOne">
							<h4 class="mb-0">Order Status</h4>
						</div>

						<div id="collapseOne" class="collapse show">
							<div class="card-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas iusto, alias, tempora fuga quam eveniet neque excepturi aliquid. Eligendi, mollitia.
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header cursor-pointer" data-toggle="collapse" data-target="#collapseTwo">
							<h4 class="mb-0">Shipping & Delivery</h4>
						</div>

						<div id="collapseTwo" class="collapse">
							<div class="card-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam voluptatibus, incidunt similique nobis sint quisquam nam ab error consequuntur sit ullam ex eum exercitationem, excepturi explicabo beatae eos aspernatur odit ad perspiciatis, neque saepe magni enim. Maiores quia, quae sequi.
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header cursor-pointer" data-toggle="collapse" data-target="#collapseThree">
							<h4 class="mb-0">Payments</h4>
						</div>

						<div id="collapseThree" class="collapse">
							<div class="card-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus repellat id, laboriosam ipsa repudiandae quisquam, suscipit officiis, praesentium itaque facilis distinctio dolorum. Velit reiciendis libero laudantium corporis, delectus impedit sunt.
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header cursor-pointer" data-toggle="collapse" data-target="#collapseFour">
							<h4 class="mb-0">Returns & Exchanges</h4>
						</div>

						<div id="collapseFour" class="collapse">
							<div class="card-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam eaque nam, ab voluptas et debitis sint hic vel ratione dignissimos.
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header cursor-pointer" data-toggle="collapse" data-target="#collapseFive">
							<h4 class="mb-0">Privacy Policy</h4>
						</div>

						<div id="collapseFive" class="collapse">
							<div class="card-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis quod saepe, inventore ipsum sint cum iste quae ratione nobis laborum minima autem totam similique, quia neque deleniti! Provident, suscipit.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- footer -->
<%@include file="footer.jsp"%>
<!-- /footer -->

</div>
<!-- /main wrapper -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/faq.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:55 GMT -->
</html>