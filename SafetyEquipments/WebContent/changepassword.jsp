<%@page import="com.safetyequipment.beans.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
<head>
<meta charset="utf-8">
<title>Dipen Safety</title>

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- <style> -->
<link rel="stylesheet" href="css/passwordvalidation.css">
<!-- * Plugins Needed for the Project * -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">
<link href="css/all.css" rel="stylesheet">
<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>
	<%
		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userlogin");
	%>
	<!-- 4	<!-- preloader start -->
	<!-- 	<div class="preloader"> -->
	<!-- 		<img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- 			width="160px"> -->
	<!-- 	</div> -->
	<!-- 	<!-- preloader end -->
	<!-- preloader start -->
	<!-- 	<div class="preloader"> -->
	<!-- 		<img src="images/preloader.gif" alt="preloader" height="160px" -->
	<!-- 			width="160px"> -->
	<!-- 	</div> -->
	<!-- preloader end -->

<section class="signin-page account">
		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index-2.jsp"> <img src="images/DLOGO.png"
							alt="logo">
						</a>
						<h2 class="text-center">Change Password</h2>
						<form class="text-left clearfix" action="ChangePasswordServlet"
							method="post" enctype="multipart/form-data"
							onsubmit="return formvalidation()">



							<div class="form-group">
<!-- 								<label>Current Password :</label> -->
								 <input type="password"
									class="form-control" placeholder="Current Password"
									name="currentpassword" id="currentpassword"
									onblur="return currentpasswordvalidation()"> <span
									id="current_password" style="color: red;"></span>
							</div>

							<div class="form-group">
<!-- 								<label>New Password :</label>  -->
								<input type="password"
									class="form-control" placeholder="New Password"
									name="newpassword" id="newpassword"
									 pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
									onblur="return newpasswordvalidation()"> <span
									id="new_password" style="color: red;"></span>
							</div>
							

							<div id="message">
								<h3>Password must contain the following:</h3>
								<p id="letter" class="invalid">
									A <b>lowercase</b> letter
								</p>
								<p id="capital" class="invalid">
									A <b>capital (uppercase)</b> letter
								</p>
								<p id="number" class="invalid">
									A <b>number</b>
								</p>
								<p id="length" class="invalid">
									Minimum <b>8 characters</b>
								</p>
							</div>

							<div class="form-group">
<!-- 								<label>Confirm New Password :</label>  -->
								<input type="password"
									class="form-control" placeholder="Confirm New Password"
									name="confirmnewpassword" id="confirmnewpassword"
									onblur="return confirmnewpasswordvalidation()"> <span
									id="confirm_new_password" style="color: red;"></span>
							</div>


							<div class="form-group">
								<input type="hidden" class="form-control"
									value="<%=user.getEmailid()%>" id="useremail" name="useremail">
							</div>

							<div class="form-group">
								<input type="hidden" class="form-control"
									value="<%=user.getUserid()%>" id="userid" name="userid">
							</div>

							<div class="text-center">
								<button type="submit" name="cpsubmit" id="cpsubmit"
									class="btn btn-primary">Change Password</button>
							</div>
						</form>
						<p class="mt-3"><a href="index.jsp"> Back to Home</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	</div>
	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
	
	var myInput = document.getElementById("upassword");
	var letter = document.getElementById("letter");
	alert(document.getElementById("letter"));
	var capital = document.getElementById("capital");
	var number = document.getElementById("number");
	var length = document.getElementById("length");	
	
		function currentpasswordvalidation() {
			var curpwd = document.getElementById('currentpassword').value;
			//var pattern = /^[A-Za-z]{1,10}$/;

			if (curpwd == "") {
				document.getElementById('current_password').innerHTML = "*Please enter current password";
				return false;
			}
			if ((curpwd.length > 8) || (curpwd.length < 8)) {
				document.getElementById('current_password').innerHTML = "* the length of  password must be 8 ";
				return false;
			} else {
				document.getElementById('current_password').innerHTML = "";
				return true;
			}

		}

		function newpasswordvalidation() {
			var newpwd = document.getElementById('newpassword').value;
			//var pattern = /^[A-Za-z]{1,10}$/;
			if (newpwd == "") {
				document.getElementById('new_password').innerHTML = "*Please enter confirm new  password";
				return false;
			}
			if ((newpwd.length > 8) || (newpwd.length < 8)) {
				document.getElementById('new_password').innerHTML = "* the length of  confirm new  password must be 8 ";
				return false;
			} else {
				document.getElementById('new_password').innerHTML = "";
				return true;
			}

		}

		function confirmnewpasswordvalidation() {
			var cnfnewpwd = document.getElementById('confirmnewpassword').value;
			var newpwd = document.getElementById('newpassword').value;
			//var pattern = /^[A-Za-z]{1,10}$/;
			if (cnfnewpwd == "") {
				document.getElementById('confirm_new_password').innerHTML = "*Please enter new password";
				return false;
			}
			if ((cnfnewpwd.length > 8) || (cnfnewpwd.length < 8)) {
				document.getElementById('confirm_new_password').innerHTML = "* the length of  new password must be 8 ";
				return false;
			}
			if (cnfnewpwd != newpwd) {
				document.getElementById('confirm_new_password').innerHTML = "* the new password and confirm new password must be same";
				return false;
			} else {
				document.getElementById('confirm_new_password').innerHTML = "";
				return true;
			}

		}

		function formvalidation() {
			if ((document.getElementById('current_password').innerHTML == "")
					&& (document.getElementById('new_password').innerHTML == "")
					&& (document.getElementById('confirm_new_password').innerHTML == "")) {
				return true;
			} else {
				return false;
			}
		}

		$(document).ready(function() {
			$('#currentpassword').blur(function(event) {
				var upass = $('#currentpassword').val();
				var uname = $('input:hidden[name=useremail]').val();
				//alert("user pass : "+upass);
				//alert("user name : "+uname);
				$.get('LoginUserServlet', {
					useremail : uname,
					password : upass
				}, function(responseText) {
					$('#LoginUserServlet').text(responseText);
					if (responseText.length > 2) {
						$('#cpsubmit').attr("disabled", true);
						$("#current_password").html(responseText);
					} else {
						//$('#LoginUserServlet').html(responseText);
						$('#cpsubmit').attr("disabled", false);
						$("#current_password").html(responseText);
					}
				});
			});
		});

		

		// When the user clicks on the password field, show the message box
		myInput.onfocus = function() {
			document.getElementById("message").style.display = "block";
		}

		// When the user clicks outside of the password field, hide the message box
		myInput.onblur = function() {
			document.getElementById("message").style.display = "none";
		}

		// When the user starts to type something inside the password field
		myInput.onkeyup = function() {
			// Validate lowercase letters
			var lowerCaseLetters = /[a-z]/g;
			if (myInput.value.match(lowerCaseLetters)) {
				letter.classList.remove("invalid");
				letter.classList.add("valid");
			} else {
				letter.classList.remove("valid");
				letter.classList.add("invalid");
			}

			// Validate capital letters
			var upperCaseLetters = /[A-Z]/g;
			if (myInput.value.match(upperCaseLetters)) {
				capital.classList.remove("invalid");
				capital.classList.add("valid");
			} else {
				capital.classList.remove("valid");
				capital.classList.add("invalid");
			}

			// Validate numbers
			var numbers = /[0-9]/g;
			if (myInput.value.match(numbers)) {
				number.classList.remove("invalid");
				number.classList.add("valid");
			} else {
				number.classList.remove("valid");
				number.classList.add("invalid");
			}

			// Validate length
			if (myInput.value.length >= 8) {
				length.classList.remove("invalid");
				length.classList.add("valid");
			} else {
				length.classList.remove("valid");
				length.classList.add("invalid");
			}
		}
	</script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/signin.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
</html>