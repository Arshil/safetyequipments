<%@page import="com.safetyequipment.beans.MyOrder"%>
<%@page import="com.safetyequipment.beans.Product"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Order List</title>
  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<link rel="icon" href="images/logo.png" type="image/x-icon">
</script>
</head>
<body>
<%
	List<MyOrder> myOrderList = (ArrayList<MyOrder>) (request.getAttribute("myorderlist"));
%>
<%int i = myOrderList.size();%>
<%@include file="header.jsp" %>
<!-- <a href="AddProductServlet">Add Product</a> -->
<!-- <button style="margin-left:45%;" ><a href="AddProductServlet" class="fa fa-plus-square"> Add new product</a></button> -->
<div>
<%if(i!=0){ %>
<table  id="example" class="display" style="width:100%" border="1">
        <thead>
            <tr>
											<th>Order ID</th>
											<th>Order Date</th>
											<th>Shipping Date</th>
											<th>Items</th>
											<th>Total Price</th>
											<th>Payment Status</th>
											<th>Shipping Status</th>
											<th>View Order</th>
											<th>Cancel Order</th>
										</tr>
        </thead>
        <tbody>
										<%
											for (MyOrder myOrder : myOrderList) {
										%>
										<tr>
											<td><%=myOrder.getShipping().getOrderkey()%></td>
											<td><%=myOrder.getShipping().getOrderdate()%></td>
											<td><%=myOrder.getShipping().getShippingdate() %> </td>
											<td><%=myOrder.getShipping().getItems()%></td>
											<td><%=myOrder.getShipping().getAmount()%></td>
											<!-- 									<td><span class="badge badge-primary">Processing</span></td> -->
											<%if(myOrder.getShipping().getPaymentstutus().equalsIgnoreCase("pending")) { %>
        									 <td><span class="badge badge-warning">Pending</span></td>
        									<%} else { %>
       										<td><span class="badge badge-success">Completed</span></td>
       										 <%} %>

											<%if(myOrder.getShipping().getShippingstatus().equalsIgnoreCase("pending")) { %>
        								    <td><span class="badge badge-warning">Pending</span></td>
        									<%} else if(myOrder.getShipping().getShippingstatus().equalsIgnoreCase("completed")){ %>
        									<td><span class="badge badge-success">Completed</span></td>
        									<%} else { %>
       									    <td><span class="badge badge-danger">Canceled</span></td>
        									<%} %>
											<td><a
												href="ViewOrderServlet?orderkey=<%=myOrder.getShipping().getOrderkey()%>"
												class="btn btn-sm btn-outline-primary">View</a></td>
											<%
												if (myOrder.getShipping().getShippingstatus().equalsIgnoreCase("cancelled")) {
											%>
											<td>Already Cancelled</td>
											<%
												} else {
											%>
											<td><a
												href="CancelOrderServlet?orderkey=<%=myOrder.getShipping().getOrderkey()%>"
												class="btn btn-sm btn-outline-primary"
												onclick="return validation()">Cancel Order</a></td>
											<%
												}
											%>
										</tr>
										<%
											}
										%>
									</tbody>
<!--                 </tfoot> -->
    </table>
    <%} else { %>
    <li style="align-content: center;margin-left: 30%; color: red;font-family: cursive;font-size:large;">NO ORDERS IN YOUR LIST...START SHOPPING</li>
    <%} %>
    </div>
<!--     div ended -->
    
    <%@include file="footer.jsp" %>
    <!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>
	<script type="text/javascript">
		function validation() {
			if (confirm("Do you want to cancel your order?") == true) {
				//alert("ok called");
				return true;
			} else {
				//alert("ok else condition called");
				return false;
			}
		}
	</script>
</body>
</html>