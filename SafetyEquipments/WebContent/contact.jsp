<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/contact.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
<head>
  <meta charset="utf-8">
  <title>Dipen Safety</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
   <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  <link rel="icon" href="images/logo.png" type="image/x-icon">

</head>

<body>

  <!-- preloader start -->
<!--   <div class="preloader"> -->
<!--     <img src="images/preloader.gif" alt="preloader"> -->
<!--   </div> -->
  <!-- preloader end -->

<!-- header -->
<header>
  <!-- top advertise -->
  <!-- <div class="alert alert-secondary alert-dismissible fade show rounded-0 pb-0 mb-0" role="alert">
    <div class="d-flex justify-content-between">
      <p>SAVE UP TO $50</p>
      <h4>SALE!</h4>
      <div class="shop-now">
        <a href="shop.jsp" class="btn btn-sm btn-primary">Shop Now</a>
      </div>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div> -->
<%@include file="header.jsp" %>
  <!-- top header -->
 <!-- /navigation -->

<!-- main wrapper -->
<div class="main-wrapper">

<!-- breadcrumb -->
<nav class="bg-gray py-3">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index-2.jsp">Home</a></li>
      <li class="breadcrumb-item" aria-current="page">Contact Us</li>
    </ol>
  </div>
</nav>
<!-- /breadcrumb -->

<!-- google map -->
<section class="map">
  <!-- Google Map -->
  <div id="map_canvas" data-latitude="23.039640" data-longitude="72.565338"></div>
</section>
<!-- /google map -->

<!-- contact -->
<section class="section">
  <div class="container">
    <div class="row justify-content-between">
      <!-- form -->
      <div class="col-lg-7 mb-5 mb-lg-0 text-center text-md-left">
        <h2 class="section-title">Contact Us</h2>
        <form action="MakeInquiryServlet" class="row" method="post" onsubmit="return formvalidation()">
          <div class="col-md-6">
            <input type="text" id="firstName" name="firstName" class="form-control mb-4 rounded-0" placeholder="First Name" onblur="return firstnamevalidation()">
          	<span id="first_Name"></span>
          </div>
          <div class="col-md-6">
            <input type="text" id="lastName" name="lastName" class="form-control mb-4 rounded-0" placeholder="Last Name" onblur="return lastnamevalidation()">
          	<span id="last_Name"></span>
          </div>
          <div class="col-md-12">
            <input type="text" id="subject" name="subject" class="form-control mb-4 rounded-0" placeholder="Subject" onblur="return subjectvalidation()">
          	<span id="sub_ject"></span>
          </div>
          <div class="col-md-12">
            <input type="email" id="emailid" name="emailid" class="form-control mb-4 rounded-0" placeholder="Email-ID" onblur="return emailvalidation()">
          	<span id="email_id"></span>
          </div>
          
          <div class="col-md-12">
            <textarea name="message" id="message" class="form-control rounded-0 mb-4" placeholder="Message" onblur="return messagevalidation()"></textarea>
            <span id="subject_message"></span>
          </div>
          <div class="col-md-12">
            <button type="submit" id="cusubmit" value="send" class="btn btn-primary">Submit now</button>
          </div>
        </form>
      </div>
      <!-- contact item -->
      <div class="col-lg-4">
        <div class="d-flex mb-60">
          <i class="ti-location-pin contact-icon"></i>
          <div>
            <h4>Our Location</h4>
            <p class="text-gray">508-509 Aakruti Complex,Nr.Sahyadri Flats, Stadium Cross Road, Navrangpura, Ahmedabad-380009, Gujarat , India
          </p>
          </div>
                 
        </div>
        <div class="d-flex mb-60">
          <i class="ti-mobile contact-icon"></i>
          <div>
            <h4>Call Us Now</h4>
            <p class="text-gray mb-0">079-40055545</p>
<!--             <p class="text-gray mb-0">(+18) 0117 022 7729</p> -->
          </div>
        </div>
        <div class="d-flex mb-60">
          <i class="ti-email contact-icon"></i>
          <div>
            <h4>Write Us Now</h4>
            <p class="text-gray mb-0">dipen_security@rediffmail.com</p>
<!--             <p class="text-gray mb-0">info@example.com</p> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /contact -->

<!-- footer -->
<%@include file="footer.jsp" %>
<!-- /footer -->

</div>
<!-- /main wrapper -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
<script type="text/javascript">

function firstnamevalidation() {
	var firstname = document.getElementById('firstName').value;
	var pattern = /^[A-Za-z]{1,20}$/;

	if (firstname == "") {
		document.getElementById('first_Name').innerHTML = "*Please enter first name";
		return false;
	}
	if ((firstname.length < 1) || (firstname.length > 20)) {
		document.getElementById('first_Name').innerHTML = "*Please enter first name between 1 to 20 characters";
		return false;
	}
	if (pattern.test(firstname)) {
		document.getElementById('first_Name').innerHTML = "";
		$('#cusubmit').attr("disabled", false);
		return true;
	} else {
		document.getElementById('first_Name').innerHTML = "*Please enter valid first name  ";
		return false;
	}

}

function lastnamevalidation() {
	var lastname = document.getElementById('lastName').value;
	var pattern = /^[A-Za-z]{1,20}$/;

	if (lastname == "") {
		document.getElementById('last_Name').innerHTML = "*Please enter last name";
		return false;
	}
	if ((lastname.length < 1) || (lastname.length > 20)) {
		document.getElementById('last_Name').innerHTML = "*Please enter last  name between 1 to 20  characters ";
		return false;
	}
	if (pattern.test(lastname)) {
		document.getElementById('last_Name').innerHTML = "";
		$('#cusubmit').attr("disabled", false);
		return true;
	} else {
		document.getElementById('last_Name').innerHTML = "*Please enter valid  last name  ";
		return false;
	}

}//subject
function subjectvalidation() {
	var lastname = document.getElementById('subject').value;
	var pattern = /^[A-Za-z]{1,50}$/;

	if (lastname == "") {
		document.getElementById('sub_ject').innerHTML = "*Please enter subject name";
		return false;
	}
	if ((lastname.length < 1) || (lastname.length > 50)) {
		document.getElementById('sub_ject').innerHTML = "*Please enter  subject   between 1 to 20 characters";
		return false;
	}
	if (pattern.test(lastname)) {
		document.getElementById('sub_ject').innerHTML = "";
		$('#cusubmit').attr("disabled", false);
		return true;
	} else {
		document.getElementById('sub_ject').innerHTML = "*Please enter valid  subject  ";
		return false;
	}

}
function emailvalidation() {

	var emailid = document.getElementById('emailid').value;
	var pattern = /^[A-Za-z0-9_.-]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/;
	if (emailid == "") {
		document.getElementById('email_id').innerHTML = "*Please enter emailid  ";
		return false;
	}
	if (pattern.test(emailid)) {
		document.getElementById('email_id').innerHTML = "";
		$('#cusubmit').attr("disabled", false);
		return true;
	} else {
		document.getElementById('email_id').innerHTML = "*Please enter valid emailid ";
		return false;
	}
}
function messagevalidation() {
	var message = document.getElementById('message').value;
//	var pattern = /^[A-Za-z]{1,20}$/;

	if (message == "") {
		document.getElementById('subject_message').innerHTML = "*Please enter message regarding to subject";
		return false;
	}
	if ((message.length < 1) || (message.length > 200)) {
		document.getElementById('subject_message').innerHTML = "*Please enter message between 1 to 200 characters ";
		return false;
	}
	 else {
		document.getElementById('subject_message').innerHTML = "";
		$('#cusubmit').attr("disabled", false);
		return true;
	}

}

function formvalidation() {
	if (document.getElementById('firstName').value == "") {
		document.getElementById('first_Name').innerHTML = "*Please enter first name";
		$('#cusubmit').attr("disabled", true);
		return false;
	}
	if (document.getElementById('lastName').value == "") {
		document.getElementById('last_Name').innerHTML = "*Please enter last name";
		$('#cusubmit').attr("disabled", true);
		return false;
	}
	if (document.getElementById('subject').value == "") {
		document.getElementById('sub_ject').innerHTML = "*Please enter subject name";
		$('#cusubmit').attr("disabled", true);
		return false;
	}
	if (document.getElementById('emailid').value == "") {
		document.getElementById('email_id').innerHTML = "*Please enter email id";
		$('#cusubmit').attr("disabled", true);
		return false;
	}
	
	if (document.getElementById('message').value == "") {
		document.getElementById('subject_message').innerHTML = "*Please enter message regarding to subject";
		$('#cusubmit').attr("disabled", true);
		return false;
	}
	if ((document.getElementById('first_Name').innerHTML == "")
			&& (document.getElementById('last_Name').innerHTML == "")
			&& (document.getElementById('sub_ject').innerHTML == "")
			&& (document.getElementById('email_id').innerHTML == "")
			&& (document.getElementById('subject_message').innerHTML == "")) {
		//alert("registration successful");
		//$('#cusubmit').attr("disabled", false);
		if (confirm("Do you want to submit your inquiry?") == true) {
  			return true;
  		} else {
  			return false;
  		}
	} else {
		//alert("Please fill details ");
		$('#cusubmit').attr("disabled", true);
		return false;
	}
}

</script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/contact.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
</html>