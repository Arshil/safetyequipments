<%@page import="com.safetyequipment.beans.Cart"%>
<%@page import="com.safetyequipment.beans.User"%>
<%@page import="com.safetyequipment.beans.SubCategory"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.safetyequipment.beans.Category"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.showme {
	display: none;
}

.showhim:hover .showme {
	display: block;
}
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="css/style.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- ** Plugins Needed for the Project ** -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">


</head>
<body>
	<%-- <%@include file="/FetchMenuServlet" %> --%>
	<jsp:include page="/FetchMenuServlet" />
	<%
		List<Category> menuList = (ArrayList) (request.getAttribute("categorylist"));
	%>
	<%
		HttpSession httpSessionuser = request.getSession(false);
		User userobj = (User) httpSessionuser.getAttribute("userlogin");
	%>
	<!-- Main Stylesheet -->

	<!-- header -->
<!-- 	<header> -->
		<!-- top advertise -->
		<!-- <div class="alert alert-secondary alert-dismissible fade show rounded-0 pb-0 mb-0" role="alert">
    <div class="d-flex justify-content-between">
      <p>SAVE UP TO $50</p>
      <h4>SALE!</h4>
      <div class="shop-now">
        <a href="shop.jsp" class="btn btn-sm btn-primary">Shop Now</a>
      </div>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div> -->

		<!-- top header -->
		<header>
			<!-- top advertise -->
			<!-- <div class="alert alert-secondary alert-dismissible fade show rounded-0 pb-0 mb-0" role="alert">
    <div class="d-flex justify-content-between">
      <p>SAVE UP TO $50</p>
      <h4>SALE!</h4>
      <div class="shop-now">
        <a href="shop.jsp" class="btn btn-sm btn-primary">Shop Now</a>
      </div>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div> -->

			<!-- top header -->
			<div class="top-header" style="height: 40px;"> 
				<div class="row">
					<div class="col-lg-6 text-center text-lg-left">
						<p class="text-white mb-lg-0 mb-1">Free shipping - Free 30
							days return - Express delivery</p>
					</div>
					<div class="col-lg-6 text-center text-lg-right">
						<ul class="list-inline">
							<%
								if (null != userobj) {
							%>
							<!--           <li class="list-inline-item"><img src="images/flag.jpg" alt="flag"></li> -->
							<li class="list-inline-item"><a href="profile-details.jsp">My Accounts</a></li>
							<li class="list-inline-item"><a href="UserCartListServlet">My Cart</a></li>
							<li class="list-inline-item"><a href="MyOrderListServlet?userid=<%=userobj.getUserid()%>">My Order</a></li>
							<li class="list-inline-item"><a href="makecomplaint.jsp">Make Complaint</a></li>
							<li class="list-inline-item"><a href="UserComplaintListServlet">My Complaints</a></li>
							<li class="list-inline-item"><a href="changepassword.jsp">Change Password</a></li>
							<li class="list-inline-item"><a href="UserLogOutServlet">LogOut</a></li>
							<li class="list-inline-item">
							<%
							} else {
							%>		
							<li class="list-inline-item"><a href="login.jsp">Login</a></li>
							<%
								}
							%>
							<!--             <form action="#"> -->
							<!--               <select class="country" name="country"> -->
							<!--                 <option value="USD">USD</option> -->
							<!--                 <option value="JPN">JPN</option> -->
							<!--                 <option value="BAN">BAN</option> -->
							<!--               </select> -->
							<!--             </form> -->
							<!--           <li class="list-inline-item">
            <a class="active" href="#">EN</a>
            <a href="#">FR</a>
          </li> -->
						</ul>
					</div>
				</div>
			</div>
			<!-- /top-header -->
		</header>
		<!-- navigation -->
		<nav class="navbar navbar-expand-lg navbar-light bg-white w-100" id="navbar">
			<a class="navbar-brand order-2 order-lg-1" href="index.jsp"><img class="img-fluid" src="images//DLOGO.png" alt="logo"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"	data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

			<div class="collapse navbar-collapse order-1 order-lg-2" id="navbarSupportedContent">
				<ul class="navbar-nav mx-auto">
					<li class="nav-item"><a class="nav-link" href="index.jsp">home</a>
					</li>
					<li class="nav-item dropdown view"><a
						class="nav-link dropdown-toggle" href="shop.jsp" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							shop </a>
						<div class="dropdown-menu">
							<%
								if (null != userobj) {
							%>
							<%
								if (userobj.getUsertype().equals("A")) {
							%>
							<a class="dropdown-item" href="GetCategoryListServlet">Manage
								Category</a> <a class="dropdown-item" href="SubCategoryListServlet">Manage
								SubCategory</a> <a class="dropdown-item" href="ProductListServlet">Manage
								Product</a> <a class="dropdown-item" href="GetCompanyList">Manage
								Company</a>
								<a class="dropdown-item" href="UserListServlet">Manage
								User</a>
								<a class="dropdown-item" href="AllOrderListServlet">Manage
								Order</a>
								<a class="dropdown-item" href="ComplaintListServlet">Manage Complaint</a>
								
								
								<%
								}
								}
							%>
							<a class="dropdown-item" href="shop.jsp">Shop</a> 
							<a class="dropdown-item" href="shop-list.jsp">Shop List</a> 
							<a	class="dropdown-item" href="product-single.jsp">Product Single</a> 
							<a class="dropdown-item" href="cart.jsp">Cart</a> 
							<a class="dropdown-item" href="shipping.jsp">Shipping Method</a> 
							<a class="dropdown-item" href="payment.jsp">Payment Method</a> 
							<a class="dropdown-item" href="review.jsp">Review</a> 
							<a class="dropdown-item" href="confirmation.jsp">Confirmation</a> 
							<a class="dropdown-item" href="track.jsp">Track Order</a> 
							<a class="dropdown-item" href="feedback.jsp">feedback</a>

						</div></li>
					<li class="nav-item dropdown view"><a
						class="nav-link dropdown-toggle" href="#" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							pages </a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="about.jsp">About Us</a> <a
								class="dropdown-item" href="contact.jsp">Contact Us</a> <a
								class="dropdown-item" href="login.jsp">Login</a> <a
								class="dropdown-item" href="signin.jsp">Signup</a> <a
								class="dropdown-item" href="forget-password.jsp">Forget
			Password</a> <a class="dropdown-item" href="dashboard.jsp">Dashboard</a>
								<a class="dropdown-item" href="faq.jsp">FAQ</a> <a
								class="dropdown-item" href="404.jsp">404 Page</a> <a
								class="dropdown-item" href="coming-soon.jsp">Coming Soon</a> <a
								class="dropdown-item" href="blog-grid.jsp">Blog</a> <a
								class="dropdown-item" href="blog-single.jsp">Blog Single</a> <a
								class="dropdown-item" href="changepassword.jsp">Change
								Password</a> <a class="dropdown-item" href="UserLogOutServlet">Log
								Out</a>
						</div></li>
					<li class="nav-item dropdown mega-dropdown"><a
						class="nav-link dropdown-toggle" href="#" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Mega Menu </a>

						<div class="dropdown-menu mega-menu">
							<%
								for (Category categorymenu : menuList) {
							%>
							<div class="mx-3 mega-menu-item">
								<h6><%=categorymenu.getCategoryname()%></h6>
								<ul class="pl-0">
									<%
										for (SubCategory subCategory : categorymenu.getSubcategorylist()) {
									%>
									<li><a
										href="FetchProductServlet?sub_category_id=<%=subCategory.getSubcategoryid()%>"><%=subCategory.getSubcategoryname()%></a></li>
									<%
										}
									%>
								</ul>
							</div>
							<%
								}
							%>

						</div></li>
					<li class="nav-item"><a class="nav-link" href="contact.jsp">Contact
							Us</a></li>
				</ul>
			</div>
			<div class="order-3 navbar-right-elements">
				<div class="search-cart">
					<!-- search -->
					<div class="search">
						<button id="searchOpen" class="search-btn">
							<i class="ti-search"></i>
						</button>
						<div class="search-wrapper">
							<form action="#">
								<input class="search-box" id="search" type="search"
									placeholder="Enter Keywords...">
								<button class="search-icon" type="submit">
									<i class="ti-search"></i>
								</button>
							</form>
						</div>
					</div>
</div>
</div>

					<!-- cart -->
		</nav>
		<!-- /navigation -->
</body>
</html>