<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/forget-password.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
<head>
<meta charset="utf-8">
<title>Dipen Safety</title>

<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- ** Plugins Needed for the Project ** -->
<!-- Bootstrap -->
<link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
<link rel="stylesheet" href="plugins/slick/slick.css">
<link rel="stylesheet" href="plugins/venobox/venobox.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/aos/aos.css">
<link rel="stylesheet"
	href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="plugins/nice-select/nice-select.css">
<link rel="stylesheet"
	href="plugins/bootstrap-slider/bootstrap-slider.min.css">

<!-- Main Stylesheet -->
<link href="css/style.css" rel="stylesheet">

<!--Favicon-->
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
  <link rel="icon" href="images/logo.png" type="image/x-icon">

</head>

<body>

	<!-- preloader start -->
	<!--   <div class="preloader"> -->
	<!--     <img src="images/preloader.gif" alt="preloader"> -->
	<!--   </div> -->
	<!-- preloader end -->

	<section class="forget-password-page account">
		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="block text-center">
						<a class="logo" href="index.jsp"> <img src="images/DLOGO.png" alt="logo">
						</a>
						<h2 class="text-center">Recover Password</h2>
						<form class="text-left clearfix" method="post"
							action="ForgotPasswordServlet" onsubmit="return validation()">
							<p>
								<!-- 								Please enter the registered email address for your account. -->
								<!--              A verification code will be sent to you.  -->
								<!--              Once you have received the verification code, you will  -->
								<!--              be able to choose a new password for your account. -->
							</p>
							<div class="form-group">
								<input type="email" class="form-control" id="emailid"
									name="emailid" placeholder="Account email address"
									required="required" onblur="return emailvalidation()">
								<span id="email_id" style="color: red;"></span>
							</div>
							<div class="text-center">
								<button type="submit" id="fpsubmit" class="btn btn-primary">Send
									Password</button>
							</div>
						</form>
						<p class="mt-3"><a href="login.jsp">Back to Login</a></p>
						<p class="mt-3"><a href="index.jsp"> Back to Home</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	</div>
	<!-- /main wrapper -->

	<!-- jQuery -->
	<script src="plugins/jQuery/jquery.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/venobox/venobox.min.js"></script>
	<script src="plugins/aos/aos.js"></script>
	<script src="plugins/syotimer/jquery.syotimer.js"></script>
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
	<script
		src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
	<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
	<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
	<!-- google map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
	<script src="plugins/google-map/gmap.js"></script>
	<!-- Main Script -->
	<script src="js/script.js"></script>

	<script type="text/javascript">
		function emailvalidation() {
			//			alert("email validation called");
			var emailid = document.getElementById('emailid').value;
			var pattern = /^[A-Za-z0-9_.-]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,6}$/;
			if (emailid == "") {
				document.getElementById('email_id').innerHTML = "*Please enter Email Id  ";
				return false;
			}
			if (pattern.test(emailid)) {
				document.getElementById('email_id').innerHTML = "";
				return true;
			} else {
				document.getElementById('email_id').innerHTML = "*Please enter valid Email Id ";
				return false;
			}
		}

		function validation() {
			if (document.getElementById('email_id').innerHTML == "") {
				return true;
			} else {
				return false;
			}
		}

		$(document).ready(function() {
			$('#emailid').blur(function(event) {
				var name = $('#emailid').val();
				//                 alert("email  : "+name);
				$.get('CheckUserRegistrationServlet', {
					useremail : name
				}, function(responseText) {
					$('#CheckUserRegistrationServlet').text(responseText);
					//alert("response text : "+responseText);
					if (responseText.length < 3) {
						$("#email_id").html("Invalid Email");
						$('#fpsubmit').attr("disabled", true);
					} else {
						$("#email_id").html("");
						$('#fpsubmit').attr("disabled", false);
					}
				});
			});
		});
	</script>



</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/forget-password.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:54 GMT -->
</html>