<%@page import="com.safetyequipment.beans.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%ArrayList<Category> categorylist = (ArrayList<Category>)request.getAttribute("categorylist");%>
<meta charset="ISO-8859-1">
<title>Category List</title>
 <link href="css/bootstrap-modal.min.css" rel="stylesheet">
 <link href="css/deletemodel.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</head>
<body>
<%@include file="header.jsp" %>
<button style="margin-left:45%;"><a href="addcategory.jsp" class="fa fa-plus-square"> Add New Category</a></button>
<div style="margin-left: 27%;">
<table style="margin-right:30" id="example" class="display" style="width:100%" border="1"></div>
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Image</th>
                <th>Category</th>
                <th>Category Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr> 
        </thead>
        <tbody>        
        <%int cnt =0; %> 
        <% for(Category category : categorylist){ %>
        <tr>   
     	<td><%= ++cnt %></td> 
     	<td><img src="data:image/jpeg;base64,<%=category.getCategoryImageString()%>" height="100" width="100"></td>
     	<td><%=category.getCategoryname() %></td>
     	<td><%=category.getDescription() %></td>
     	<td><a href="UpdateCategory?category_id=<%= category.getCategoryid() %>" class="fa fa-edit" style="font-size:36px;"></a></td>	
        <td><a href="DeleteCategory?category_id=<%= category.getCategoryid() %>"  class="fa fa-trash-o" style="font-size:36px"  onclick="return validation()"></a></td>
       	</tr>
        <%}%>        
  </table>
    </div>
 <%@include file="footer.jsp" %>
       <script type="text/javascript"> 
      function validation() {
    	  if (confirm("Are you sure you want to delete category?\nIf you delete category, the relevant subcategory and products will also be deleted.") == true) {
  			//alert("ok called");
  			return true;
  		} else {
  			//alert("ok else condition called");
  			return false;
  		}
	}
       </script>   
</body>
</html>