<%@page import="com.safetyequipment.beans.Cart"%>
<%@page import="com.safetyequipment.beans.User"%>
<%@page import="com.safetyequipment.beans.SubCategory"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.safetyequipment.beans.Category"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.showme {
  display: none;
}
.showhim:hover .showme {
  display: block;
}
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
 <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">
 <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">


</head>
<body>
<%HttpSession httpSessionuser = request.getSession(false); User suserobj = (User)httpSessionuser.getAttribute("userlogin"); %>
<jsp:include page="/ReviewOrderServlet" />
	<%
		List<Cart> ucartlist = (ArrayList) (request.getAttribute("cartlist"));
	%>
<!-- Main Stylesheet -->

<!-- header -->
<header>
</header>
      <!-- cart -->
      <div class="showhim" id="wrapper" style="border-color: black;border-style: dotted;">
        <button id="cartopen" class="cart-btn"><i class="ti-bag"></i><span class="d-xs-none"><a href="UserCartListServlet" onblur="show_cart()">CART</a></span></button>
 	     <div class="showme">
          <i id="cartClose" class="ti-close cart-close"></i>
          <h4 class="mb-4">Your Cart</h4>
          <ul class="pl-0 mb-3">
           <%for(Cart cart :  ucartlist) {%>
            <li class="d-flex border-bottom">
             <div class="product-info"
			style="height: 100px; width: 100px;">
			<img class="img-fluid"				
			src="data:image/jpeg;base64,<%=cart.getProduct().getImagestring()%>"
			alt="product-img" />
		</div>
              <div class="mx-3">
                <h6><%=cart.getProduct().getProductname()%></h6>
                <span><%=cart.getQuatity() %></span> X <span><%=cart.getProduct().getProductprice() %></span>
              </div>
              <i class="ti-close"></i>
            </li>
            <%} %>
        </ul>
        <div class="text-center"> 
            <a href="UserCartListServlet" class="btn btn-dark btn-mobile rounded-0">view cart</a>
            <a href="shipping.jsp" class="btn btn-dark btn-mobile rounded-0">check out</a>
          </div>        
        
        </div>
         	</div>
	
</body>
</html>