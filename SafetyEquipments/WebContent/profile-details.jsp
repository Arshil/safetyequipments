<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
  <meta charset="utf-8">
  <title>Dipen Safety</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<%
		HttpSession httpSessionpro = request.getSession(false);
		User userpro = (User) httpSessionpro.getAttribute("userlogin");
	%>
  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>
<!-- header -->
<%@include file="header.jsp" %>
<!-- main wrapper -->
<div class="main-wrapper">

<!-- breadcrumb -->
<nav class="bg-gray py-3">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index-2.jsp">Home</a></li>
      <li class="breadcrumb-item" aria-current="page">My Accounts</li>
    </ol>
  </div>
</nav>
<!-- /breadcrumb -->

<section class="user-dashboard section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="list-inline dashboard-menu text-center">
          <li class="list-inline-item"><a href="dashboard.jsp">Dashboard</a></li>
          <li class="list-inline-item"><a href="order.jsp">Orders</a></li>
          <li class="list-inline-item"><a href="address.jsp">Address</a></li>
          <li class="list-inline-item"><a class="active"  href="profile-details.jsp">Profile Details</a></li>
        </ul>
     
        <div class="dashboard-wrapper dashboard-user-profile">
          	<a href="userinfo.jsp" class="fa fa-edit" style="font-size:17px;align-content: center;">Change Details</a>
          <div class="media">
            <div class="text-center">
            <%if(null!=userpro.getImagestring()){ %>
              <img class="media-object user-img" src="data:image/jpeg;base64,<%=userpro.getImagestring()%>" alt="Image">
              <%} else{%>
              <img class="media-object user-img" src="images/avtarimg.png" alt="Image">
              <%} %>
            </div>
            <div class="media-body">
              <ul class="user-profile-list">
                <li><span>Full Name:</span><%=userpro.getFirstname() +" " + userpro.getLastname() %></li>
                <%if(null!=userpro.getAddress() && null!=userpro.getPincode()){ %>
               <li><span>Address:</span><%=userpro.getAddress() +" - " + userpro.getPincode()%></li>
<%--                 <li><span>Address:</span><%=userpro.getAddress() +" Pincode-" + userpro.getPincode()%></li> --%>
                <%}else{ %>
                <li><span>Address:</span>--</li>
            	<%}%>  
                <li><span>Email:</span><%=userpro.getEmailid() %></li>
                <%if(null!=userpro.getMobileno()){ %>
                <li><span>Phone:</span><%=userpro.getMobileno() %></li>
                <%}else{ %>
                <li><span>Phone:</span>--</li>
                <%} %>
                <%if(null!=userpro.getBirthdate()){ %>
                <li><span>Date of Birth:</span><%=userpro.getBirthdate() %></li>
              <%} else{%>
                <li><span>Date of Birth:</span>--</li>
            <%} %>  
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<%@include file="footer.jsp" %>
<!-- /footer -->

</div>
<!-- /main wrapper -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
</body>

</html>