<%@page import="com.safetyequipment.beans.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
		HttpSession httpSession = request.getSession(false);
		User user = (User) httpSession.getAttribute("userlogin");
	%>
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed "  data-ktheader-minimize="on" >
	<div class="kt-container  kt-container--fluid ">
		<!-- begin:: Brand -->
<div class="kt-header__brand " id="kt_header_brand">
	<div class="kt-header__brand-logo">
		<a href="index.html">
			<img alt="Logo" src="images/DLOGO.png"/>	
		</a>		
	</div> 
<!-- 	<div class="kt-header__brand-nav"> -->
<!-- 		<div class="dropdown"> -->
<!-- 			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> -->
<!-- 				Dashboard -->
<!-- 			</button> -->
<!-- 			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md"> -->
<!-- 				<ul class="kt-nav kt-nav--bold kt-nav--md-space kt-margin-t-20 kt-margin-b-20"> -->
<!-- 					<li class="kt-nav__item"> -->
<!-- 						<a class="kt-nav__link active" href="#"> -->
<!-- 							<span class="kt-nav__link-icon"><i class="flaticon2-user"></i></span> -->
<!-- 							<span class="kt-nav__link-text">Human Resources</span> -->
<!-- 						</a> -->
<!-- 					</li> -->
<!-- 					<li class="kt-nav__item"> -->
<!-- 						<a class="kt-nav__link" href="#"> -->
<!-- 							<span class="kt-nav__link-icon"><i class="flaticon-feed"></i></span> -->
<!-- 							<span class="kt-nav__link-text">Customer Relationship</span> -->
<!-- 						</a>                                                  -->
<!-- 					</li> -->
<!-- 					<li class="kt-nav__item"> -->
<!-- 						<a class="kt-nav__link" href="#"> -->
<!-- 							<span class="kt-nav__link-icon"><i class="flaticon2-settings"></i></span> -->
<!-- 							<span class="kt-nav__link-text">Order Processing</span> -->
<!-- 						</a>                                                  -->
<!-- 					</li> -->
<!-- 					<li class="kt-nav__item"> -->
<!-- 						<a class="kt-nav__link" href="#"> -->
<!-- 							<span class="kt-nav__link-icon"><i class="flaticon2-chart2"></i></span> -->
<!-- 							<span class="kt-nav__link-text">Accounting</span> -->
<!-- 						</a> -->
<!-- 					</li> -->
<!-- 					<li class="kt-nav__separator"></li> -->
<!-- 					<li class="kt-nav__item"> -->
<!-- 						<a class="kt-nav__link" href="#"> -->
<!-- 							<span class="kt-nav__link-icon"><i class="flaticon-security"></i></span> -->
<!-- 							<span class="kt-nav__link-text">Finance</span> -->
<!-- 						</a>                                                  -->
<!-- 					</li> -->
<!-- 					<li class="kt-nav__item"> -->
<!-- 						<a class="kt-nav__link" href="#"> -->
<!-- 							<span class="kt-nav__link-icon"><i class="flaticon2-cup"></i></span> -->
<!-- 							<span class="kt-nav__link-text">Administration</span> -->
<!-- 						</a> -->
<!-- 					</li> -->
<!-- 				</ul> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
</div>
<!-- end:: Brand -->		<!-- begin:: Header Topbar -->
<div class="kt-header__topbar">
	<!--begin: Search -->
<!-- 	<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown"  id="kt_quick_search_toggle"> -->
<!-- 		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px"> -->
<!-- 			<span class="kt-header__topbar-icon" ><i class="flaticon2-search-1"></i></span> -->
<!-- 		</div> -->
<!-- 		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg"> -->
<!-- 			<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown"> -->
<!--     <form method="get" class="kt-quick-search__form"> -->
<!--         <div class="input-group"> -->
<!--             <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div> -->
<!--             <input type="text" class="form-control kt-quick-search__input" placeholder="Search..."> -->
<!--             <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div> -->
<!--        	</div> -->
<!--     </form> -->
<!--     <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200"> -->

<!--     </div> -->
<!-- </div> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<!--end: Search -->

	<!--begin: Notifications -->

	<!--end: Notifications -->

	<!--begin: Quick actions -->
	<!--end: Quick actions -->

	<!--begin: Cart -->
		<!--end: Cart-->

	<!--begin: Quick panel toggler -->
<!-- 	<div class="kt-header__topbar-item" data-toggle="kt-tooltip" title="Quick panel" data-placement="top"> -->
<!-- 		<div class="kt-header__topbar-wrapper"> -->
<!-- 			<span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn"><i class="flaticon2-menu-2"></i></span> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<!--end: Quick panel toggler -->

	<!--begin: Language bar -->
<!-- 	<div class="kt-header__topbar-item kt-header__topbar-item--langs"> -->
<!-- 		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px"> -->
<!-- 			<span class="kt-header__topbar-icon"> -->
<!-- 				<img class="" src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo11/dist/assets/media/flags/012-uk.svg" alt="" /> -->
<!-- 			</span> -->
<!-- 		</div> -->
<!-- 		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim"> -->
<!-- 			<ul class="kt-nav kt-margin-t-10 kt-margin-b-10"> -->
<!--     <li class="kt-nav__item kt-nav__item--active"> -->
<!--         <a href="#" class="kt-nav__link"> -->
<!--             <span class="kt-nav__link-icon"><img src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo11/dist/assets/media/flags/020-flag.svg" alt="" /></span> -->
<!--             <span class="kt-nav__link-text">English</span> -->
<!--         </a> -->
<!--     </li> -->
<!--     <li class="kt-nav__item"> -->
<!--         <a href="#" class="kt-nav__link"> -->
<!--             <span class="kt-nav__link-icon"><img src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo11/dist/assets/media/flags/016-spain.svg" alt="" /></span> -->
<!--             <span class="kt-nav__link-text">Spanish</span> -->
<!--         </a> -->
<!--     </li> -->
<!--     <li class="kt-nav__item"> -->
<!--         <a href="#" class="kt-nav__link"> -->
<!--             <span class="kt-nav__link-icon"><img src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo11/dist/assets/media/flags/017-germany.svg" alt="" /></span> -->
<!--             <span class="kt-nav__link-text">German</span> -->
<!--         </a> -->
<!--     </li> -->
<!-- </ul>		</div> -->

<!-- 	</div> -->
	<!--end: Language bar -->

	<!--begin: User bar -->
	<div class="kt-header__topbar-item kt-header__topbar-item--user">
		<%if(user.getUsertype().equalsIgnoreCase("A")){ %>
		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
			<span class="kt-header__topbar-welcome kt-visible-desktop">Welcome,</span>
		
			<span class="kt-header__topbar-username kt-visible-desktop"><%=user.getFirstname() +" " + user.getLastname() %></span>
		
			<%if((null!=user.getImagestring()) && (user.getUsertype().equalsIgnoreCase("A"))){ %>
			<%System.out.print("utype" + user.getUsertype()); %>
			<img alt="Pic" src="data:image/jpeg;base64,<%=user.getImagestring()%>"/>
			<%} else{%>
			<img alt="Pic" src="images/avtarimg.png"/>
			<%} %>
			<span class="kt-header__topbar-icon kt-bg-brand kt-hidden"><b>S</b></span>
		</div><%} %>
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
			<!--begin: Head -->
    <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
        <div class="kt-user-card__avatar">
            <img class="kt-hidden-" alt="Pic" src="../../themes/metronic/theme/default/demo11/dist/assets/media/users/300_25.jpg" />
            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
        </div>
        <div class="kt-user-card__name">
            Sean Stone
        </div>
        <div class="kt-user-card__badge">
            <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>
        </div>
    </div>
<!--end: Head -->

<!--begin: Navigation -->
<div class="kt-notification">
    <a href="custom/apps/user/profile-1/personal-information.html" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-calendar-3 kt-font-success"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                My Profile
            </div>
            <div class="kt-notification__item-time">
                Account settings and more
            </div>
        </div>
    </a>
    <a href="custom/apps/user/profile-3.html" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-mail kt-font-warning"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                My Messages
            </div>
            <div class="kt-notification__item-time">
                Inbox and tasks
            </div>
        </div>
    </a>
    <a href="custom/apps/user/profile-2.html" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-rocket-1 kt-font-danger"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                My Activities
            </div>
            <div class="kt-notification__item-time">
                Logs and notifications
            </div>
        </div>
    </a>
    <a href="custom/apps/user/profile-3.html" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-hourglass kt-font-brand"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                My Tasks
            </div>
            <div class="kt-notification__item-time">
                latest tasks and projects
            </div>
        </div>
    </a>

    <a href="custom/apps/user/profile-1/overview.html" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-cardiogram kt-font-warning"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                Billing
            </div>
            <div class="kt-notification__item-time">
                billing & statements <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">2 pending</span>
            </div>
        </div>
    </a>
    <div class="kt-notification__custom kt-space-between">
        <a href="custom/user/login-v2.html" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>

        <a href="custom/user/login-v2.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a>
    </div>
</div>
<!--end: Navigation -->
		</div>
	</div>
	<!--end: User bar -->
</div>
<!-- end:: Header Topbar -->
	</div>
</div>
</body>
</html>