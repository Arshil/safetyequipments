<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.themefisher.com/elite-shop/track.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:42 GMT -->
<head>
  <meta charset="utf-8">
  <title>Dipen Safety</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="plugins/slick/slick.css">
  <link rel="stylesheet" href="plugins/venobox/venobox.css">
  <link rel="stylesheet" href="plugins/animate/animate.css">
  <link rel="stylesheet" href="plugins/aos/aos.css">
  <link rel="stylesheet" href="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.css">
  <link rel="stylesheet" href="plugins/nice-select/nice-select.css">
  <link rel="stylesheet" href="plugins/bootstrap-slider/bootstrap-slider.min.css">

  <!-- Main Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <!--Favicon-->
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="images/favicon.png" type="image/x-icon">

</head>

<body>

  <!-- preloader start -->
<!--   <div class="preloader"> -->
<!--     <img src="images/preloader.gif" alt="preloader"> -->
<!--   </div> -->
  <!-- preloader end -->

<!-- header -->
<%@include file="header.jsp" %>
<!-- /navigation -->

<!-- main wrapper -->
<div class="main-wrapper">

<!-- breadcrumb -->
<nav class="bg-gray py-3">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index-2.jsp">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Order Track</li>
    </ol>
  </div>
</nav>
<!-- /breadcrumb -->

<!-- track -->
<section class="section">
  <div class="container">
    <div class="row bg-dark">
      <div class="col-lg-12 text-center">
        <div class="p-4">
          <h5 class="text-white">TRACKING ORDER NO - 3587SMRT37</h5>
        </div>
      </div>
    </div>
    <div class="row bg-gray px-3 py-5">
      <div class="col-md-4 text-center">
        <p><strong>Shipped Via: </strong>Standard Delevery</p>
      </div>
      <div class="col-md-4 text-center">
        <p><strong>Status: </strong>Processing Order</p>
      </div>
      <div class="col-md-4 text-center">
        <p><strong>Expected Date: </strong>Sep 1, 2019</p>
      </div>
      <div class="d-flex justify-content-between w-100 mt-5 flex-column flex-sm-row">
        <div class="border mx-1 mb-2 border-primary p-2 p-md-4 text-center">
          <i class="ti-bag icon-md mb-3 d-inline-block"></i>
          <h5>Confirmed Order</h5>
        </div>
        <div class="border mx-1 mb-2 border-primary p-2 p-md-4 text-center">
          <i class="ti-settings icon-md mb-3 d-inline-block"></i>
          <h5>Processing Order</h5>
        </div>
        <div class="border mx-1 mb-2 p-2 p-md-4 text-center">
          <i class="ti-crown icon-md mb-3 d-inline-block"></i>
          <h5>Quality Check</h5>
        </div>
        <div class="border mx-1 mb-2 p-2 p-md-4 text-center">
          <i class="ti-truck icon-md mb-3 d-inline-block"></i>
          <h5>Product Dispatched</h5>
        </div>
        <div class="border mx-1 mb-2 p-2 p-md-4 text-center">
          <i class="ti-home icon-md mb-3 d-inline-block"></i>
          <h5>Product Delivered</h5>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /track -->

<!-- footer -->
 <%@include file="footer.jsp"%>
<!-- /footer -->

</div>
<!-- /main wrapper -->

<!-- jQuery -->
<script src="plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/venobox/venobox.min.js"></script>
<script src="plugins/aos/aos.js"></script>
<script src="plugins/syotimer/jquery.syotimer.js"></script>
<script src="plugins/instafeed/instafeed.min.js"></script>
<script src="plugins/zoom-master/jquery.zoom.min.js"></script>
<script src="plugins/bootstrap-touchspin-master/jquery.bootstrap-touchspin.min.js"></script>
<script src="plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="plugins/google-map/gmap.js"></script>
<!-- Main Script -->
<script src="js/script.js"></script>
</body>

<!-- Mirrored from demo.themefisher.com/elite-shop/track.jsp by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Oct 2019 04:22:42 GMT -->
</html>