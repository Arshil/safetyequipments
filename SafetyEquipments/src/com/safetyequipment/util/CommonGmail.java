package com.safetyequipment.util;


import java.util.Properties;
import java.util.Date;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class CommonGmail {

	static Properties properties = new Properties();
	static {
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
	}

	public void sendMail(String email, String subject, String text) {
		String returnStatement = null;
		try {
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					//return new PasswordAuthentication("arshilshah2000@gmail.com", "Arsh@0504");
					return new PasswordAuthentication("neelmodi1850@gmail.com", "mansi@1710");
				}
			};
			Session session = Session.getInstance(properties, auth);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("yourEmailId"));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setSentDate(new Date());
			message.setSubject(subject);
			message.setText(text);
			returnStatement = "The e-mail was sent successfully";
			System.out.println(returnStatement);
			Transport.send(message);
		} catch (Exception e) {
			returnStatement = "Error in sending mail";
			e.printStackTrace();
		}
	}
}