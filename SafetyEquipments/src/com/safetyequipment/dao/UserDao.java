package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Contact;
import com.safetyequipment.beans.User;

public interface UserDao {

	public int userregistration(Connection connection, User user) throws SQLException;

	public User validateuser(Connection connection, String email, String password) throws SQLException;

	public int updateuserdetails(Connection connection, User user) throws SQLException;

	public List<User> alluserlist(Connection connection) throws SQLException;

	public int deleteUser(Connection connection, int userid) throws SQLException;

	public User selectedUser(Connection connection, int userid) throws SQLException;

	public int editUser(Connection connection, User user) throws SQLException;

	public String selectedUserPassword(Connection connection, int userid) throws SQLException;

	public int userPasswordChange(Connection connection, int userid, String password) throws SQLException;

	public String selectedUserForgotPassword(Connection connection, String emailid) throws SQLException;

	public int validateEmail(Connection connection, String userEmail) throws SQLException;

	public int validateUserRegistrationEmail(Connection connection, String userEmail) throws SQLException;

	public int validateUserLogin(Connection connection, String emailid, String password) throws SQLException;

	public int makeInquiry(Connection connection, Contact contact) throws SQLException;

	public List<Contact> allInquiriesList(Connection connection) throws SQLException;

	public int activeUser(Connection connection, int userid) throws SQLException;

	public int updateUserAddress(Connection connection, User user) throws SQLException;

	public int updateUserPincode(Connection connection, User user) throws SQLException;

	public int updateUserMobile(Connection connection, User user) throws SQLException;

	
}
