
package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Category;

public interface CategoryDao {
	public int addcategorydetails(Connection connection, Category category) throws SQLException;

	public int deletecategoryDetails(Connection connection, int categoryId) throws SQLException;
	public List<Category> selectCategory(Connection connection) throws SQLException;

	public Category selectcategory(Connection connection, int category_id) throws SQLException;


	public int modifycategory(Connection connection, Category category) throws SQLException;

	public void deletesubcategory(Connection connection, int categoryId) throws SQLException;

	public void deleteproduct(Connection connection, int categoryId) throws SQLException;
}
