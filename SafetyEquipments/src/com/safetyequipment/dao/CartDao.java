package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Product;

public interface CartDao {

	public int addcartproducts(Connection connection, Cart cart) throws SQLException;

	public List<Cart> selectedUserCartList(Connection connection, int userid) throws SQLException;

	public int deleteCartProduct(Connection connection, int cartid) throws SQLException;

	public int removeproductfromcart(Connection connection, int userid, int productid) throws SQLException;

	public int updateCartQuantity(Connection connection, int userid, int productid, int qunatity) throws SQLException;

	public int removeAllCartProducts(Connection connection, int userid) throws SQLException;

}
