package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Product;

public interface ProductDao {

public 	int addProduct(Connection connection, Product product) throws SQLException;

public 	List<Product> selectAllProductlist(Connection connection) throws SQLException;

public Product selectedProduct(Connection connection, int id) throws SQLException;

public int editProduct(Connection connection, Product product) throws SQLException;

public int deleteProduct(Connection connection, int id) throws SQLException;

public List<Product> fetchProductList(Connection getconnection, int subcategoryid) throws SQLException;

public List<Product> fetchAllProductListByCategoryId(Connection connection, int parseInt) throws SQLException;

public ArrayList<Product> searchEnteredProduct(Connection connection, String productname) throws SQLException;

public int[] updateProductQuantity(Connection connection, List<Cart> cartlist) throws SQLException;

public int[] addProductQuantity(Connection connection, List<OrderDetails> orderDetailslist) throws SQLException;







}
