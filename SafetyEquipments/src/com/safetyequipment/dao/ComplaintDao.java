package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Complaint;

public interface ComplaintDao {

	public int addComplaint(Connection connection, Complaint complaint) throws SQLException;

	public int deleteComplaint(Connection connection, int complaintId) throws SQLException;

	public List<Complaint> allComplaintList(Connection connection) throws SQLException;

	public int updateComplaint(Connection connection, Complaint complaint) throws SQLException;

	public Complaint selectedComplaint(Connection connection, int complaintid) throws SQLException;

	public List<Complaint> selectedProductComplaintList(Connection connection, int productid) throws SQLException;

	public int registerUserComplaint(Connection connection, Complaint complaint) throws SQLException;

	public List<Complaint> userAllComplaints(Connection connection, int userid) throws SQLException;

	public Complaint selectedUserComplaint(Connection connection, int usercomplaintid) throws SQLException;

	public List<Complaint> complaintListSelectedProduct(Connection connection, int productid) throws SQLException;

	public int editUserComplaint(Connection connection, Complaint complaint) throws SQLException;

	public List<Complaint> allUserComplaints(Connection connection) throws SQLException;

	public Complaint editSelectedUserComplaint(Connection connection, int usercomplaintid) throws SQLException;

	public int editComplaintStatus(Connection connection, Complaint complaint) throws SQLException;

	

}
