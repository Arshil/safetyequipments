package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Payment;
import com.safetyequipment.beans.Shipping;

public interface MyOrderDao {

	public List<MyOrder> myOrderList(Connection connection, int userid) throws SQLException;

	public List<MyOrder> allOrderList(Connection connection) throws SQLException;

	public Shipping selectedShippingOrder(Connection connection, String orderkey) throws SQLException;

	public Order selectedOrder(Connection connection, String orderkey) throws SQLException;

	public List<OrderDetails> selectedOrderDetails(Connection connection, int orderid) throws SQLException;

	public MyOrder selectedMyOrder(Connection connection, String orderkey) throws SQLException;

	public Payment selectedOrderPayment(Connection connection, String orderkey) throws SQLException;

}
