package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.SubCategory;

public interface SubCategoryDao {

	public int addSubCategory(Connection connection, SubCategory subcategory) throws SQLException;

	public List<SubCategory> selectSubCategoryList(Connection connection) throws SQLException;

	

	public int editSubcategory(Connection connection, SubCategory subcategory) throws SQLException;

	public int deleteSubCategory(Connection connection, int id) throws SQLException;

	public SubCategory selectedSubCategory(Connection connection, int id) throws SQLException;

	public List<SubCategory> selectedCategorySubcategoryList(Connection connection, int categoryid) throws SQLException;

	public void deleteproduct(Connection connection, int subcategoryId) throws SQLException;

}
