package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.safetyequipment.beans.Category;

public interface GetCategory {

	ArrayList<Category> getcategorydetails(Connection connection) throws SQLException;

}
