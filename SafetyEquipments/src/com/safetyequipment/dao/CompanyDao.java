package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Company;

public interface CompanyDao {

	public int addcompanydetails(Connection connection, Company company) throws SQLException;

	public ArrayList<Company> getcompanydetails(Connection connection) throws SQLException;

	public int deletecompanyDetails(Connection connection, int companyid) throws SQLException;

	public Company selectcompany(Connection getconnection, int parseInt) throws SQLException;

	public int modifycompany(Connection connection, Company company) throws SQLException;

	public List<Company> getCompanyListOfSelectedProduct(Connection connection, int productid) throws SQLException;

}
