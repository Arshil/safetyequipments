package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.safetyequipment.beans.Payment;

public interface PaymentDao {

	public int registerPayment(Connection connection, Payment payment) throws SQLException;

	public int registerCODPayment(Connection connection, Payment payment) throws SQLException;

	
	
}
