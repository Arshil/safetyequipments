package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.Shipping;

public interface ShippingDao {

	public int addShippingOrder(Connection connection, Shipping shipping) throws SQLException;

	public int updatePaymentStatus(Connection connection, String orderkey) throws SQLException;

	public Shipping selectedOrder(Connection connection, String orderkey, int userid) throws SQLException;

	public List<Product> productListForComplaint(Connection connection, Shipping shipping) throws SQLException;

	public Shipping selecedShipping(Connection connection, String orderkey) throws SQLException;

	public int updateShippingStatus(Connection connection, Shipping shipping) throws SQLException;

	public int cancelOrder(Connection connection, Shipping shipping) throws SQLException;

	public int addShipping(Connection connection, Shipping shipping) throws SQLException;

	public int editShipping(Connection connection, Shipping shipping) throws SQLException;

	public double[] totalSelling(Connection connection) throws SQLException;

}
