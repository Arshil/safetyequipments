package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Product;
import com.safetyequipment.dao.ProductDao;

public class ProductDaoImpl implements ProductDao {
	// product_id, product_name, category_id, product_price, product_image,
	// company_id, subcategory_id,product_description, product_isactive
	String editProductquery = "update product_table set product_name  = ?,category_id = ?,product_price = ?,product_image = coalesce(?,product_image),company_id = ?, subcategory_id = ?,product_description = ?,product_quantity = ? ,product_explain=? where product_id = ?";
	String selectedProductquery = "SELECT c.category_name,s.sub_category_name, p.product_id, p.product_name, p.category_id, p.product_price, p.product_image, p.company_id, p.subcategory_id, p.product_description ,p.product_quantity,p.product_explain ,co.company_name from category_table c ,sub_category_table s ,product_table p ,company_table co  where  p.category_id = c.category_id and p.subcategory_id = s.sub_category_id and p.company_id = co.company_id and p.product_id = ? and p.product_isactive = 1";
	String insertProductquery = "insert into product_table(product_name,category_id,product_price,product_image,company_id,subcategory_id,product_description,product_quantity,product_explain)values(?,?,?,coalesce(?,product_image),?,?,?,?,?)";
	String selectallproductquery = " select c.category_name,s.sub_category_name, p.product_id, p.product_name, p.category_id, p.product_price, p.product_image, p.company_id, p.subcategory_id, p.product_description,p.product_explain,p.product_quantity,co.company_name from category_table c ,sub_category_table s ,product_table p ,company_table co  where  p.category_id = c.category_id and p.subcategory_id = s.sub_category_id and p.company_id = co.company_id and p.product_isactive = 1";
	String deleteProduct = "update product_table set product_isactive = ? where product_id = ? ";
	String selectallproductsbyCategoryid = "select * from product_table where category_id=? and product_isactive = 1";
	String selectedSubCategoryProductListQuery = "select * from product_table where subcategory_id=? and product_isactive=1";
	String getProductDetails = "select * from product_table where product_id = ?";
	String getSearchedProduct = "select * from product_table where product_isactive=1 and product_name like ? ";
	String updateProductQuantity = "update product_table set product_quantity = product_quantity - ? where product_id = ?";
	String addProductQuantity = "update product_table set product_quantity = product_quantity + ? where product_id = ?";

	@Override
	public int addProduct(Connection connection, Product product) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(insertProductquery)) {
			// product_name,category_id,product_price,product_image,company_id
			// ,subcategory_id,product_description
			preparedStatement.setString(1, product.getProductname());
			// System.out.println(subcategory.getSubcategoryname());
			preparedStatement.setInt(2, product.getCategoryid());
			preparedStatement.setInt(3, product.getProductprice());
			preparedStatement.setBlob(4, product.getProductimage());
			preparedStatement.setInt(5, product.getCompanyid());
			preparedStatement.setInt(6, product.getSubcategoryid());
			// System.out.println(product.getCategoryid());
			preparedStatement.setString(7, product.getDescription());
			preparedStatement.setInt(8, product.getQuantity());
			preparedStatement.setString(9, product.getAboutproduct());
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public List<Product> selectAllProductlist(Connection connection) throws SQLException {
		List<Product> productlist = new ArrayList<Product>();
		try (PreparedStatement preparedStatement = connection.prepareStatement(selectallproductquery)) {
			// product_name,category_id,product_price,product_image,company_id
			// ,subcategory_id,product_description
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Product product = new Product();
					product.setProductid(resultSet.getInt("product_id"));
					product.setProductname(resultSet.getString("product_name"));
					product.setCategoryid(resultSet.getInt("category_id"));
					product.setCategoryname(resultSet.getString("category_name"));
					product.setSubcategoryid(resultSet.getInt("subcategory_id"));
					product.setSubcategoryname(resultSet.getString("sub_category_name"));
					product.setCompanyid(resultSet.getInt("company_id"));
					product.setCompanyname(resultSet.getString("company_name"));
					product.setDescription(resultSet.getString("product_description"));
					product.setProductprice(resultSet.getInt("product_price"));
					product.setQuantity(resultSet.getInt("product_quantity"));
					product.setAboutproduct(resultSet.getString("product_explain"));
					System.out.println("in dao pexp" + product.getAboutproduct());
					if (resultSet.getString("product_image") != null) {
						product.setImagestring(Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
					}
					productlist.add(product);
				}
				return productlist;
			}

		}
	}

	@Override
	public Product selectedProduct(Connection connection, int id) throws SQLException {

		try (PreparedStatement preparedStatement = connection.prepareStatement(selectedProductquery)) {
			preparedStatement.setInt(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				while (resultSet.next()) {
					Product product = new Product();
					// product_id, product_name, category_id, product_price,
					// product_image, company_id, subcategory_id, product_description,
					// product_isactive
					product.setProductid(resultSet.getInt("product_id"));
					product.setProductname(resultSet.getString("product_name"));
					product.setCategoryid(resultSet.getInt("category_id"));
					product.setSubcategoryid(resultSet.getInt("subcategory_id"));
					product.setSubcategoryname(resultSet.getString("sub_category_name"));
					product.setProductprice(resultSet.getInt("product_price"));
					product.setCompanyid(resultSet.getInt("company_id"));
					product.setQuantity(resultSet.getInt("product_quantity"));
					product.setDescription(resultSet.getString("product_description"));
					product.setAboutproduct(resultSet.getString("product_explain"));
					if (resultSet.getString("product_image") != null) {
						product.setImagestring(Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
					}
					return product;
				}
			}
		}
		return null;

	}

	@Override
	public int editProduct(Connection connection, Product product) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(editProductquery);) {
			// String editProductquery = "update Product_table set product_name = ?
			// ,category_id = ?,product_price = ?,
			// product_image = coalesce(?,product_image),company_id = ?,
			// subcategory_id = ?,product_description = ? where product_id = ?";
			preparedStatement.setString(1, product.getProductname());
			preparedStatement.setInt(2, product.getCategoryid());
			preparedStatement.setInt(3, product.getProductprice());
			// preparedStatement.setString(3, product.getDescription());
			preparedStatement.setBlob(4, product.getProductimage());
			preparedStatement.setInt(5, product.getCompanyid());
			preparedStatement.setInt(6, product.getSubcategoryid());
			preparedStatement.setString(7, product.getDescription());
			preparedStatement.setInt(8, product.getQuantity());
			preparedStatement.setString(9, product.getAboutproduct());
			preparedStatement.setInt(10, product.getProductid());
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public int deleteProduct(Connection connection, int id) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deleteProduct);) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, id);
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public List<Product> fetchProductList(Connection getconnection, int subcategoryid) throws SQLException {
		try (PreparedStatement preparedStatement = getconnection
				.prepareStatement(selectedSubCategoryProductListQuery)) {
			List<Product> productlist = new ArrayList<>();
			preparedStatement.setInt(1, subcategoryid);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {

				while (resultSet.next()) {
					Product product = new Product();
					product.setProductid(resultSet.getInt("product_id"));
					product.setProductname(resultSet.getString("product_name"));
					product.setCategoryid(resultSet.getInt("category_id"));
					product.setSubcategoryid(resultSet.getInt("subcategory_id"));
					// product.setSubcategoryname(resultSet.getString("sub_category_name"));
					product.setProductprice(resultSet.getInt("product_price"));
					product.setCompanyid(resultSet.getInt("company_id"));
					product.setDescription(resultSet.getString("product_description"));
					product.setQuantity(resultSet.getInt("product_quantity"));
					product.setAboutproduct(resultSet.getString("product_explain"));
					if (resultSet.getString("product_image") != null) {
						product.setImagestring(Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
					}
					productlist.add(product);
				}
				return productlist;
			}
		}

		// TODO Auto-generated method stub

	}

	@Override
	public List<Product> fetchAllProductListByCategoryId(Connection connection, int categoryid) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(selectallproductsbyCategoryid)) {
			List<Product> productlist = new ArrayList<>();
			preparedStatement.setInt(1, categoryid);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				Product product;
				while (resultSet.next()) {
					product = new Product();
					product.setProductid(resultSet.getInt("product_id"));
					product.setProductname(resultSet.getString("product_name"));
					product.setCategoryid(resultSet.getInt("category_id"));
					product.setSubcategoryid(resultSet.getInt("subcategory_id"));
					// product.setSubcategoryname(resultSet.getString("sub_category_name"));
					product.setProductprice(resultSet.getInt("product_price"));
					product.setCompanyid(resultSet.getInt("company_id"));
					product.setDescription(resultSet.getString("product_description"));
					product.setQuantity(resultSet.getInt("product_quantity"));
					product.setAboutproduct(resultSet.getString("product_explain"));
					if (resultSet.getString("product_image") != null) {
						product.setImagestring(Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
					}
					productlist.add(product);
				}
				return productlist;
			}
		}
		// TODO Auto-generated method stub
	}

	@Override
	public ArrayList<Product> searchEnteredProduct(Connection connection, String productname) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(getSearchedProduct)) {
			String p1 = "%";
			String p2 = productname;
			String p3 = "%";
			String pro = p1 + p2 + p3;
			System.out.println("pro dao name" + pro + "pro size dao " + pro.length());
			preparedStatement.setString(1, pro);
			ArrayList<Product> prolist = new ArrayList<>();
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				while (resultSet.next()) {
					Product product = new Product();
					product.setProductid(resultSet.getInt("product_id"));
					product.setProductname(resultSet.getString("product_name"));
					product.setCategoryid(resultSet.getInt("category_id"));
					product.setSubcategoryid(resultSet.getInt("subcategory_id"));
					// product.setSubcategoryname(resultSet.getString("sub_category_name"));
					product.setProductprice(resultSet.getInt("product_price"));
					product.setCompanyid(resultSet.getInt("company_id"));
					product.setQuantity(resultSet.getInt("product_quantity"));
					product.setDescription(resultSet.getString("product_description"));
					product.setAboutproduct(resultSet.getString("product_explain"));
					if (resultSet.getString("product_image") != null) {
						product.setImagestring(Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
					}
					prolist.add(product);
				}
				return prolist;
			}
		}

	}

	@Override
	public int[] updateProductQuantity(Connection connection, List<Cart> cartlist) throws SQLException {
		// TODO Auto-generated method stub

		try (PreparedStatement preparedstatment = connection.prepareStatement(updateProductQuantity)) {

			for (Cart cart : cartlist) {
				preparedstatment.setInt(1, cart.getQuatity());
				preparedstatment.setInt(2, cart.getProductid());
				preparedstatment.addBatch();
			}
			return preparedstatment.executeBatch();

		}

	}

	@Override
	public int[] addProductQuantity(Connection connection, List<OrderDetails> orderDetailslist) throws SQLException {
		// TODO Auto-generated method stub

		try (PreparedStatement preparedstatment = connection.prepareStatement(addProductQuantity)) {

			for (OrderDetails orderDetails : orderDetailslist) {
				preparedstatment.setInt(1, orderDetails.getProductquantity());
				preparedstatment.setInt(2, orderDetails.getProductid());
				preparedstatment.addBatch();
			}
			return preparedstatment.executeBatch();

		}

	}
}
