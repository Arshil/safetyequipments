package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Product;
import com.safetyequipment.dao.CartDao;

public class CartDaoImpl implements CartDao {

	String addproductincart = "insert into cart_table(pro_id, u_id, cart_date) values(?,?,now())";
	String selectedusercart = "select c.category_name,s.sub_category_name, p.product_id,p.product_name, p.category_id, p.product_price, p.product_image,p.company_id, p.subcategory_id, p.product_description,p.product_quantity ,co.company_name , ca.cart_id , ca.pro_id , ca.u_id,ca.cart_date,ca.cart_quantity from category_table c ,sub_category_table s ,product_table p ,company_table co , cart_table ca where   p.category_id = c.category_id and p.subcategory_id = s.sub_category_id  and p.company_id = co.company_id and p.product_id = ca.pro_id and ca.u_id = ?";
	String usercartproductdeletequery = "delete from cart_table where cart_id = ? ";
	String removeproductfromcart = "delete from cart_table where u_id=? and pro_id=?";
	String updateQuantity = "update cart_table set cart_quantity = ? where u_id = ?  and pro_id = ?";
	String removeAllCartProducts ="delete from cart_table where u_id = ?"; 
//cart_id, ,pro_id, u_id cart_date, product_quantity
	@Override
	public int addcartproducts(Connection connection, Cart cart) throws SQLException {
		// TODO Auto-generated method stub

		try (PreparedStatement preparedstatment = connection.prepareStatement(addproductincart)) {
			preparedstatment.setInt(1, cart.getProductid());
			preparedstatment.setInt(2, cart.getUserid());
			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public List<Cart> selectedUserCartList(Connection connection, int userid) throws SQLException {
		List<Cart> cartlist = new ArrayList<Cart>();
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedusercart)) {
			preparedstatment.setInt(1, userid);
			System.out.println("cartlist fill start");
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				// cart_id, pro_id, u_id, cart_date
				while (resultSet.next()) {
					Cart cart = new Cart();
					Product productobj = new Product();
					cart.setCartid(resultSet.getInt("cart_id"));
					cart.setUserid(resultSet.getInt("u_id"));
					cart.setProductid(resultSet.getInt("pro_id"));
					cart.setQuatity(resultSet.getInt("cart_quantity"));
					productobj.setProductid(resultSet.getInt("product_id"));
					System.out.println(resultSet.getInt("product_id"));
					productobj.setProductname(resultSet.getString("product_name"));
					productobj.setCategoryid(resultSet.getInt("category_id"));
					productobj.setCategoryname(resultSet.getString("category_name"));
					productobj.setSubcategoryid(resultSet.getInt("subcategory_id"));
					productobj.setSubcategoryname(resultSet.getString("sub_category_name"));
					productobj.setCompanyid(resultSet.getInt("company_id"));
					productobj.setCompanyname(resultSet.getString("company_name"));
					productobj.setDescription(resultSet.getString("product_description"));
					productobj.setProductprice(resultSet.getInt("product_price"));
					productobj.setQuantity(resultSet.getInt("product_quantity"));
					if (resultSet.getString("product_image") != null) {
						productobj.setImagestring(
								Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
					}
					cart.setProduct(productobj);
					// System.out.println("cart product id :"+resultSet.getInt("pro_id"));
					cartlist.add(cart);
					// System.out.println("cartlist fill");
				}
			}
			// System.out.println("cartlist full");
			return cartlist;

		}

	}

	@Override
	public int deleteCartProduct(Connection connection, int cartid) throws SQLException {
		// TODO Auto-generated method stub

		try (PreparedStatement preparedstatment = connection.prepareStatement(usercartproductdeletequery)) {
			preparedstatment.setInt(1, cartid);
			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public int removeproductfromcart(Connection connection, int userid, int productid) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(removeproductfromcart)) {
			preparedStatement.setInt(1, userid);
			preparedStatement.setInt(2, productid);
			return preparedStatement.executeUpdate();

		}

	}

	@Override
	public int updateCartQuantity(Connection connection, int userid, int productid, int qunatity) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuantity)) {
			preparedStatement.setInt(1, qunatity);
			preparedStatement.setInt(2, userid);
			preparedStatement.setInt(3, productid);
			return preparedStatement.executeUpdate();

		}

	}
	
	@Override
	public int removeAllCartProducts(Connection connection, int userid) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedstatment = connection.prepareStatement(removeAllCartProducts)) {
			preparedstatment.setInt(1, userid);
			return preparedstatment.executeUpdate();
		}
	}
	
	
	
}