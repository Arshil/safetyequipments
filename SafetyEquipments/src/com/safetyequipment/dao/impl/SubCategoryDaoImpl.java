package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.dao.SubCategoryDao;

public class SubCategoryDaoImpl implements SubCategoryDao {

	String selectSubcategoryList = " SELECT category_table.category_name,sub_category_table.category_id,sub_category_table.sub_category_id, sub_category_table.sub_category_name,sub_category_table.sub_category_description,sub_category_table.sub_category_description, sub_category_table.sub_category_image FROM category_table  INNER JOIN sub_category_table ON category_table.category_id = sub_category_table.category_id where sub_category_table.sub_category_status = ?";
	String insertSubcategory = "insert into sub_category_table(sub_category_name, category_id, sub_category_description, sub_category_image,sub_category_status) values(?,?,?,coalesce(?,sub_category_image),?)";
	String selectedSubCategory = "select * from sub_category_table where sub_category_id = ?";
	String editSubcategory = " update sub_category_table set sub_category_name = ?, category_id = ?, sub_category_description = ?,sub_category_image = coalesce(?,sub_category_image) where sub_category_id = ?";
	String deleteSubCategory = "update sub_category_table set sub_category_status = ? where sub_category_id = ?";
	String selectedCategorySubcategory = " SELECT c.category_name,c.category_id,s.sub_category_name,s.sub_category_id,s.category_id,s.sub_category_description,s.sub_category_image,s.sub_category_status FROM  category_table c INNER JOIN sub_category_table s  ON c.category_id = s.category_id  WHERE s.category_id = ? and s.sub_category_status = ?";
	String deleteproductquery = "update product_table set product_isactive = ? where subcategory_id = ? ";
	/*
	 * SELECT c.category_name,c.category_id,s.sub_category_name,s.sub_category_id,s.
	 * category_id,s.sub_category_description,s.sub_category_image,s.
	 * sub_category_status FROM category_table c INNER JOIN sub_category_table s ON
	 * c.category_id = s.category_id WHERE s.category_id = 3 and
	 * s.sub_category_status =1
	 */

	// sub_category_id, sub_category_name, category_id,
	// sub_category_description,sub_category_image, sub_category_status
	@Override
	public List<SubCategory> selectSubCategoryList(Connection connection) throws SQLException {

		List<SubCategory> subcategorylist = new ArrayList<SubCategory>();
		try (PreparedStatement preparedstatement = connection.prepareStatement(selectSubcategoryList)) {
			preparedstatement.setInt(1, 1);
			try (ResultSet rest = preparedstatement.executeQuery()) {
				while (rest.next()) {
					SubCategory subcategory = new SubCategory();
					subcategory.setSubcategoryid(rest.getInt("sub_category_id"));
					subcategory.setSubcategoryname(rest.getString("sub_category_name"));
					subcategory.setCategoryid(rest.getInt("category_id"));
					subcategory.setDescription(rest.getString("sub_category_description"));
					subcategory.setCategoryname(rest.getString("category_name"));
					if (rest.getString("sub_category_image") != null) {
						subcategory.setImagestring(
								Base64.getEncoder().encodeToString(rest.getBytes("sub_category_image")));
					}
					subcategorylist.add(subcategory);
				}
				return subcategorylist;
			}
		}
	}

	@Override
	public int addSubCategory(Connection connection, SubCategory subcategory) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(insertSubcategory)) {
			preparedStatement.setString(1, subcategory.getSubcategoryname());
			// System.out.println(subcategory.getSubcategoryname());
			preparedStatement.setInt(2, subcategory.getCategoryid());
			// System.out.println(subcategory.getCategoryid());
			preparedStatement.setString(3, subcategory.getDescription());
			preparedStatement.setBlob(4, subcategory.getSubcategoryimage());
			preparedStatement.setInt(5, 1);

			return preparedStatement.executeUpdate();
		}

	}

	@Override
	public SubCategory selectedSubCategory(Connection connection, int id) throws SQLException {

		try (PreparedStatement preparedStatement = connection.prepareStatement(selectedSubCategory)) {
			preparedStatement.setInt(1, id);
			try (ResultSet rest = preparedStatement.executeQuery();) {
				while (rest.next()) {
					SubCategory subcategory = new SubCategory();
					subcategory.setCategoryid(rest.getInt("category_id"));
					subcategory.setSubcategoryid(rest.getInt("sub_category_id"));
					subcategory.setSubcategoryname(rest.getString("sub_category_name"));
					subcategory.setDescription(rest.getString("sub_category_description"));
					if (rest.getString("sub_category_image") != null) {
						subcategory.setImagestring(
								Base64.getEncoder().encodeToString(rest.getBytes("sub_category_image")));
					}
					return subcategory;
				}

			}
		}
		return null;

	}

	@Override
	public int editSubcategory(Connection connection, SubCategory subcategory) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(editSubcategory);) {
			// String editSubcategory = " update sub_category_table set sub_category_name =
			// ?, category_id = ?,
			// sub_category_description = ?,sub_category_image =
			// coalesce(?,sub_category_image) where sub_category_id = ?";
			preparedStatement.setString(1, subcategory.getSubcategoryname());
			preparedStatement.setInt(2, subcategory.getCategoryid());
			preparedStatement.setString(3, subcategory.getDescription());
			preparedStatement.setBlob(4, subcategory.getSubcategoryimage());
			preparedStatement.setInt(5, subcategory.getSubcategoryid());
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public int deleteSubCategory(Connection connection, int id) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deleteSubCategory);) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, id);
			int deletedSubcategory =  preparedStatement.executeUpdate();
			deleteproduct(connection,id);
			return deletedSubcategory;
		}
	}

	@Override
	public void deleteproduct(Connection connection, int subcategoryId) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deleteproductquery)) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, subcategoryId);
			preparedStatement.executeUpdate();
		}
	}
	
	
	
	public List<SubCategory> selectedCategorySubcategoryList(Connection connection, int categoryid)
			throws SQLException {

		List<SubCategory> subcategorylist = new ArrayList<SubCategory>();
		try (PreparedStatement preparedstatement = connection.prepareStatement(selectedCategorySubcategory)) {
			preparedstatement.setInt(1, categoryid);
			preparedstatement.setInt(2, 1);
			try (ResultSet rest = preparedstatement.executeQuery()) {
				while (rest.next()) {
					SubCategory subcategory = new SubCategory();
					subcategory.setSubcategoryid(rest.getInt("sub_category_id"));
					subcategory.setSubcategoryname(rest.getString("sub_category_name"));
					subcategory.setCategoryid(rest.getInt("category_id"));
					subcategory.setDescription(rest.getString("sub_category_description"));
					subcategory.setCategoryname(rest.getString("category_name"));
//					if (rest.getString("sub_category_image") != null) {
//						subcategory.setImagestring(
//								Base64.getEncoder().encodeToString(rest.getBytes("sub_category_image")));
//					}
					subcategorylist.add(subcategory);
				}
				return subcategorylist;
			}
		}
	}

}
