package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.dao.ShippingDao;

public class ShippingDaoImpl implements ShippingDao {

	// shipping_id, order_key, order_date, shipping_address, shipping_date,
	// shipping_status, payment_status, order_amt, user_id, no_of_products
	String insertShippingOrder = "insert into shipping_table(order_key,shipping_address,order_amt,user_id,no_of_products,order_date)values(?,?,?,?,?,now())";
	String updatePaymentStatus = "update shipping_table set payment_status = ? where order_key = ?";
	String selectedOrder = "select * from shipping_table where order_key = ? and user_id = ?";
	String productListForComplaint = "select s.shipping_id, s.order_key, s.user_id, s.no_of_products,o.order_id, o.order_key, o.user_id,od.orderdetails_id, od.order_id, od.product_id,od.product_quantity,p.product_id,p.product_name from shipping_table s,order_table o,orderdetails_table od, product_table p where s.order_key = o.order_key and o.order_id = od.order_id and s.user_id = o.user_id  and p.product_id = od.product_id and s.user_id = ? and s.shipping_id = ? and s.order_key = ?";
	String selectedShipping = "select * from shipping_table where order_key = ?";
	String updateShippingStatus = "update shipping_table set  payment_status = ? ,  shipping_status = ? where order_key = ?  ";
	String cancelOrder = "update shipping_table set shipping_status = ? where order_key = ?";
	String addShipping = "update shipping_table set shipping_date = ? where order_key = ?";
	String editShipping = "update shipping_table set shipping_date = ? where order_key = ?";
	String totalSellingQuery = "select * from  shipping_table where shipping_status = ? and payment_status = ?";

	@Override
	public int addShippingOrder(Connection connection, Shipping shipping) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(insertShippingOrder)) {
			preparedstatment.setString(1, shipping.getOrderkey());
			preparedstatment.setString(2, shipping.getAddress());
			preparedstatment.setDouble(3, shipping.getAmount());
			preparedstatment.setInt(4, shipping.getUserid());
			preparedstatment.setInt(5, shipping.getItems());
			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public int updatePaymentStatus(Connection connection, String orderkey) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(updatePaymentStatus)) {
			preparedstatment.setString(1, "completed");
			preparedstatment.setString(2, orderkey);
			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public Shipping selectedOrder(Connection connection, String orderkey, int userid) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedOrder)) {
			preparedstatment.setString(1, orderkey);
			preparedstatment.setInt(2, userid);
			int cnt = 0;
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				// shipping_id, order_key, order_date, shipping_address,
				// shipping_date, shipping_status, payment_status,
				// order_amt, user_id, no_of_products
				Shipping shipping = new Shipping();
				while (resultSet.next()) {
					++cnt;
					if (cnt > 0) {
						shipping.setShippingid(resultSet.getInt("shipping_id"));
						shipping.setOrderkey(resultSet.getString("order_key"));
						shipping.setUserid(resultSet.getInt("user_id"));
						shipping.setAddress(resultSet.getString("shipping_address"));
						shipping.setOrderdate(String.valueOf(resultSet.getDate("order_date")));
						shipping.setShippingdate(String.valueOf(resultSet.getDate("shipping_date")));
						shipping.setPaymentstutus(resultSet.getString("payment_status"));
						shipping.setShippingstatus(resultSet.getString("shipping_status"));
						shipping.setAmount(resultSet.getDouble("order_amt"));
						shipping.setItems(resultSet.getInt("no_of_products"));
						return shipping;
					} else {
						return null;
					}
				}

			}
		}
		return null;
	}

	@Override
	public List<Product> productListForComplaint(Connection connection, Shipping shipping) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(productListForComplaint)) {
			preparedstatment.setInt(1, shipping.getUserid());
			preparedstatment.setInt(2, shipping.getShippingid());
			preparedstatment.setString(3, shipping.getOrderkey());
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				List<Product> productlist = new ArrayList<Product>();
				while (resultSet.next()) {
					Product product = new Product();
					product.setProductid(resultSet.getInt("product_id"));
					product.setQuantity(resultSet.getInt("product_quantity"));
					product.setProductname(resultSet.getString("product_name"));
					productlist.add(product);
				}
				return productlist;
			}
		}
	}

	@Override
	public Shipping selecedShipping(Connection connection, String orderkey) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedShipping)) {
			preparedstatment.setString(1, orderkey);
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				while (resultSet.next()) {
					Shipping shipping = new Shipping();
					shipping.setOrderkey(resultSet.getString("order_key"));
					shipping.setUserid(resultSet.getInt("user_id"));
					shipping.setShippingstatus(resultSet.getString("shipping_status"));
					shipping.setOrderdate(String.valueOf(resultSet.getDate("order_date")));
					shipping.setPaymentstutus(resultSet.getString("payment_status"));
					shipping.setAmount(resultSet.getDouble("order_amt"));
					if (resultSet.getDate("shipping_date") == null) {
						shipping.setShippingdate("In 7-10 working days");
					} else {
						shipping.setShippingdate(String.valueOf(resultSet.getDate("shipping_date")));
					}
					return shipping;
				}

			}
		}
		return null;
	}

	@Override
	public int updateShippingStatus(Connection connection, Shipping shipping) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(updateShippingStatus)) {
			preparedstatment.setString(1, shipping.getPaymentstutus());
			preparedstatment.setString(2, shipping.getShippingstatus());
			preparedstatment.setString(3, shipping.getOrderkey());
			return preparedstatment.executeUpdate();
		}
		// TODO Auto-generated method stub

	}

	@Override
	public int cancelOrder(Connection connection, Shipping shipping) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(cancelOrder)) {
			preparedstatment.setString(1, "cancelled");
			preparedstatment.setString(2, shipping.getOrderkey());
			return preparedstatment.executeUpdate();
		}
		// TODO Auto-generated method stub

	}

	@Override
	public int addShipping(Connection connection, Shipping shipping) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(addShipping)) {
			preparedstatment.setDate(1, Date.valueOf(shipping.getShippingdate()));
			preparedstatment.setString(2, shipping.getOrderkey());
			return preparedstatment.executeUpdate();
		}
		// TODO Auto-generated method stub

	}

	@Override
	public int editShipping(Connection connection, Shipping shipping) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(editShipping)) {
			preparedstatment.setDate(1, Date.valueOf(shipping.getShippingdate()));
			preparedstatment.setString(2, shipping.getOrderkey());
			return preparedstatment.executeUpdate();
		}
		// TODO Auto-generated method stub

	}
	@Override
	public double[] totalSelling(Connection connection) throws SQLException {
		double sell[] = new double[12];
		for (int i = 0; i < sell.length; i++) {
			sell[i] = 0;
		}
		try (PreparedStatement preparedstatment = connection.prepareStatement(totalSellingQuery)) {
			preparedstatment.setString(1, "completed");
			preparedstatment.setString(2, "completed");
			// return preparedstatment.executeUpdate();
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				while (resultSet.next()) {
					int month = Integer.parseInt(String.valueOf(resultSet.getDate("order_date")).substring(5,7));
					sell[month - 1] += resultSet.getDouble("order_amt");
					//System.out.println("month :"+month);
					//System.out.println("order amount : "+resultSet.getDouble("order_amt"));
				}
				return sell;
			}
			// TODO Auto-generated method stub

		}
	}

}
