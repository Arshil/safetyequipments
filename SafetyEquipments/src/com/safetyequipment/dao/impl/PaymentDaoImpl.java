package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.safetyequipment.beans.Payment;
import com.safetyequipment.dao.PaymentDao;

public class PaymentDaoImpl implements PaymentDao {
	String registerPayment = "insert into payment_table(bank_name, bank_txn_id, currency, gateway_name, merchant_id, order_id, payment_mode, response_code, response_message, status, txn_amount, txn_id, txn_date)values(?,?,?,?,?,?,?,?,?,?,?,?,now())";
	String codPayment = "insert into payment_table(currency,order_id, payment_mode, status, txn_amount, txn_id ,txn_date)values(?,?,?,?,?,?,now())";
	@Override
	public int registerPayment(Connection connection, Payment payment) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(registerPayment)) {
			// bank_name, bank_txn_id, currency, gateway_name,
			// merchant_id, order_id, payment_mode, response_code,
			// response_message, status, txn_amount, txn_id, txn_date
			preparedStatement.setString(1, payment.getBankname());
			preparedStatement.setString(2, payment.getBanktxnid());
			preparedStatement.setString(3, payment.getCurrency());
			preparedStatement.setString(4, payment.getGatewayname());
			preparedStatement.setString(5, payment.getMerchantid());
			preparedStatement.setString(6, payment.getOrderid());
			preparedStatement.setString(7, payment.getPaymentmode());
			preparedStatement.setString(8, payment.getResponsecode());
			preparedStatement.setString(9, payment.getResponsemsg());
			preparedStatement.setString(10, payment.getStatus());
			preparedStatement.setDouble(11, payment.getTxnamount());
			preparedStatement.setString(12, payment.getTxnid());
			return preparedStatement.executeUpdate();
		}

	}
	
	@Override
	public int registerCODPayment(Connection connection, Payment payment) throws SQLException
	{
		try (PreparedStatement preparedStatement = connection.prepareStatement(codPayment)) {
			preparedStatement.setString(1, payment.getCurrency());
			preparedStatement.setString(2,payment.getOrderid());
			preparedStatement.setString(3,payment.getPaymentmode());
			preparedStatement.setString(4,payment.getStatus());
			preparedStatement.setDouble(5,payment.getTxnamount());
			preparedStatement.setString(6, payment.getTxnid());
			return preparedStatement.executeUpdate();
		}
	}
}
