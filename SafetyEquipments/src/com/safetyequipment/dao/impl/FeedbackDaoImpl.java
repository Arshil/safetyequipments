package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.safetyequipment.beans.Feedback;
import com.safetyequipment.beans.User;
import com.safetyequipment.dao.FeedbackDao;

public class FeedbackDaoImpl implements FeedbackDao {

    String insertfeedback = "insert into feedback_table (user_id,feedback_review, feedback_rate,feedback_date) values(?,?,?,now())";
   String selectallfeedback = "select f.feedback_id, f.user_id, f.feedback_review, f.feedback_rate, f.feedback_date ,u.user_id,u.user_profileimage,u.user_email from user_table u, feedback_table f where  f.user_id = u.user_id ; ";
    @Override
	public int addfeedback(Connection connection, Feedback feedback) throws SQLException {
		// TODO Auto-generated method stub
	try (PreparedStatement preparedstatment = connection.prepareStatement(insertfeedback)) {
			
		preparedstatment.setInt(1, feedback.getUserid());
		preparedstatment.setString(2, feedback.getDescription());
		preparedstatment.setInt(3,feedback.getRatings());
		return preparedstatment.executeUpdate();	
		}	
	}
    
    
    @Override
	public List<Feedback> allFeedbackList(Connection connection) throws SQLException {
		List<Feedback> feedbacklist = new ArrayList<Feedback>();
		try (PreparedStatement preparedStatement = connection.prepareStatement(selectallfeedback)) {
			try (ResultSet rest = preparedStatement.executeQuery()) {
				while (rest.next()) {
					Feedback feedback= new Feedback();
					System.out.println("called dao");
					//feedback_id, user_id, feedback_review, feedback_rate,
					//feedback_date, user_id, user_profileimage
					feedback.setFeedbackid(rest.getInt("feedback_id"));
					feedback.setUserid(rest.getInt("user_id"));
					feedback.setDescription(rest.getString("feedback_review"));
					feedback.setRatings(rest.getInt("feedback_rate"));
					feedback.setUseremailid(rest.getString("user_email"));
					feedback.setFeedbackDate(String.valueOf(rest.getDate("feedback_date")));
					if (rest.getString("user_profileimage") != null) {
						feedback.setProfileImageString(Base64.getEncoder().encodeToString(rest.getBytes("user_profileimage")));
					}
					feedbacklist.add(feedback);
				}
				System.out.println("called exit");
				return feedbacklist;
			}
		}
	}
}
