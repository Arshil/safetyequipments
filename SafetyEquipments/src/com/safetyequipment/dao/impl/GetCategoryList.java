package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;

import com.safetyequipment.beans.Category;
import com.safetyequipment.dao.GetCategory;

public class GetCategoryList implements GetCategory{
	
	String getcategorydetailsquery = "select * from category_table where category_status = 1";

	@Override
	public ArrayList<Category> getcategorydetails(Connection connection) throws SQLException {
		// TODO Auto-generated method st
		try(PreparedStatement preparedstatment = connection.prepareStatement(getcategorydetailsquery); ResultSet resultSet = preparedstatment.executeQuery())
		{
		
			ArrayList<Category> categorylist = new ArrayList<>();
			while (resultSet.next()) 
			{
				Category category = new Category();
				category.setCategoryname(resultSet.getString("category_name"));
				category.setDescription(resultSet.getString("category_description"));
				category.setCategoryid(resultSet.getInt("category_id"));
				
				if (resultSet.getBytes("category_image") != null) {
					category.setCategoryImageString(Base64.getEncoder().encodeToString(resultSet.getBytes("category_image")));

					
				}
				
				categorylist.add(category);
			}
			return categorylist;
		} 
	}
}
