package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Payment;
import com.safetyequipment.dao.MyOrderDao;

public class MyOrderDaoImpl implements MyOrderDao {
	String myOrder = "select * from shipping_table where user_id = ? order by order_date desc";
	String allOrders = "select shipping_id, order_key, order_date, shipping_address, shipping_date, shipping_status, payment_status,order_amt, user_id, no_of_products from shipping_table order by order_date desc ";
	String selectedShippingOrder = "select * from shipping_table where order_key = ?";
	String selectedOrder = "select * from order_table where order_key = ?";
	String selectedOrderDetails = "select od.orderdetails_id, od.order_id, od.product_id, od.product_price, od.product_quantity,p.product_id ,p.product_image,p.product_name from orderdetails_table od , product_table p where od.product_id = p.product_id and od.order_id = ?";
	String selectedOrderPayment = "select * from payment_table where order_id = ?";

	// shipping_id, order_key, order_date, shipping_address, shipping_date,
	// shipping_status, payment_status, order_amt, user_id, no_of_products
	@Override
	public List<MyOrder> myOrderList(Connection connection, int userid) throws SQLException {

		try (PreparedStatement preparedstatment = connection.prepareStatement(myOrder)) {
			List<MyOrder> myOrderlist = new ArrayList<MyOrder>();
			preparedstatment.setInt(1, userid);
			ResultSet resultSet = preparedstatment.executeQuery();
			while (resultSet.next()) {
				MyOrder myOrder = new MyOrder();
				Shipping shipping = new Shipping();
				shipping.setAddress(resultSet.getString("shipping_address"));
				shipping.setAmount(resultSet.getDouble("order_amt"));
				shipping.setUserid(resultSet.getInt("user_id"));
				shipping.setOrderdate(String.valueOf(resultSet.getDate("order_date")));
				shipping.setShippingid(resultSet.getInt("shipping_id"));
				shipping.setOrderkey(resultSet.getString("order_key"));
				shipping.setPaymentstutus(resultSet.getString("payment_status"));
				shipping.setShippingstatus(resultSet.getString("shipping_status"));
				shipping.setItems(resultSet.getInt("no_of_products"));
				if (resultSet.getDate("shipping_date")==null) {
					shipping.setShippingdate("In 7-10 working days");
				} else {
					shipping.setShippingdate(String.valueOf(resultSet.getDate("shipping_date")));
				}
				myOrder.setShipping(shipping);
				myOrderlist.add(myOrder);
			}
			//System.out.println("MYORDERLISTSERVLET myorder list : " + myOrderlist.size());
			return myOrderlist;
		}

	}

	@Override
	public List<MyOrder> allOrderList(Connection connection) throws SQLException {

		try (PreparedStatement preparedstatment = connection.prepareStatement(allOrders)) {
			// System.out.println("called 3");
			List<MyOrder> allOrderlist = new ArrayList<MyOrder>();
			// shipping_id, order_key, order_date, shipping_address, shipping_date,
			// shipping_status, payment_status, order_amt, user_id, no_of_products
			ResultSet resultSet = preparedstatment.executeQuery();
			while (resultSet.next()) {
				// System.out.println("called for loop ");
				MyOrder myOrder = new MyOrder();
				Shipping shipping = new Shipping();
				shipping.setAddress(resultSet.getString("shipping_address"));
				shipping.setAmount(resultSet.getDouble("order_amt"));
				shipping.setUserid(resultSet.getInt("user_id"));
				shipping.setOrderdate(String.valueOf(resultSet.getDate("order_date")));
				shipping.setShippingid(resultSet.getInt("shipping_id"));
				shipping.setOrderkey(resultSet.getString("order_key"));
				shipping.setPaymentstutus(resultSet.getString("payment_status"));
				shipping.setShippingstatus(resultSet.getString("shipping_status"));
				shipping.setItems(resultSet.getInt("no_of_products"));
				//System.out.println("call 1");
				if (resultSet.getDate("shipping_date")==null) {
					//System.out.println("call 2");
					shipping.setShippingdate("In 7-10 working days");
				} else {
					//System.out.println("call 3");
					shipping.setShippingdate(String.valueOf(resultSet.getDate("shipping_date")));
				}
				myOrder.setShipping(shipping);
				allOrderlist.add(myOrder);
			}
			// System.out.println("MYORDERLISTSERVLET myorder list : "+allOrderlist.size());
			return allOrderlist;
		}

	}

	@Override
	public Shipping selectedShippingOrder(Connection connection, String orderkey) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedShippingOrder)) {
			preparedstatment.setString(1, orderkey);
			int cnt = 0;
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				// shipping_id, order_key, order_date, shipping_address,
				// shipping_date, shipping_status, payment_status,
				// order_amt, user_id, no_of_products
				Shipping shipping = new Shipping();
				while (resultSet.next()) {
					++cnt;
					if (cnt > 0) {
						shipping.setShippingid(resultSet.getInt("shipping_id"));
						shipping.setOrderkey(resultSet.getString("order_key"));
						shipping.setUserid(resultSet.getInt("user_id"));
						shipping.setAddress(resultSet.getString("shipping_address"));
						shipping.setOrderdate(String.valueOf(resultSet.getDate("order_date")));
						if (resultSet.getDate("shipping_date")==null) {
							shipping.setShippingdate("In 7-10 working days");
						} else {
							shipping.setShippingdate(String.valueOf(resultSet.getDate("shipping_date")));
						}
						shipping.setPaymentstutus(resultSet.getString("payment_status"));
						shipping.setShippingstatus(resultSet.getString("shipping_status"));
						shipping.setAmount(resultSet.getDouble("order_amt"));
						shipping.setItems(resultSet.getInt("no_of_products"));
						return shipping;
					} else {
						return null;
					}
				}

			}
		}
		return null;
	}

	@Override
	public Order selectedOrder(Connection connection, String orderkey) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedOrder)) {
			preparedstatment.setString(1, orderkey);
			ResultSet resultSet = preparedstatment.executeQuery();
			while (resultSet.next()) {
				Order order = new Order();
				order.setOrderid(resultSet.getInt("order_id"));
				order.setOrderkey(resultSet.getString("order_key"));
				order.setTransaction_amount(resultSet.getDouble("order_amount"));
				order.setUserid(resultSet.getInt("user_id"));
				order.setDatetime(String.valueOf(resultSet.getTimestamp("order_date")));
				return order;
			}
			// order_id, order_key, user_id, order_amount, order_date
		}

		return null;

	}

	@Override
	public List<OrderDetails> selectedOrderDetails(Connection connection, int orderid) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedOrderDetails)) {
			// orderdetails_id, order_id, product_id, product_price,
			// product_quantity, product_id, product_image, product_name
			preparedstatment.setInt(1, orderid);
			List<OrderDetails> orderdetailslist = new ArrayList<OrderDetails>();
			ResultSet resultSet = preparedstatment.executeQuery();
			while (resultSet.next()) {
				OrderDetails orderDetails = new OrderDetails();
				orderDetails.setOrderdetailsid(resultSet.getInt("orderdetails_id"));
				orderDetails.setOrderid(resultSet.getInt("order_id"));
				orderDetails.setProductid(resultSet.getInt("product_id"));
				orderDetails.setProductprice(resultSet.getInt("product_price"));
				orderDetails.setProductquantity(resultSet.getInt("product_quantity"));
				if (resultSet.getString("product_image") != null) {
					orderDetails
							.setImagestring(Base64.getEncoder().encodeToString(resultSet.getBytes("product_image")));
				}
				orderDetails.setProductname(resultSet.getString("product_name"));
				orderdetailslist.add(orderDetails);
			}
			return orderdetailslist;
		}

	}

	@Override
	public Payment selectedOrderPayment(Connection connection, String orderkey) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedOrderPayment)) {
			preparedstatment.setString(1, orderkey);
			ResultSet resultSet = preparedstatment.executeQuery();
			while (resultSet.next()) {
				// bank_name, bank_txn_id, currency, gateway_name, merchant_id, order_id,
				// payment_mode, response_code, response_message, status, txn_amount, txn_date,
				// txn_id
				Payment payment = new Payment();
				if (resultSet.getString("payment_mode").equals("COD")) {
					payment.setCurrency(resultSet.getString("currency"));
					payment.setOrderid(resultSet.getString("order_id"));
					payment.setPaymentmode(resultSet.getString("payment_mode"));
					payment.setStatus(resultSet.getString("status"));
					payment.setTxnamount(Double.parseDouble(resultSet.getString("txn_amount")));
					// payment.setTxndatetime(String.valueOf(resultSet.getDate("txn_date")));
					payment.setTxnid(resultSet.getString("txn_id"));
					return payment;

				} else {
					payment.setBankname(resultSet.getString("bank_name"));
					payment.setBanktxnid(resultSet.getString("bank_txn_id"));
					payment.setCurrency(resultSet.getString("currency"));
					payment.setGatewayname(resultSet.getString("gateway_name"));
					payment.setMerchantid(resultSet.getString("merchant_id"));
					payment.setOrderid(resultSet.getString("order_id"));
					payment.setPaymentmode(resultSet.getString("payment_mode"));
					payment.setResponsecode(resultSet.getString("response_code"));
					payment.setResponsemsg(resultSet.getString("response_message"));
					payment.setStatus(resultSet.getString("status"));
					payment.setTxnamount(Double.parseDouble(resultSet.getString("txn_amount")));
					 payment.setTxndatetime(String.valueOf(resultSet.getTimestamp("txn_date")));
					payment.setTxnid(resultSet.getString("txn_id"));
					return payment;
				}

			}
		}

		return null;
	}

	@Override
	public MyOrder selectedMyOrder(Connection connection, String orderkey) throws SQLException {
		MyOrder myOrder = new MyOrder();
		//System.out.println("step 3");
		myOrder.setShipping(selectedShippingOrder(connection, orderkey));
		//System.out.println("step 4");
		myOrder.setOrder(selectedOrder(connection, orderkey));
		//System.out.println("step 5");
		myOrder.setOrderdetailslist(selectedOrderDetails(connection, myOrder.getOrder().getOrderid()));
		//System.out.println("step 6");
		if (myOrder.getShipping().getPaymentstutus().equals("completed")) {
			//System.out.println("step 6.1");
			myOrder.setPayment(selectedOrderPayment(connection, orderkey));
			//System.out.println("step 6.2");
		}
		// System.out.println("step 3");
		//System.out.println("step 7");
		return myOrder;

	}

}
