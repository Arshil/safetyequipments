package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Complaint;
import com.safetyequipment.dao.ComplaintDao;

public class ComplaintDaoImpl implements ComplaintDao {

	String addComplaint = "insert into complaint_table(complaint_name,category_id, subcategory_id, product_id,company_id)values(?,?,?,?,?)";
	String deleteComplaint = "update complaint_table set active_status = ? where complaint_id = ?";
	String allComplaint = "select co.complaint_id, co.complaint_name,co.category_id, co.subcategory_id, co.product_id, co.company_id, co.active_status, ca.category_id, ca.category_name, s.sub_category_id, s.sub_category_name,p.product_id, p.product_name,c.company_id ,c.company_name from complaint_table co, category_table ca, sub_category_table s, product_table p ,company_table c where co.category_id = ca.category_id and co.subcategory_id = s.sub_category_id  and co.product_id = p.product_id and co.company_id = c.company_id and co.active_status = ?";
	String updateComplaint = "update complaint_table set complaint_name = ? , category_id = ?, subcategory_id = ?, product_id = ? , company_id = ? where complaint_id = ?";
	String selectedComplaint = "select co.complaint_id, co.complaint_name,  co.category_id, co.subcategory_id, co.product_id, co.company_id, co.active_status, ca.category_id, ca.category_name, s.sub_category_id, s.sub_category_name,p.product_id, p.product_name,c.company_id ,c.company_name from complaint_table co, category_table ca, sub_category_table s, product_table p ,company_table c where co.category_id = ca.category_id and co.subcategory_id = s.sub_category_id  and co.product_id = p.product_id and co.company_id = c.company_id and co.complaint_id = ?";;
	String selectedProductComplaintList = "select * from complaint_table where product_id = ? and active_status = ?";
	String registerUserComplaint = "insert into user_complaint_table(complaint_id, user_id, order_key,product_id,complaint_description)values(?,?,?,?,?)";
	String userAllComplaint = "select c.complaint_name,c.complaint_id,p.product_id,p.product_name , u.usercomplaint_id, u.complaint_id, u.user_id, u.order_key, u.complaint_description, u.product_id, u.active_status from complaint_table c,product_table p , user_complaint_table u where u.complaint_id = c.complaint_id and c.product_id = p.product_id  and u.user_id = ?";
	String selectedUserComplaint = "select u.usercomplaint_id , u.complaint_id, u.user_id, u.order_key, u.complaint_description, u.product_id, u.active_status ,p.product_id ,p.product_name from  user_complaint_table u,product_table p where u.product_id = p.product_id and  u.usercomplaint_id = ?";
	String selectedProductComplaint = "select complaint_id,complaint_name,product_id from complaint_table where product_id = ?";
	String editUserComplaint = "update user_complaint_table set complaint_id = ? , complaint_description = ? where usercomplaint_id = ?";
	String allUserComplaints = "select c.complaint_id,c.complaint_name,p.product_id,p.product_name , u.usercomplaint_id, u.complaint_id, u.user_id, u.order_key, u.complaint_description, u.product_id, u.active_status from complaint_table c,product_table p , user_complaint_table u where p.product_id = u.product_id and u.complaint_id = c.complaint_id";
	String editSelectedUserComplaint = "select c.complaint_id,c.complaint_name,u.active_status,u.complaint_description,u.complaint_id,u.order_key,u.product_id,u.user_id,u.usercomplaint_id ,p.product_id,p.product_name from complaint_table c,user_complaint_table u , product_table p where u.product_id = p.product_id and u.complaint_id = c.complaint_id and u.usercomplaint_id = ?";
	String editComplaintStatus = "update user_complaint_table set active_status = ? where usercomplaint_id = ?";
	// complaint_id, complaint_name, complaint_description, category_id,
	// subcategory_id, product_id, company_id, active_status
	@Override
	public int addComplaint(Connection connection, Complaint complaint) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(addComplaint)) {
			preparedstatment.setString(1, complaint.getName());
			preparedstatment.setInt(2, complaint.getCategoryid());
			preparedstatment.setInt(3, complaint.getSubcategoryid());
			preparedstatment.setInt(4, complaint.getProductid());
			preparedstatment.setInt(5, complaint.getCompanyid());
			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public int deleteComplaint(Connection connection, int complaintId) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deleteComplaint)) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, complaintId);
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public List<Complaint> allComplaintList(Connection connection) throws SQLException {

		// System.out.println("all calleld");
		try (PreparedStatement preparedstatment = connection.prepareStatement(allComplaint)) {
			preparedstatment.setInt(1, 1);
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				List<Complaint> complaintlist = new ArrayList<Complaint>();
				while (resultSet.next()) {
					Complaint complaint = new Complaint();
					// output
					// complaint_id, complaint_name, complaint_description,
					// category_id, subcategory_id, product_id, company_id,
					// active_status, category_id, category_name, sub_category_id,
					// sub_category_name, product_id, product_name, company_id,
					// company_name
					complaint.setComplaintid(resultSet.getInt("complaint_id"));
					// System.out.println("complaint id :"+complaint.getComplaintid());
					complaint.setName(resultSet.getString("complaint_name"));
					// complaint.setDescription(resultSet.getString("complaint_description"));
					complaint.setCategoryid(resultSet.getInt("category_id"));
					complaint.setSubcategoryid(resultSet.getInt("subcategory_id"));
					complaint.setProductid(resultSet.getInt("product_id"));
					complaint.setCompanyid(resultSet.getInt("company_id"));
					complaint.setCategoryname(resultSet.getString("category_name"));
					complaint.setSubcategoryname(resultSet.getString("sub_category_name"));
					complaint.setProductname(resultSet.getString("product_name"));
					complaint.setCompanyname(resultSet.getString("company_name"));
					complaintlist.add(complaint);
				}
				return complaintlist;
			}
		}

	}

	@Override
	public int updateComplaint(Connection connection, Complaint complaint) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(updateComplaint)) {
			preparedStatement.setString(1, complaint.getName());
			preparedStatement.setInt(2, complaint.getCategoryid());
			preparedStatement.setInt(3, complaint.getSubcategoryid());
			preparedStatement.setInt(4, complaint.getProductid());
			preparedStatement.setInt(5, complaint.getCompanyid());
			preparedStatement.setInt(6, complaint.getComplaintid());
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public Complaint selectedComplaint(Connection connection, int complaintid) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(selectedComplaint)) {
			preparedStatement.setInt(1, complaintid);
			Complaint complaint = new Complaint();
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				complaint.setComplaintid(resultSet.getInt("complaint_id"));
				complaint.setName(resultSet.getString("complaint_name"));
				// complaint.setDescription(resultSet.getString("complaint_description"));
				complaint.setCategoryid(resultSet.getInt("category_id"));
				complaint.setSubcategoryid(resultSet.getInt("subcategory_id"));
				complaint.setProductid(resultSet.getInt("product_id"));
				complaint.setCompanyid(resultSet.getInt("company_id"));
				complaint.setCategoryname(resultSet.getString("category_name"));
				complaint.setSubcategoryname(resultSet.getString("sub_category_name"));
				complaint.setProductname(resultSet.getString("product_name"));
				complaint.setCompanyname(resultSet.getString("company_name"));
			}
			return complaint;
		}
	}

	@Override
	public List<Complaint> selectedProductComplaintList(Connection connection, int productid) throws SQLException {
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectedProductComplaintList)) {
			preparedstatment.setInt(1, productid);
			preparedstatment.setInt(2, 1);
			try (ResultSet resultSet = preparedstatment.executeQuery()) {
				List<Complaint> complaintlist = new ArrayList<Complaint>();
				while (resultSet.next()) {
					Complaint complaint = new Complaint();
					complaint.setComplaintid(resultSet.getInt("complaint_id"));
					complaint.setName(resultSet.getString("complaint_name"));
					complaint.setProductid(resultSet.getInt("product_id"));
					complaintlist.add(complaint);
				}
				return complaintlist;
			}
		}

	}
	@Override
	public int registerUserComplaint(Connection connection, Complaint complaint) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(registerUserComplaint,Statement.RETURN_GENERATED_KEYS)) {
			preparedstatment.setInt(1, complaint.getComplaintid());
			preparedstatment.setInt(2, complaint.getUserid());
			preparedstatment.setString(3, complaint.getOrderkey());
			preparedstatment.setInt(4, complaint.getProductid());
			preparedstatment.setString(5, complaint.getDescription());
			preparedstatment.executeUpdate();
			ResultSet resultSet = preparedstatment.getGeneratedKeys();
			 if(resultSet.next())
			 {
				return resultSet.getInt(1);
			 }
			
		}
		
		
		return 0;
	}

	@Override
	public List<Complaint> userAllComplaints(Connection connection,int userid) throws SQLException {
	try(PreparedStatement preparedStatement = connection.prepareStatement(userAllComplaint)){
		preparedStatement.setInt(1,userid);
		try(ResultSet resultSet = preparedStatement.executeQuery())
		{
			List<Complaint> complaintlist = new ArrayList<Complaint>();
			while (resultSet.next()) {
				Complaint complaint = new Complaint();
				//complaint_name, complaint_id, product_id, product_name, 
				//usercomplaint_id, complaint_id, user_id, order_key, 
				//complaint_description, product_id, active_status
				complaint.setUsercomplaintid(resultSet.getInt("usercomplaint_id"));
				complaint.setComplaintid(resultSet.getInt("complaint_id"));
				complaint.setName(resultSet.getString("complaint_name"));
				complaint.setProductid(resultSet.getInt("product_id"));
				complaint.setProductname(resultSet.getString("product_name"));
				complaint.setUserid(resultSet.getInt("user_id"));
				complaint.setOrderkey(resultSet.getString("order_key"));
				complaint.setActivestatus(resultSet.getString("active_status"));
				if(resultSet.getString("complaint_name").equalsIgnoreCase("Others"))
				{
					complaint.setDescription(resultSet.getString("complaint_description"));
				}
				else
				{
					complaint.setDescription("Not given");
				}
				complaintlist.add(complaint);
			}
			return complaintlist;
		}
		
	}
}
	
	@Override
	public Complaint selectedUserComplaint(Connection connection, int usercomplaintid) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(selectedUserComplaint)) {
			preparedStatement.setInt(1, usercomplaintid);
			Complaint complaint = new Complaint();
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				complaint.setComplaintid(resultSet.getInt("complaint_id"));
				complaint.setUsercomplaintid(resultSet.getInt("usercomplaint_id"));
				if(resultSet.getString("complaint_name").equalsIgnoreCase("Others"))
				{
					complaint.setDescription(resultSet.getString("complaint_description"));
				}
				else
				{
					complaint.setDescription("Not given");
				}
				complaint.setProductname(resultSet.getString("product_name"));
				complaint.setProductid(resultSet.getInt("product_id"));
				complaint.setOrderkey(resultSet.getString("order_key"));
				complaint.setUserid(resultSet.getInt("user_id"));
				complaint.setActivestatus(resultSet.getString("active_status"));
			}
			return complaint;
		}
	}
	
	
	@Override
	public List<Complaint> complaintListSelectedProduct(Connection connection, int productid) throws SQLException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(selectedProductComplaint)){
			preparedStatement.setInt(1,productid);
			try(ResultSet resultSet = preparedStatement.executeQuery())
			{
				List<Complaint> complaintlist = new ArrayList<Complaint>();
				while (resultSet.next()) {
					Complaint complaint = new Complaint();					
					complaint.setComplaintid(resultSet.getInt("complaint_id"));
					complaint.setName(resultSet.getString("complaint_name"));
					complaint.setProductid(resultSet.getInt("product_id"));
					complaintlist.add(complaint);
				}
				return complaintlist;
			}
			
		}
	}
	
	@Override
	public int editUserComplaint(Connection connection, Complaint complaint) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(editUserComplaint)) {
			//preparedStatement.setString(1, complaint.getName());
			preparedStatement.setInt(1, complaint.getComplaintid());
			preparedStatement.setString(2, complaint.getDescription());
			preparedStatement.setInt(3, complaint.getUsercomplaintid());
			return preparedStatement.executeUpdate();
		}
	}
	@Override
	public List<Complaint> allUserComplaints(Connection connection) throws SQLException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(allUserComplaints)){
			//preparedStatement.setInt(1,userid);
			try(ResultSet resultSet = preparedStatement.executeQuery())
			{
				List<Complaint> complaintlist = new ArrayList<Complaint>();
				while (resultSet.next()) {
					Complaint complaint = new Complaint();
					//complaint_id, complaint_name, product_id, product_name,
					//usercomplaint_id, complaint_id, user_id, order_key, 
					//complaint_description, product_id, active_status
					complaint.setUsercomplaintid(resultSet.getInt("usercomplaint_id"));
					complaint.setComplaintid(resultSet.getInt("complaint_id"));
					complaint.setName(resultSet.getString("complaint_name"));
					complaint.setProductid(resultSet.getInt("product_id"));
					complaint.setProductname(resultSet.getString("product_name"));
					complaint.setUserid(resultSet.getInt("user_id"));
					complaint.setOrderkey(resultSet.getString("order_key"));
					complaint.setActivestatus(resultSet.getString("active_status"));
					if(resultSet.getString("complaint_name").equalsIgnoreCase("Others"))
					{
						complaint.setDescription(resultSet.getString("complaint_description"));
					}
					else
					{
						complaint.setDescription("Not given");
					}
					complaintlist.add(complaint);
				}
				return complaintlist;
			}
			
		}
	}
	@Override
	public Complaint editSelectedUserComplaint(Connection connection,int usercomplaintid) throws SQLException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(editSelectedUserComplaint)){
			preparedStatement.setInt(1,usercomplaintid);
			try(ResultSet resultSet = preparedStatement.executeQuery())
			{
				Complaint complaint = new Complaint();
				while (resultSet.next()) {
					//complaint_id, complaint_name, active_status,
					//complaint_description, complaint_id, order_key, product_id,
					//user_id, usercomplaint_id, product_id, product_name
					complaint.setUsercomplaintid(resultSet.getInt("usercomplaint_id"));
					complaint.setComplaintid(resultSet.getInt("complaint_id"));
					complaint.setName(resultSet.getString("complaint_name"));
					complaint.setProductid(resultSet.getInt("product_id"));
					complaint.setProductname(resultSet.getString("product_name"));
					complaint.setUserid(resultSet.getInt("user_id"));
					complaint.setOrderkey(resultSet.getString("order_key"));
					complaint.setActivestatus(resultSet.getString("active_status"));
					if(resultSet.getString("complaint_name").equalsIgnoreCase("Others"))
					{
						complaint.setDescription(resultSet.getString("complaint_description"));
					}
					else
					{
						complaint.setDescription("Not given");
					}
				}
				return complaint;
			}
		}
	}
	@Override
	public int editComplaintStatus(Connection connection,Complaint complaint) throws SQLException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(editComplaintStatus)){
			preparedStatement.setString(1,complaint.getActivestatus());
			preparedStatement.setInt(2,complaint.getUsercomplaintid());
			return preparedStatement.executeUpdate();
		}
	}
	
}
