package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Company;
import com.safetyequipment.dao.CompanyDao;

public class CompanyDaoImpl implements CompanyDao {

	String addcompanydetailsquery = "insert into company_table(company_name, company_email, company_contact, company_address) values (?,?,?,?)";
	String selectallactivecompanyquery = "select * from company_table where company_isactive = 1";
	String deletecompanyquery = "update company_table set company_isactive = ? where company_id = ?";
	String updatecompanyquery = "update company_table set company_name=?, company_email=?, company_contact = ?,company_address=?  where company_id=?";
	String selectcompanyquery = "select * from company_table where company_id = ?";
	String getCompanyListSelectedProductquery = "select c.company_id, c.company_name, c.company_email, c.company_contact, c.company_address, c.company_isactive,p.product_id, p.product_name, p.category_id, p.product_price, p.product_image, p.company_id, p.subcategory_id, p.product_description, p.product_isactive, p.product_quantity from product_table p,company_table c where  p.company_id = c.company_id and p.product_id = ?";

	@Override
	public int addcompanydetails(Connection connection, Company company) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(addcompanydetailsquery)) {
			preparedstatment.setString(1, company.getCompanyname());
			preparedstatment.setString(2, company.getCompanyemail());
			preparedstatment.setString(3, company.getCompanycontact());
			preparedstatment.setString(4, company.getCompanyaddress());

			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public ArrayList<Company> getcompanydetails(Connection connection) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectallactivecompanyquery);
				ResultSet resultSet = preparedstatment.executeQuery()) {

			ArrayList<Company> companylist = new ArrayList<>();
			while (resultSet.next()) {
				Company company = new Company();

				company.setCompanyname(resultSet.getString("company_name"));
				company.setCompanycontact(resultSet.getString("company_contact"));
				company.setCompanyaddress(resultSet.getString("company_address"));
				company.setCompanyemail(resultSet.getString("company_email"));
				company.setCompanyid(resultSet.getInt("company_id"));

				companylist.add(company);
			}
			return companylist;
		}
	}

	@Override
	public int deletecompanyDetails(Connection connection, int companyid) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(deletecompanyquery)) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, companyid);
			return preparedStatement.executeUpdate();

		}
	}

	@Override
	public Company selectcompany(Connection connection, int companyid) throws SQLException {
		// TODO Auto-generated method stub
		Company company = new Company();
		try (PreparedStatement stmt = connection.prepareStatement(selectcompanyquery);) {

			stmt.setInt(1, companyid);
			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					company.setCompanyname(rs.getString("company_name"));
					company.setCompanycontact(rs.getString("company_contact"));
					company.setCompanyemail(rs.getString("company_email"));
					company.setCompanyaddress(rs.getString("company_address"));
					company.setCompanyid(rs.getInt("company_id"));

				}
				return company;
			}
		}

	}

	@Override
	public int modifycompany(Connection connection, Company company) throws SQLException {
		// TODO Auto-generated method stub
		int cnt = 0;
		try (PreparedStatement preparedStatement = connection.prepareStatement(updatecompanyquery)) {
			System.out.println("dao called");
			preparedStatement.setString(++cnt, company.getCompanyname());
			preparedStatement.setString(++cnt, company.getCompanyemail());
			preparedStatement.setString(++cnt, company.getCompanycontact());
			preparedStatement.setString(++cnt, company.getCompanyaddress());
			preparedStatement.setInt(++cnt, company.getCompanyid());
			return preparedStatement.executeUpdate();
		}

	}
	@Override
	public List<Company> getCompanyListOfSelectedProduct(Connection connection, int productid) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(getCompanyListSelectedProductquery)) {
			// System.out.println("dao called");
			preparedStatement.setInt(1, productid);
			List<Company> companylist = new ArrayList<Company>();
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				while (resultSet.next()) {
					Company company = new Company();
					company.setCompanyname(resultSet.getString("company_name"));
					company.setCompanycontact(resultSet.getString("company_contact"));
					company.setCompanyaddress(resultSet.getString("company_address"));
					company.setCompanyemail(resultSet.getString("company_email"));
					company.setCompanyid(resultSet.getInt("company_id"));

					companylist.add(company);
				}
				return companylist;
			}
		}

	}

}
