package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.dao.*;
import java.util.Base64;
import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.SubCategory;

public class CategoryDaoimpl implements CategoryDao {
	String addcategorydetailsquery = "insert into category_table(category_name, category_image, category_description) values (?,?,?)";
	String deletecategory = "update category_table set category_status= ? where category_id = ?";
	String selectcategory = "select * from category_table where category_id = ? and category_status = 1";
	String updatecategoryquery = "update category_table set category_name=?, category_description=?, category_image = coalesce(?,category_image) where category_id=?";
	String deletesubcategoryquery = "update sub_category_table set sub_category_status = ? where category_id = ?";
	String deleteproductquery = "update product_table set product_isactive = ? where category_id = ? ";

	@Override
	public int addcategorydetails(Connection connection, Category category) throws SQLException {
		// TODO Auto-generated method st
		try (PreparedStatement preparedstatment = connection.prepareStatement(addcategorydetailsquery)) {
			preparedstatment.setString(1, category.getCategoryname());
			preparedstatment.setBlob(2, category.getCategoryimage());
			preparedstatment.setString(3, category.getDescription());
			return preparedstatment.executeUpdate();
		}
	}

	@Override
	public int deletecategoryDetails(Connection connection, int categoryId) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deletecategory)) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, categoryId);
			int deletedCategory = preparedStatement.executeUpdate();
			deletesubcategory(connection, categoryId);
			deleteproduct(connection, categoryId);
			return deletedCategory;

		}
	}
	@Override
	public void deletesubcategory(Connection connection, int categoryId) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deletesubcategoryquery)) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, categoryId);
			preparedStatement.executeUpdate();
			// deletesubcategory(connection,categoryId);

		}
	}
	@Override
	public void deleteproduct(Connection connection, int categoryId) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deleteproductquery)) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, categoryId);
			preparedStatement.executeUpdate();
			// deletesubcategory(connection,categoryId);

		}
	}

	@Override
	public List<Category> selectCategory(Connection connection) throws SQLException {

		System.out.println("all calleld");
		try (PreparedStatement preparedstatment = connection.prepareStatement(selectcategory)) {
			try (ResultSet rest = preparedstatment.executeQuery()) {
				List<Category> categorylist = new ArrayList<Category>();
				while (rest.next()) {
					Category category = new Category();
					category.setCategoryid(rest.getInt("category_id"));
					category.setCategoryname(rest.getString("category_name"));
					category.setDescription(rest.getString("category_description"));
					if (rest.getString("category_image") != null) {
						category.setCategoryImageString(
								Base64.getEncoder().encodeToString(rest.getBytes("category_image")));
					}
					categorylist.add(category);
				}
				return categorylist;
			}
		}

	}

	@Override
	public Category selectcategory(Connection connection, int category_id) throws SQLException {
		Category category = new Category();
		try (PreparedStatement stmt = connection.prepareStatement(selectcategory);) {

			stmt.setInt(1, category_id);
			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					category.setCategoryname(rs.getString("category_name"));
					if (rs.getBytes("category_image") != null) {
						category.setCategoryImageString(
								Base64.getEncoder().encodeToString(rs.getBytes("category_image")));

					}
					category.setDescription(rs.getString("category_description"));
					category.setCategoryid(rs.getInt("category_id"));
					System.out.println("dao cid : " + rs.getInt("category_id"));
				}
				return category;
			}
		}
	}

	@Override
	public int modifycategory(Connection connection, Category category) throws SQLException {

		int cnt = 0;
		try (PreparedStatement preparedStatement = connection.prepareStatement(updatecategoryquery)) {
			System.out.println("dao called");
			preparedStatement.setString(++cnt, category.getCategoryname());
			preparedStatement.setString(++cnt, category.getDescription());
			preparedStatement.setBlob(++cnt, category.getCategoryimage());
			preparedStatement.setInt(++cnt, category.getCategoryid());
			return preparedStatement.executeUpdate();
		}

	}
}
