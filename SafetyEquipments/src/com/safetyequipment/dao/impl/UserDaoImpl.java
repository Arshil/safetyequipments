package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.safetyequipment.beans.Contact;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.User;
import com.safetyequipment.dao.UserDao;

public class UserDaoImpl implements UserDao {

	String insertuserquery = "insert into user_table(user_email,user_password,user_firstname,user_lastname)values(?,?,?,?)";
	String selectuser = "select * from user_table where user_email = ? and user_password = ?";
	String updateuserquery = "update user_table set user_email=?, user_gender=?, user_mobile=?, user_address=?,user_profileimage=coalesce(?,user_profileimage), user_firstname=?, user_lastname=?, user_pincode=? where user_id=?";
	String alluserlistquery = " select * from user_table ";
	String deleteUserquery = "update user_table set user_isactive = ? where user_id = ?";
	String selectedUser = " select * from user_table where user_id = ?";
	String editUserquery = "update user_table set user_firstname = ?, user_lastname = ?, user_mobile = ?, user_address = ? , user_pincode = ? , user_profileimage=coalesce(?,user_profileimage) where user_id = ? ";
	String selecteduserPasswordquery = "select user_password from user_table where user_id = ?";
	String userPasswordChangequery = "update user_table set user_password = ? where user_id =?";
	String forgotpasswordquery = " select user_password from user_table where user_email = ?";
	String validateemailquery = "select user_email from user_table where user_email = ? and user_isactive = 1 ";
	String validateuserlogin = "select * from  user_table where user_email = ?  and user_password = ?";
	String makeinquiry = "insert into contactus_table(firstname, lastname, email, inqyuiry_description, subject,inquiry_date)values(?,?,?,?,?,now())";
	String viewAllInquiries = "select * from contactus_table";
	String activeUserquery = "update user_table set user_isactive = ? where user_id = ?";
	String updateUserMobile = "update user_table set user_mobile = ? where user_id = ?";
	String updateUserAddress = "update user_table set user_address = ? where user_id = ?";
	String updateUserPincode = "update user_table set user_pincode = ? where user_id = ?";
	// user_id, user_email, user_gender, user_mobile, user_dob, user_address
	// , user_password, user_type,
	// user_profileimage, user_firstname, user_lastname

	// String selectuserlistquery = "select * from user_table";
	@Override
	public int userregistration(Connection connection, User user) throws SQLException {
		// String insertuserquery = "insert into
		// user_table(user_email,user_password,user_firstname,user_lastname) values
		// (?,?,?,?)";
		if (connection != null) {

			try (PreparedStatement preparedstatment = connection.prepareStatement(insertuserquery);)

			{
				preparedstatment.setString(1, user.getEmailid());
				System.out.println("email in dao :" + user.getEmailid());
				preparedstatment.setString(2, user.getPassword());
				preparedstatment.setString(3, user.getFirstname());
				preparedstatment.setString(4, user.getLastname());
				return preparedstatment.executeUpdate();
			} finally {
				System.out.println("email in after dao :" + user.getEmailid());
			}
		} else {

		}
		return 0;
	}
	@Override
	public User validateuser(Connection connection, String email, String password) throws SQLException {
		// TODO Auto-generated method stub overload
		if (connection != null) {
			try (PreparedStatement preparedstatment = connection.prepareStatement(selectuser);) {
				preparedstatment.setString(1, email);
				preparedstatment.setString(2, password);
				try (ResultSet resultSet = preparedstatment.executeQuery();) {
					while (resultSet.next()) {
						User user = new User();
						user.setUserid(resultSet.getInt("user_id"));
						System.out.println("inside dao user id :" + resultSet.getInt("user_id"));
						user.setEmailid(resultSet.getString("user_email"));
						user.setFirstname(resultSet.getString("user_firstname"));
						user.setLastname(resultSet.getString("user_lastname"));
						user.setGender(resultSet.getString("user_gender"));
						user.setAddress(resultSet.getString("user_address"));
						user.setMobileno(resultSet.getString("user_mobile"));
						user.setPincode(resultSet.getString("user_pincode"));
						user.setPassword(resultSet.getString("user_password"));
						user.setUsertype(resultSet.getString("user_type"));
						if (resultSet.getString("user_profileimage") != null) {
							user.setImagestring(
									Base64.getEncoder().encodeToString(resultSet.getBytes("user_profileimage")));
						}
						return user;

					}

				}

			}

		}

		return null;
	}

	@Override
	public int updateuserdetails(Connection connection, User user) throws SQLException {
		// TODO Auto-generated method stub
		int cnt = 0;
		try (PreparedStatement preparedStatement = connection.prepareStatement(updateuserquery)) {
			System.out.println("user dao called");

			preparedStatement.setString(++cnt, user.getEmailid());
			preparedStatement.setString(++cnt, user.getGender());
			preparedStatement.setString(++cnt, user.getMobileno());
			preparedStatement.setString(++cnt, user.getAddress());
			preparedStatement.setBlob(++cnt, user.getUserimage());
			preparedStatement.setString(++cnt, user.getFirstname());
			preparedStatement.setString(++cnt, user.getLastname());
			preparedStatement.setString(++cnt, user.getPincode());
			preparedStatement.setInt(++cnt, user.getUserid());

			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public List<User> alluserlist(Connection connection) throws SQLException {
		List<User> userlist = new ArrayList<User>();
		try (PreparedStatement preparedStatement = connection.prepareStatement(alluserlistquery)) {
			try (ResultSet rest = preparedStatement.executeQuery()) {
				while (rest.next()) {
					User user = new User();
					// user_id, user_email, user_gender, user_mobile, user_dob,
					// user_address, user_password, user_type, user_profileimage,
					// user_firstname, user_lastname, user_isactive
					user.setUserid(rest.getInt("user_id"));
					user.setFirstname(rest.getString("user_firstname"));
					user.setLastname(rest.getString("user_lastname"));
					user.setEmailid(rest.getString("user_email"));
					user.setMobileno(rest.getString("user_mobile"));
					user.setAddress(rest.getString("user_address"));
					user.setGender(rest.getString("user_gender"));
					user.setBirthdate(rest.getString("user_dob"));
					user.setPincode(rest.getString("user_pincode"));
					user.setActivestatus(rest.getInt("user_isactive"));
					if (rest.getString("user_profileimage") != null) {
						user.setImagestring(Base64.getEncoder().encodeToString(rest.getBytes("user_profileimage")));
					}
					userlist.add(user);
				}
				return userlist;
			}
		}
	}

	@Override
	public int deleteUser(Connection connection, int userid) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(deleteUserquery);) {
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, userid);
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public User selectedUser(Connection connection, int userid) throws SQLException {

		try (PreparedStatement preparedStatement = connection.prepareStatement(selectedUser)) {
			preparedStatement.setInt(1, userid);
			try (ResultSet rest = preparedStatement.executeQuery();) {
				while (rest.next()) {
					User user = new User();
					// user_id, user_email, user_gender, user_mobile,
					// user_dob, user_address, user_password, user_type,
					// user_profileimage, user_firstname, user_lastname, user_isactive
					user.setUserid(rest.getInt("user_id"));
					user.setFirstname(rest.getString("user_firstname"));
					user.setLastname(rest.getString("user_lastname"));
					user.setEmailid(rest.getString("user_email"));
					user.setGender(rest.getString("user_gender"));
					user.setAddress(rest.getString("user_address"));
					user.setMobileno(rest.getString("user_mobile"));
					user.setPincode(rest.getString("user_pincode"));
					user.setBirthdate(rest.getString("user_dob"));
					user.setUsertype(rest.getString("user_type"));
					if (rest.getString("user_profileimage") != null) {
						user.setImagestring(Base64.getEncoder().encodeToString(rest.getBytes("user_profileimage")));
					}
					return user;
				}
			}
		}
		return null;

	}

	@Override
	public int editUser(Connection connection, User user) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(editUserquery);) {
			// String editUserquery = "update user_table set user_firstname = ?,
			// user_lastname = ?, user_mobile = ?, user_address = ? ,
			// user_pincode = ? where user_id = ? ";
			preparedStatement.setString(1, user.getFirstname());
			preparedStatement.setString(2, user.getLastname());
			preparedStatement.setString(3, user.getMobileno());
			preparedStatement.setString(4, user.getAddress());
			preparedStatement.setString(5, user.getPincode());
			preparedStatement.setBlob(6, user.getUserimage());
			preparedStatement.setInt(7, user.getUserid());
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public String selectedUserPassword(Connection connection, int userid) throws SQLException {
		String message = " ";
		try (PreparedStatement preparedStatement = connection.prepareStatement(selecteduserPasswordquery)) {
			preparedStatement.setInt(1, userid);
			System.out.println(userid);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					System.out.println(resultSet.getString("user_password"));
					message = resultSet.getString("user_password");
				}
				return message;
			}
		}
	}

	@Override
	public int userPasswordChange(Connection connection, int userid, String password) throws SQLException {

		try (PreparedStatement preparedStatement = connection.prepareStatement(userPasswordChangequery)) {
			preparedStatement.setString(1, password);
			preparedStatement.setInt(2, userid);
			return preparedStatement.executeUpdate();
		}

	}

	@Override
	public String selectedUserForgotPassword(Connection connection, String emailid) throws SQLException {
		String message = "Not Found";
		try (PreparedStatement preparedStatement = connection.prepareStatement(forgotpasswordquery)) {
			preparedStatement.setString(1, emailid);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					System.out.println(resultSet.getString("user_password"));
					message = resultSet.getString("user_password");
				}
				return message;
			}
		}
	}

	@Override
	public int validateEmail(Connection connection, String userEmail) throws SQLException {
		// TODO Auto-generated method stub
		int count = 0;
		String message = "Not Found";
		try (PreparedStatement preparedStatement = connection.prepareStatement(validateemailquery)) {
			preparedStatement.setString(1, userEmail);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					count++;
				}
				return count;
			}
		}
	}

	@Override
	public int validateUserRegistrationEmail(Connection connection, String userEmail) throws SQLException {
		// TODO Auto-generated method stub
		int count = 0;
		// String message = "Not Found";
		try (PreparedStatement preparedStatement = connection.prepareStatement(validateemailquery)) {
			preparedStatement.setString(1, userEmail);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					count++;
				}
				return count;
			}
		}
	}

	@Override
	public int validateUserLogin(Connection connection, String emailid, String password) throws SQLException {
		// TODO Auto-generated method stub
		int count = 0;
		try (PreparedStatement preparedStatement = connection.prepareStatement(validateuserlogin)) {
			preparedStatement.setString(1, emailid);
			preparedStatement.setString(2, password);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					count++;
				}
				return count;
			}
		}
	}

	@Override
	public int makeInquiry(Connection connection, Contact contact) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(makeinquiry)) {
			preparedStatement.setString(1, contact.getFname());
			preparedStatement.setString(2, contact.getLname());
			preparedStatement.setString(3, contact.getEmail());
			preparedStatement.setString(4, contact.getInquirydesc());
			preparedStatement.setString(5, contact.getSubject());
			return preparedStatement.executeUpdate();
		}
	}

	@Override
	public List<Contact> allInquiriesList(Connection connection) throws SQLException {
		List<Contact> inquirieslist = new ArrayList<Contact>();
		try (PreparedStatement preparedStatement = connection.prepareStatement(viewAllInquiries)) {
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Contact contact = new Contact();
					// contact_id, firstname, lastname, email,
					// inqyuiry_description, subject, inquiry_date
					contact.setContactid(resultSet.getInt("contact_id"));
					contact.setFname(resultSet.getString("firstname"));
					contact.setLname(resultSet.getString("lastname"));
					contact.setEmail(resultSet.getString("email"));
					contact.setInquirydesc(resultSet.getString("inqyuiry_description"));
					contact.setSubject(resultSet.getString("subject"));
					// contact.set(resultSet.getString("inqyuiry_description"));
					contact.setDate(String.valueOf(resultSet.getDate("inquiry_date")));
					inquirieslist.add(contact);
				}
				return inquirieslist;
			}
		}
	}
	
	
	@Override
	public int activeUser(Connection connection, int userid) throws SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(activeUserquery);) {
			preparedStatement.setInt(1, 1);
			preparedStatement.setInt(2, userid);
			return preparedStatement.executeUpdate();
		}
	}
	
	@Override
	public int updateUserAddress(Connection connection, User user) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(updateUserAddress)) {
			System.out.println("user dao called");
			preparedStatement.setString(1, user.getAddress());
			preparedStatement.setInt(2, user.getUserid());
			return preparedStatement.executeUpdate();
		}
	}
	@Override
	public int updateUserPincode(Connection connection, User user) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(updateUserPincode)) {
			System.out.println("user dao called");
			preparedStatement.setString(1, user.getPincode());
			preparedStatement.setInt(2, user.getUserid());
			return preparedStatement.executeUpdate();
		}
	}
	@Override
	public int updateUserMobile(Connection connection, User user) throws SQLException {
		// TODO Auto-generated method stub
		try (PreparedStatement preparedStatement = connection.prepareStatement(updateUserMobile)) {
			System.out.println("user dao called");
			preparedStatement.setString(1, user.getMobileno());
			preparedStatement.setInt(2, user.getUserid());
			return preparedStatement.executeUpdate();
		}
	}
	
}
