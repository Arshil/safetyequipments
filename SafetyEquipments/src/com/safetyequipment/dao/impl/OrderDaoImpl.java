package com.safetyequipment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.dao.OrderDao;

public class OrderDaoImpl implements OrderDao {

	String insertorder="insert into order_table(order_key, user_id, order_amount, order_date)values(?,?,?,now())";
	String insertOrderDetails = "insert into orderdetails_table(order_id,product_id,product_price,product_quantity) values (?,?,?,?)";
//order_id, order_key, user_id, order_amount, order_date
	@Override
	public int registerOrder(Connection connection,Order order) throws SQLException {
		// TODO Auto-generated method stub
		//order_id, user_id, order_date, order_status, order_amount
		
		try (PreparedStatement preparedstatment = connection.prepareStatement(insertorder,Statement.RETURN_GENERATED_KEYS)) {
			preparedstatment.setString(1,order.getOrderkey());
			preparedstatment.setInt(2, order.getUserid());
			preparedstatment.setDouble(3, order.getTransaction_amount());
			
			 preparedstatment.executeUpdate();
			 
			 ResultSet resultSet = preparedstatment.getGeneratedKeys();
			 if(resultSet.next())
			 {
				return resultSet.getInt(1);
			 }
			 
		}
		return 0;
	}
	@Override
	public int[]  insertOrderDetails(Connection connection, List<OrderDetails> orderDetails) throws SQLException {
		// TODO Auto-generated method stub
		
		try (PreparedStatement preparedstatment = connection.prepareStatement(insertOrderDetails)) {
			
			for(OrderDetails details : orderDetails) {
				preparedstatment.setInt(1,details.getOrderid());
				preparedstatment.setInt(2, details.getProductid());
				preparedstatment.setInt(3, details.getProductprice());
				preparedstatment.setInt(4, details.getProductquantity());
				preparedstatment.addBatch();
				
			}
			 return preparedstatment.executeBatch();
			 
			
		}
		
	}
}
