package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.OrderDetails;

public interface OrderDao {

	int registerOrder(Connection connection, Order order) throws SQLException;

int[] insertOrderDetails(Connection connection, List<OrderDetails> orderDetails) throws SQLException;


}
