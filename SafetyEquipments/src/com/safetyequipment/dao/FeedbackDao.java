package com.safetyequipment.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Feedback;

public interface FeedbackDao {

	public int addfeedback(Connection connection, Feedback feedback) throws SQLException;

	public List<Feedback> allFeedbackList(Connection connection) throws SQLException;

}
