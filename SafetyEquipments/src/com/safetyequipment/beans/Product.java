package com.safetyequipment.beans;

import java.io.InputStream;

public class Product {
	//product_id, product_name, fk_category_id, product_price, product_image, fk_company_id, fk_subcategory_id, product_description, product_isactive
	private int productid;
	private int subcategoryid;
	private String productname;
	private String categoryname;
	private String subcategoryname;
	private int categoryid;
	private int companyid;
	private String companyname;
	private InputStream productimage;
	private int productprice;
	private String imagestring;
	private String description;
	private int quantity;
	private String aboutproduct;
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public int getSubcategoryid() {
		return subcategoryid;
	}
	public void setSubcategoryid(int subcategoryid) {
		this.subcategoryid = subcategoryid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	public String getSubcategoryname() {
		return subcategoryname;
	}
	public void setSubcategoryname(String subcategoryname) {
		this.subcategoryname = subcategoryname;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public InputStream getProductimage() {
		return productimage;
	}
	public void setProductimage(InputStream productimage) {
		this.productimage = productimage;
	}
	public int getProductprice() {
		return productprice;
	}
	public void setProductprice(int productprice) {
		this.productprice = productprice;
	}
	public String getImagestring() {
		return imagestring;
	}
	public void setImagestring(String imagestring) {
		this.imagestring = imagestring;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCompanyid() {
		return companyid;
	}
	public void setCompanyid(int companyid) {
		this.companyid = companyid;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getAboutproduct() {
		return aboutproduct;
	}
	public void setAboutproduct(String aboutproduct) {
		this.aboutproduct = aboutproduct;
	}
}
