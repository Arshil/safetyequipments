package com.safetyequipment.beans;

public class Shipping  {
//	shipping_id, order_key, order_date, shipping_address, shipping_date, 
//shipping_status, payment_status, order_amt, user_id
	private int shippingid;
	private String orderkey;
	private String orderdate;
	private String shippingdate;
	private String paymentstutus;
	private String shippingstatus;
	private String address;
	private double amount;
	private int userid;
	private int items;
	
	public int getShippingid() {
		return shippingid;
	}
	public void setShippingid(int shippingid) {
		this.shippingid = shippingid;
	}
	public String getOrderkey() {
		return orderkey;
	}
	public void setOrderkey(String orderkey) {
		this.orderkey = orderkey;
	}
	public String getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}
	public String getShippingdate() {
		return shippingdate;
	}
	public void setShippingdate(String shippingdate) {
		this.shippingdate = shippingdate;
	}
	public String getPaymentstutus() {
		return paymentstutus;
	}
	public void setPaymentstutus(String paymentstutus) {
		this.paymentstutus = paymentstutus;
	}
	public String getShippingstatus() {
		return shippingstatus;
	}
	public void setShippingstatus(String shippingstatus) {
		this.shippingstatus = shippingstatus;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getItems() {
		return items;
	}
	public void setItems(int items) {
		this.items = items;
	}
}
