package com.safetyequipment.beans;

import java.io.InputStream;

public class OrderDetails {
private int orderdetailsid;
private int orderid;
private int productid;
private int productprice;
private int productquantity;
private String productname;
private InputStream productimage;
private String imagestring;
public int getOrderdetailsid() {
	return orderdetailsid;
}
public void setOrderdetailsid(int orderdetailsid) {
	this.orderdetailsid = orderdetailsid;
}
public int getOrderid() {
	return orderid;
}
public void setOrderid(int orderid) {
	this.orderid = orderid;
}
public int getProductid() {
	return productid;
}
public void setProductid(int productid) {
	this.productid = productid;
}
public int getProductprice() {
	return productprice;
}
public void setProductprice(int productprice) {
	this.productprice = productprice;
}
public int getProductquantity() {
	return productquantity;
}
public void setProductquantity(int productquantity) {
	this.productquantity = productquantity;
}
public String getProductname() {
	return productname;
}
public void setProductname(String productname) {
	this.productname = productname;
}
public InputStream getProductimage() {
	return productimage;
}
public void setProductimage(InputStream productimage) {
	this.productimage = productimage;
}
public String getImagestring() {
	return imagestring;
}
public void setImagestring(String imagestring) {
	this.imagestring = imagestring;
}
}