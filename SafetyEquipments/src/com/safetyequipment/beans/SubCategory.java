package com.safetyequipment.beans;

import java.io.InputStream;

public class SubCategory {
private int subcategoryid;
private String subcategoryname;
private String categoryname;
private int categoryid;
private InputStream subcategoryimage;
private String imagestring;
private String description;

public InputStream getSubcategoryimage() {
	return subcategoryimage;
}
public void setSubcategoryimage(InputStream subcategoryimage) {
	this.subcategoryimage = subcategoryimage;
}
public String getImagestring() {
	return imagestring;
}
public void setImagestring(String imagestring) {
	this.imagestring = imagestring;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}

public String getSubcategoryname() {
	return subcategoryname;
}
public void setSubcategoryname(String subcategoryname) {
	this.subcategoryname = subcategoryname;
}
public String getCategoryname() {
	return categoryname;
}
public void setCategoryname(String categoryname) {
	this.categoryname = categoryname;
}
public int getCategoryid() {
	return categoryid;
}
public void setCategoryid(int categoryid) {
	this.categoryid = categoryid;
}
public int getSubcategoryid() {
	return subcategoryid;
}
public void setSubcategoryid(int subcategoryid) {
	this.subcategoryid = subcategoryid;
}

}
