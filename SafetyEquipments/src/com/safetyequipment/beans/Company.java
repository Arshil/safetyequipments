package com.safetyequipment.beans;

public class Company {
	
 @Override
	public String toString() {
		return "Company [companyname=" + companyname + ", companycontact=" + companycontact + ", companyemail="
				+ companyemail + ", companyaddress=" + companyaddress + "]";
	}

 private String companyname;
 private String companycontact;
 private String companyemail;
 private String companyaddress;
 private int companyid;
 
 
 
 public String getCompanyname() {
	return companyname;
}
public void setCompanyname(String companyname) {
	this.companyname = companyname;
}
public String getCompanycontact() {
	return companycontact;
}
public void setCompanycontact(String companycontact) {
	this.companycontact = companycontact;
}
public String getCompanyemail() {
	return companyemail;
}
public void setCompanyemail(String companyemail) {
	this.companyemail = companyemail;
}
public String getCompanyaddress() {
	return companyaddress;
}
public void setCompanyaddress(String companyaddress) {
	this.companyaddress = companyaddress;
}
public int getCompanyid() {
	return companyid;
}
public void setCompanyid(int companyid) {
	this.companyid = companyid;
}

 

}
