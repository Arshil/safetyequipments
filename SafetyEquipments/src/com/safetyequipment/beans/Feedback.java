package com.safetyequipment.beans;

import java.io.InputStream;

public class Feedback {
	private int feedbackid;
	private String useremailid;
	private int ratings;
	private String description;
	private int userid;
	private String feedbackDate;
	private String profileImageString;
	private InputStream profileImage;
	
	public int getFeedbackid() {
		return feedbackid;
	}
	public void setFeedbackid(int feedbackid) {
		this.feedbackid = feedbackid;
	}
	public String getUseremailid() {
		return useremailid;
	}
	public void setUseremailid(String useremailid) {
		this.useremailid = useremailid;
	}
	public int getRatings() {
		return ratings;
	}
	
	
	public void setRatings(int ratings) {
		this.ratings = ratings;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getFeedbackDate() {
		return feedbackDate;
	}
	public void setFeedbackDate(String feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
	public String getProfileImageString() {
		return profileImageString;
	}
	public void setProfileImageString(String profileImageString) {
		this.profileImageString = profileImageString;
	}
	public InputStream getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(InputStream profileImage) {
		this.profileImage = profileImage;
	}

}
