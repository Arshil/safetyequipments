package com.safetyequipment.beans;

import java.io.InputStream;
import java.util.List;

public class Category {

	private String categoryname;
	private InputStream categoryimage;
	private String description;
	private int categoryid;
	private String categoryImageString;
	private List<SubCategory> subcategorylist;
	
	@Override
	public String toString() {
		return "Category [categoryname=" + categoryname + ", categoryimage=" + categoryimage + ", description="
				+ description + "]";
	}
	
	
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
		
	public InputStream getCategoryimage() {
		return categoryimage;
	}
	public void setCategoryimage(InputStream categoryimage) {
		this.categoryimage = categoryimage;
	}


	public String getCategoryImageString() {
		return categoryImageString;
	}


	public void setCategoryImageString(String categoryImageString) {
		this.categoryImageString = categoryImageString;
	}


	public int getCategoryid() {
		return categoryid;
	}


	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}


	public List<SubCategory> getSubcategorylist() {
		return subcategorylist;
	}


	public void setSubcategorylist(List<SubCategory> subcategorylist) {
		this.subcategorylist = subcategorylist;
	}

}
