package com.safetyequipment.beans;

import java.util.List;

import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Payment;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.Shipping;

public class MyOrder {
private Order order;
private OrderDetails orderdetails;
private Payment payment;
private Shipping shipping;
private List<OrderDetails> orderdetailslist;
private List<Product> productlist;
public Order getOrder() {
	return order;
}
public void setOrder(Order order) {
	this.order = order;
}
public OrderDetails getOrderdetails() {
	return orderdetails;
}
public void setOrderdetails(OrderDetails orderdetails) {
	this.orderdetails = orderdetails;
}
public Payment getPayment() {
	return payment;
}
public void setPayment(Payment payment) {
	this.payment = payment;
}
public Shipping getShipping() {
	return shipping;
}
public void setShipping(Shipping shipping) {
	this.shipping = shipping;
}
public List<Product> getProductlist() {
	return productlist;
}
public void setProductlist(List<Product> productlist) {
	this.productlist = productlist;
}
public List<OrderDetails> getOrderdetailslist() {
	return orderdetailslist;
}
public void setOrderdetailslist(List<OrderDetails> orderdetailslist) {
	this.orderdetailslist = orderdetailslist;
}
}
