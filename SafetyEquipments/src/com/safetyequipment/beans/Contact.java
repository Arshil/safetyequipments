package com.safetyequipment.beans;

public class Contact {


	private String fname;
	private String lname;
	private String email;
	private int contactid;
	private String subject;
	private String inquirydesc;
	private String date;
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getContactid() {
		return contactid;
	}
	public void setContactid(int contactid) {
		this.contactid = contactid;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getInquirydesc() {
		return inquirydesc;
	}
	public void setInquirydesc(String inquirydesc) {
		this.inquirydesc = inquirydesc;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
