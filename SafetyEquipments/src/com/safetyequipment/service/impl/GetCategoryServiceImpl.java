package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.util.ArrayList;

import com.safetyequipment.beans.Category;
import com.safetyequipment.dao.GetCategory;
import com.safetyequipment.dao.impl.GetCategoryList;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.util.CommonUtil;

public class GetCategoryServiceImpl implements GetCategoryService{

	

	GetCategory categoryobj  =  new GetCategoryList();
	
	@Override
	public ArrayList<Category> getcategorylist() {
		
		try (Connection connection = CommonUtil.getconnection()) {
			 ArrayList<Category> categorylist = categoryobj.getcategorydetails(connection);
			 return categorylist;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
}
