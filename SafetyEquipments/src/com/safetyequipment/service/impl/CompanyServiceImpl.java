package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Company;
import com.safetyequipment.dao.CompanyDao;
import com.safetyequipment.dao.impl.CompanyDaoImpl;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.util.CommonUtil;

public class CompanyServiceImpl implements CompanyService {

	CompanyDao companydao = new CompanyDaoImpl();

	@Override
	public String addcompany(Company company) {
		int insertedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			insertedrows = companydao.addcompanydetails(connection, company);
			System.out.println("Rows : " + insertedrows);
			if (insertedrows > 0) {
				return "company added";
			} else {
				return "company not added";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<Company> selectcompanylist() {
		try (Connection connection = CommonUtil.getconnection()) {
			return companydao.getcompanydetails(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String removecompanyRecord(String companyid) {
		try (Connection connection = CommonUtil.getconnection()) {
			int deletedRows = companydao.deletecompanyDetails(connection, Integer.parseInt(companyid));
			if (deletedRows > 0) {
				return "deleted";
			} else {
				return "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Company getselectedcompany(String company_id) {
		try (Connection connection = CommonUtil.getconnection()) {
			return companydao.selectcompany(connection, Integer.parseInt(company_id));
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public String updatecompany(Company company) {
		// TODO Auto-generated method stub

		int updatedrows = 0;
		try (Connection connection = CommonUtil.getconnection();) {
			updatedrows = companydao.modifycompany(connection, company);
			System.out.println("updated rows:" + updatedrows);
			if (updatedrows > 0) {
				return "Success";
			} else {
				return "Fail";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;

	}

	@Override
	public List<Company> getCompanyListOfSelectedProduct(int productid) {
		try (Connection connection = CommonUtil.getconnection()) {
			return companydao.getCompanyListOfSelectedProduct(connection,productid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
}
