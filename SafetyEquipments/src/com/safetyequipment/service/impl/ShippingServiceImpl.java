package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.beans.User;
import com.safetyequipment.dao.ShippingDao;
import com.safetyequipment.dao.impl.ShippingDaoImpl;
import com.safetyequipment.service.ShippingService;
import com.safetyequipment.util.CommonGmail;
import com.safetyequipment.util.CommonUtil;

public class ShippingServiceImpl implements ShippingService {

	ShippingDao shippingDao = new ShippingDaoImpl();
	CommonGmail commonGmail = new CommonGmail();

	@Override
	public String addShippingOrder(Shipping shipping) {
		try (Connection connection = CommonUtil.getconnection()) {
			int insertedrows = shippingDao.addShippingOrder(connection, shipping);
			System.out.println("Rows : " + insertedrows);
			if (insertedrows > 0) {
				return "Order inserted for shipping successfully";
			} else {
				return "Order can't  insert for shipping successfully";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public String updatePaymentStatus(String orderkey) {
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedrows = shippingDao.updatePaymentStatus(connection, orderkey);
			// System.out.println("Rows : " + updatedrows);
			if (updatedrows > 0) {
				return "Order updated successfully";
			} else {
				return "Order can't  updated successfully";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Shipping selectedOrder(String orderkey, int userid) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()) {
			return shippingDao.selectedOrder(connection, orderkey, userid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<Product> productListForComplaint(Shipping shipping) {
		try (Connection connection = CommonUtil.getconnection()) {
			return shippingDao.productListForComplaint(connection, shipping);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Shipping selecedShipping(String orderkey) {
		try (Connection connection = CommonUtil.getconnection()) {
			return shippingDao.selecedShipping(connection, orderkey);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String updateShippingStatus(Shipping shipping) {
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedShippingStatusrows = shippingDao.updateShippingStatus(connection, shipping);
			if (updatedShippingStatusrows > 0) {
				return "shipping status updated successfully";
			} else {
				return "can't update shipping status";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String cancelOrder(Shipping shipping, User userobj) {
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedShippingStatusrows = shippingDao.cancelOrder(connection, shipping);
			if (updatedShippingStatusrows > 0) {
				if (shipping.getPaymentstutus().equals("completed")) {
					// String message = "Registration success";

					String subject = "Cancel Order";

					// System.out.println(":fnmae : " + user.getFirstname());

					String text = "You order which hs order id " + shipping.getOrderkey()
							+ " hass been canceeled according to your request . Your payment which has been paid by you will be given back to you. Our employee will contact you soon . ";

					commonGmail.sendMail(userobj.getEmailid(), subject, text);

				}
				return "order has been cancelled";
			} else {
				return "can't cancel order";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

// TODO Auto-generated method stub
	@Override
	public String addShipping(Shipping shipping) {
		try (Connection connection = CommonUtil.getconnection()) {
			int addShipping = shippingDao.addShipping(connection, shipping);
			if (addShipping > 0) {
				return "shipping date added successfully";
			} else {
				return "can't add shipping date";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String editShipping(Shipping shipping) {
		try (Connection connection = CommonUtil.getconnection()) {
			int editShipping = shippingDao.editShipping(connection, shipping);
			if (editShipping > 0) {
				return "shipping date edited successfully";
			} else {
				return "can't edit shipping date";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public double[] totalSelling() {
		try (Connection connection = CommonUtil.getconnection()) {
			return shippingDao.totalSelling(connection);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}
