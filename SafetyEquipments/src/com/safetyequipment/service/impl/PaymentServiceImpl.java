package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;

import com.safetyequipment.beans.Payment;
import com.safetyequipment.dao.PaymentDao;
import com.safetyequipment.dao.impl.PaymentDaoImpl;
import com.safetyequipment.service.PaymentService;
import com.safetyequipment.util.CommonUtil;

public class PaymentServiceImpl implements PaymentService{
PaymentDao paymentDao  = new PaymentDaoImpl();
	@Override
	public String registerPayment(Payment payment) {
		try(Connection connection = CommonUtil.getconnection())
		{
			int insertedRows = paymentDao.registerPayment(connection, payment);
			if(insertedRows > 0)
			{
				return "success";
				
			}
			else
			{
				return "false";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
		return null;
	}
	
	@Override
	public String registerCODPayment(Payment payment) 
	{
		try(Connection connection = CommonUtil.getconnection())
		{
			int insertedRows = paymentDao.registerCODPayment(connection, payment);
			if(insertedRows > 0)
			{
				return "success";
				
			}
			else
			{
				return "false";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
		return null;
	}
	
}
