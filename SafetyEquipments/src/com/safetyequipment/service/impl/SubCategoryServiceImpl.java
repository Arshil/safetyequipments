package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.dao.CategoryDao;
import com.safetyequipment.dao.SubCategoryDao;
import com.safetyequipment.dao.impl.CategoryDaoimpl;
import com.safetyequipment.dao.impl.SubCategoryDaoImpl;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.util.CommonUtil;

public class SubCategoryServiceImpl implements SubCategoryService {

	// CategoryDao categorydao = new CategoryDaoimpl();
	SubCategoryDao subcategorydao = new SubCategoryDaoImpl();

	@Override
	public List<SubCategory> selectedSubCategory() {
		try(Connection connection = CommonUtil.getconnection()) {
			return subcategorydao.selectSubCategoryList(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String insertSubcayegory(SubCategory subcategory) {
		try (Connection connection = CommonUtil.getconnection()){
			int insertedrows = subcategorydao.addSubCategory(connection, subcategory);
			if (insertedrows > 0) {
				return "SubCategory added Successfully";
			} else {
				return "Can't  add.";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public SubCategory selectedsubcategory(int id) {
		try(Connection connection = CommonUtil.getconnection()) {
			return subcategorydao.selectedSubCategory(connection, id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public String editsubcategory(SubCategory subcategory) {

		try(Connection connection = CommonUtil.getconnection()) {
			int updatedrows = subcategorydao.editSubcategory(connection, subcategory);
			if (updatedrows > 0) {
				return "Sub Category Updated Successfully";
			} else {
				return "Can't Update";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public String deletesubcategory(int id) {
		try (Connection connection = CommonUtil.getconnection()){
			int deletedrows = subcategorydao.deleteSubCategory(connection, id);
			if (deletedrows > 0) {
				return "Deleted Sub Category successfully";
			} else {
				return "Can't Deleted Sub Category";
			}
		}catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	@Override
	public List<SubCategory> selectedcategorySubCategory(int categoryid) {
		try (Connection connection = CommonUtil.getconnection()){
			return subcategorydao.selectedCategorySubcategoryList(connection,categoryid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
