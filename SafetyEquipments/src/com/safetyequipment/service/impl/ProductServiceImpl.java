package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.dao.ProductDao;
import com.safetyequipment.dao.impl.ProductDaoImpl;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.util.CommonUtil;

public class ProductServiceImpl implements ProductService{

	ProductDao productdao = new ProductDaoImpl();
	
	
	@Override
	public String insertProduct(Product product) {
		try(Connection connection = CommonUtil.getconnection()) {
			int insertedrows = productdao.addProduct(connection,product);
			if (insertedrows > 0) {
				return "Product added Successfully";
			} else {
				return "Can't  add.";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;

	}
	
	@Override
	public List<Product> selectedproductList() {
		try(Connection connection = CommonUtil.getconnection())
		{
			return productdao.selectAllProductlist(connection);
		}
		catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Product selectedproduct(int id) {
		try(Connection connection = CommonUtil.getconnection()) {
			return productdao.selectedProduct(connection, id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String editproduct(Product product) {

		try(Connection connection = CommonUtil.getconnection()) {
			int updatedrows = productdao.editProduct(connection,product);
			if (updatedrows > 0) {
				return "Product Updated Successfully";
			} else {
				return "Can't Update";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public String deleteproduct(int id) {
		try(Connection connection = CommonUtil.getconnection()) {
			int deletedrows = productdao.deleteProduct(connection, id);
			if (deletedrows > 0) {
				return "Deleted Product successfully";
			} else {
				return "Can't Deleted Product";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	@Override
	public List<Product> getProductList(String subcategoryid) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()){
			return  productdao.fetchProductList(connection,Integer.parseInt(subcategoryid));
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<Product> getProductsOfSelectedCategory(String categoryId) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()){
			return  productdao.fetchAllProductListByCategoryId(connection,Integer.parseInt(categoryId));
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public ArrayList<Product> searchproduct(String productname) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()){
			return  productdao.searchEnteredProduct(connection,productname);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

	@Override
	public String  updateProductQuantity(List<Cart> cartlist)
	{
		try (Connection connection = CommonUtil.getconnection()){
			int updatedrows[]  =  productdao.updateProductQuantity(connection, cartlist);
			if(updatedrows.length == cartlist.size())
			{
				return "products quantity updated successfully";
			}
			else
			{
				return "can't update products quantity ";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String addProductQuantity(List<OrderDetails> orderDetailslist) {
		try (Connection connection = CommonUtil.getconnection()){
			int updatedrows[]  =  productdao.addProductQuantity(connection, orderDetailslist);
			if(updatedrows.length == orderDetailslist.size())
			{
				return "products quantity added successfully";
			}
			else
			{
				return "can't add products quantity ";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
}
