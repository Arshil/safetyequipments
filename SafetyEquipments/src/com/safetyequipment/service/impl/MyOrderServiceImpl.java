package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.dao.MyOrderDao;
import com.safetyequipment.dao.impl.MyOrderDaoImpl;
import com.safetyequipment.service.MyOrderService;
import com.safetyequipment.util.CommonUtil;

public class MyOrderServiceImpl implements MyOrderService {
	MyOrderDao myOrderDao = new MyOrderDaoImpl();

	@Override
	public List<MyOrder> myOrderList(int userid) {

		try (Connection connection = CommonUtil.getconnection()) {

			return myOrderDao.myOrderList(connection, userid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<MyOrder> allOrderList() {
		try (Connection connection = CommonUtil.getconnection()) {
			return myOrderDao.allOrderList(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public MyOrder selectedMyOrder(String orderkey) {
		try (Connection connection = CommonUtil.getconnection()) {
			System.out.println("step 2:");
			return myOrderDao.selectedMyOrder(connection, orderkey);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
