package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.Catch;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Contact;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.User;
import com.safetyequipment.dao.UserDao;
import com.safetyequipment.dao.impl.UserDaoImpl;
import com.safetyequipment.service.UserService;
import com.safetyequipment.util.*;

public class UserRegisterServiceImpl implements UserService {

	CommonGmail commonGmail = new CommonGmail();
	UserDao userdao = new UserDaoImpl();

	@Override
	public String userregister(User user) {
		int insertedrows;

		try (Connection connection = CommonUtil.getconnection();) {
			System.out.println("email in service : " + user.getEmailid());
			insertedrows = userdao.userregistration(connection, user);
			String message = "Registration success";

			String subject = "Welcome to Dipen Safety PVT. LTD. \n Please Do not reply to this email. This is a system generated email.";

			System.out.println(":fnmae : " + user.getFirstname());

			String text = "Thank you " + user.getFirstname() + " " + user.getLastname() + " for Registration";
			if (insertedrows > 0) {

				if (message.equals("Registration success")) {
					commonGmail.sendMail(user.getEmailid(), subject, text);
					return message;
				}
			} else {
				return "Registration fail";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User authenticate(String email, String password) {

		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			return userdao.validateuser(connection, email, password);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String modifyUserDetails(User user) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			int c = userdao.updateuserdetails(connection, user);
			if (c > 0) {
				return "update success";
			} else {
				return "update fail";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<User> allUserList() {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			return userdao.alluserlist(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String deleteuser(int userid) {
		try (Connection connection = CommonUtil.getconnection()) {
			int deletedrows = userdao.deleteUser(connection, userid);
			if (deletedrows > 0) {
				User user = userdao.selectedUser(connection, userid);
				String subject = "Deactivation of Account";
				// System.out.println(":fnmae : " + user.getFirstname());

				String text = "Hello "+user.getFirstname() +"  "+user.getLastname()+" We are sorry to inform you that your account has been deactivated by the admin . ";

				commonGmail.sendMail(user.getEmailid(), subject, text);
				return "Deleted User successfully";
			} else {
				return "Can't Deleted User";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User selecteduser(int userid) {
		try (Connection connection = CommonUtil.getconnection()) {
			return userdao.selectedUser(connection, userid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String edituser(User user) {

		try (Connection connection = CommonUtil.getconnection()) {
			int updatedrows = userdao.editUser(connection, user);
			if (updatedrows > 0) {
				return "User Updated Successfully";
			} else {
				return "Can't Update";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public String selecteduserpassword(int userid) {
		try (Connection connection = CommonUtil.getconnection()) {
			// System.out.println("user impl called");
			return userdao.selectedUserPassword(connection, userid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String userpassswordchange(int userid, String password) {
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedrows = userdao.userPasswordChange(connection, userid, password);
			if (updatedrows > 0) {
				return "Password Changed Successfully";
			} else {
				return "Can't Change Password";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String selecteduserforgotpassword(String emailid) {
		try (Connection connection = CommonUtil.getconnection()) {
			// System.out.println("user impl called");
			String message = userdao.selectedUserForgotPassword(connection, emailid);
			if (message.equals("Not Found")) {
				return "You have entered invalid emailid.Please enter registered emailid.";
			} else {
				String subject = " Dipen Safety Password ";
				String text = "Your password is :" + message + " . \n Please don't share your password with anyone.";
				commonGmail.sendMail(emailid, subject, text);
				return "Your password has been sent to registered email id";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String validateEmail(String userEmail) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			int i = userdao.validateEmail(connection, userEmail);
			if (i == 1) {
				return "";
			} else {
				return "Email ID is not registered";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public String validateuserregistrationEmail(String userEmail) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			int i = userdao.validateUserRegistrationEmail(connection, userEmail);
			System.out.println("i in service impl :" + i);
			if (i >= 1) {
				return "Already Registered Email Id";
			} else {
				return " ";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public String validateuserlogin(String emailid, String password) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()) {
			int i = userdao.validateUserLogin(connection, emailid, password);
			if (i == 1) {
				return "";
			} else {
				return "Invalid Credentials";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String addInquiry(Contact contact) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()) {
			int i = userdao.makeInquiry(connection, contact);
			if (i == 1) {
				String message = "Registration success";

				String subject = "Dipen Safety PVT. LTD.";

				// System.out.println(":fnmae : " + .getFirstname());

				String text = "Thank you for contact with us ." + contact.getFname() + " " + contact.getLname()
						+ ". We will shortly connect with you. \n Have a nice Day. Regards";
				if (i > 0) {

					if (message.equals("Registration success")) {
						commonGmail.sendMail(contact.getEmail(), subject, text);
						return message;
					}
				} else {
					return "Registration fail";
				}

				return "";
			} else {
				return "failed";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Contact> allInquiriesList() {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			return userdao.allInquiriesList(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String activeUser(int userid) {
	try (Connection connection = CommonUtil.getconnection()) {
		int updatedrows = userdao.activeUser(connection, userid);
		if (updatedrows > 0) {
			User user = userdao.selectedUser(connection, userid);
			String subject = "Activation of Account";
			// System.out.println(":fnmae : " + user.getFirstname());

			String text = "Hello "+user.getFirstname() +"  "+user.getLastname()+" We are happy to inform you that you have been activeted by the admin . ";

			commonGmail.sendMail(user.getEmailid(), subject, text);
			return "Activated User successfully";
		} else {
			return "Can't activate User";
		}
	} catch (ClassNotFoundException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return null;
}
	@Override
	public String updateUserAddress(User user)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedAddress = userdao.updateUserAddress(connection, user);
			if(updatedAddress > 0)
			{
				return "user address updated successfully";
			}
			else
			{
				return "can't update user address";
			}
		}
		catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String updateUserPincode(User user)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedPincode = userdao.updateUserPincode(connection, user);
			if(updatedPincode > 0)
			{
				return "user pincode updated successfully";
			}
			else
			{
				return "can't update user pincode";
			}
		}
		catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String updateUserMobile(User user)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedMobile = userdao.updateUserMobile(connection, user);
			if(updatedMobile > 0)
			{
				return "user mobile updated successfully";
			}
			else
			{
				return "can't update user mobile";
			}
		}
		catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}