package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Feedback;
import com.safetyequipment.beans.User;
import com.safetyequipment.dao.FeedbackDao;
import com.safetyequipment.dao.impl.FeedbackDaoImpl;
import com.safetyequipment.service.FeedbackService;
import com.safetyequipment.util.CommonGmail;
import com.safetyequipment.util.CommonUtil;

public class FeedbackServiceImpl implements FeedbackService{

	FeedbackDao feedbackdao = new FeedbackDaoImpl();
	CommonGmail commonGmail = new CommonGmail();
	@Override
	public String insertFeedback(Feedback feedback) {
		// TODO Auto-generated method stub
		int insertedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			insertedrows = feedbackdao.addfeedback(connection, feedback);
			System.out.println("feed Rows : " + insertedrows);
			if (insertedrows > 0) {
				String subject = "Feedback Response";
				String text = "Dear Customer," + 
						"Thank you for sharing your valuable feedback,"
						+ " we will certainly forward it to the relevant department and necessary steps "
						+ "will be taken to curb these type of issues in the future." + 
						"Additionally, for a faster resolution, we request you to use"
						+ " the chat feature through Dipen Safety Equipments Website and experience a faster and simpler"
						+ " way of raising your query/request/complaint." + 
						"Regards," + 
						"Team Dipen Safety Equipments .";
				commonGmail.sendMail(feedback.getUseremailid(), subject, text);
				return "success";
			} else {
				return "fail";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Feedback> allFeedbackList() {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection();) {
			System.out.println("servicde start ");
			//System.out.println("called dao");
			return feedbackdao.allFeedbackList(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
