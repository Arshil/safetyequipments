package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import com.safetyequipment.beans.Category;
import com.safetyequipment.dao.CategoryDao;
import com.safetyequipment.dao.impl.CategoryDaoimpl;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.util.CommonGmail;
import com.safetyequipment.util.CommonUtil;

public class CategoryServiceImpl implements CategoryService {

	CommonGmail commonGmail = new CommonGmail();
	CategoryDao categorydao = new CategoryDaoimpl();

	@Override
	public String addcategory(Category category) {
		int insertedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			insertedrows = categorydao.addcategorydetails(connection, category);
			System.out.println("Rows : " + insertedrows);
			if (insertedrows > 0) {
				return "category added";
			} else {
				return "category not added";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}
	@Override
	public String removeCategoryRecord(String id) {
		try (Connection connection = CommonUtil.getconnection()) {
			int deletedRows = categorydao.deletecategoryDetails(connection, Integer.parseInt(id));
			if(deletedRows >0)
			{
				return "deleted";
			}
			else
			{
				return "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<Category> selectCategorylist()
	{
		try (Connection connection = CommonUtil.getconnection()){
			return categorydao.selectCategory(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public Category getselectedcategory(String category_id)
	{
		try(Connection connection = CommonUtil.getconnection()){
			return categorydao.selectcategory(connection, Integer.parseInt(category_id));
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	@Override
	public String updatecategory(Category category)
	{
		
		int updatedrows = 0;
		try (Connection connection = CommonUtil.getconnection();) 
		{
				updatedrows = categorydao.modifycategory(connection,category);
				System.out.println("u rows:" + updatedrows);
				if(updatedrows > 0) 
				{
					return "Success";
				}
				else 
				{
					return "Fail";
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		return null;
			
	
		}
		
}
	


