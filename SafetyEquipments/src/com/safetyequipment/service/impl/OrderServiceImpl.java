package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.dao.OrderDao;
import com.safetyequipment.dao.impl.OrderDaoImpl;
import com.safetyequipment.service.OrderService;
import com.safetyequipment.util.CommonUtil;

public class OrderServiceImpl implements OrderService {

	OrderDao orderdao = new OrderDaoImpl();

	@Override
	public int registerOrderDetails(Order order) {
		// TODO Auto-generated method stub
		try (Connection connection = CommonUtil.getconnection()) {
			return orderdao.registerOrder(connection, order);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int[] saveOrderDetails(List<Cart> cartlist, int ordersummeryid) {
		
		List<OrderDetails> orderDetails = new ArrayList<>();
		
		for(Cart cart : cartlist) {
			OrderDetails details = new OrderDetails();
			details.setOrderid(ordersummeryid);
			fillOrderDetails(details, cart);
			orderDetails.add(details);
		}
		
		try (Connection connection = CommonUtil.getconnection()) {
			return orderdao.insertOrderDetails(connection,orderDetails);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static void fillOrderDetails(OrderDetails orderDetails, Cart cart) {
		
		orderDetails.setProductid(cart.getProductid());
		orderDetails.setProductprice(cart.getProduct().getProductprice());
		orderDetails.setProductquantity(cart.getQuatity());
		
	}

}
