package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Complaint;
import com.safetyequipment.dao.ComplaintDao;
import com.safetyequipment.dao.impl.ComplaintDaoImpl;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.util.CommonGmail;
import com.safetyequipment.util.CommonUtil;

public class ComplaintServiceImpl implements ComplaintService {

	ComplaintDao complaintDao = new ComplaintDaoImpl();
	CommonGmail commonGmail = new CommonGmail();

	@Override
	public String addComplaint(Complaint complaint) {
		try (Connection connection = CommonUtil.getconnection()) {
			int insertedComplaint = complaintDao.addComplaint(connection, complaint);
			if (insertedComplaint > 0) {
				return "complaint inserted successfully";
			} else {
				return "can't insert complaint";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String deleteComplaint(int complaintId) {
		try (Connection connection = CommonUtil.getconnection()) {
			int deletedComplaint = complaintDao.deleteComplaint(connection, complaintId);
			if (deletedComplaint > 0) {
				return "complaint deleted successfully";
			} else {
				return "can't delete complaint";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Complaint> allComplaintList() {
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.allComplaintList(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String updateComplaint(Complaint complaint) {
		try (Connection connection = CommonUtil.getconnection()) {
			System.out.println("step service");
			int updatedComplaint = complaintDao.updateComplaint(connection, complaint);
			System.out.println("updatedrows : " + updatedComplaint);
			if (updatedComplaint > 0) {
				return "complaint updated successfully";
			} else {
				return "can't update complaint";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Complaint selectedComplaint(int complaintid) {
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.selectedComplaint(connection, complaintid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Complaint> selectedProductComplaintList(int productid) {
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.selectedProductComplaintList(connection, productid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String registerUserComplaint(Complaint complaint) {
		try (Connection connection = CommonUtil.getconnection()) {
			int insertedComplaint = complaintDao.registerUserComplaint(connection, complaint);
			if (insertedComplaint > 0) {
				String subject = "complaint registration";
				String text = "Hello there!" + " Thank you for getting in touch with us. "
						+ "We have received your request and have assigned a ticket id " + insertedComplaint
						+ " against the same. "
						+ "Our executive will review your request and get in touch with you soon."
						+ " Please note: For a faster resolution, you may raise a chat via Dipen Safety Equipments Website."
						+ " In case you do not find an option to chat with us, please upgrade your Dipen Safety Equipments Website. "
						+ "Regards," + "Team Dipen Safety Equipments .";
				commonGmail.sendMail(complaint.getUseremail(), subject, text);
				return "complaint registered successfully";

			} else {
				return "can't register complaint";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<Complaint> userAllComplaints(int userid)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.userAllComplaints(connection, userid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Complaint selectedUserComplaint( int usercomplaintid) {
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.selectedUserComplaint(connection, usercomplaintid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Complaint> complaintListSelectedProduct(int productid)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.complaintListSelectedProduct(connection, productid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String editUserComplaint(Complaint complaint)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			System.out.println("step service");
			int editedComplaint = complaintDao.editUserComplaint(connection, complaint);
			System.out.println("edited rows : " + editedComplaint);
			if (editedComplaint > 0) {
				return "complaint edited successfully";
			} else {
				return "can't edited  complaint";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<Complaint> allUserComplaints()
	{
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.allUserComplaints(connection);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public Complaint editSelectedUserComplaint(int usercomplaintid)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			return complaintDao.editSelectedUserComplaint(connection, usercomplaintid);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String editComplaintStatus(Complaint complaint)
	{
		try (Connection connection = CommonUtil.getconnection()) {
			int updatedcomplaints =  complaintDao.editComplaintStatus(connection, complaint);
			if(updatedcomplaints > 0)
			{
				return "complaint status updated succeessfully";
			}
			else
			{
				return "can't update complaint status";
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
