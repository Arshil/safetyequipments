package com.safetyequipment.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Product;
import com.safetyequipment.dao.CartDao;
import com.safetyequipment.dao.impl.CartDaoImpl;
import com.safetyequipment.service.CartService;
import com.safetyequipment.util.CommonUtil;

public class CartServiceImpl implements CartService{

	CartDao cartdao = new CartDaoImpl();
	
	@Override
	public String addProductToCart(Cart cart) {
		// TODO Auto-generated method stub
		int insertedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			insertedrows = cartdao.addcartproducts(connection, cart);
			System.out.println("Rows : " + insertedrows);
			if (insertedrows > 0) {
				return "product added";
			} else {
				return "product not added";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Cart> selectedusercartlist(int userid) {
		try(Connection connection = CommonUtil.getconnection())
		{
			return cartdao.selectedUserCartList(connection, userid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
		
	}
	
	@Override
	public String deletecartproduct(int cartid) {
		// TODO Auto-generated method stub
		int deletedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			deletedrows = cartdao.deleteCartProduct(connection, cartid);
			System.out.println("Rows : " + deletedrows);
			if (deletedrows > 0) {
				return "Product has been removed from your cart successfully";
			} else {
				return "Product can't remov your cart.";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String removeProductFromCart(int userid, int productid) {
		// TODO Auto-generated method stub
		int deleteddrows;
		try (Connection connection = CommonUtil.getconnection()) {
			deleteddrows = cartdao.removeproductfromcart(connection, userid,productid);
			
			if (deleteddrows > 0) {
				return "product removed";
			} else {
				return "product not removed";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

		
		
	}
	@Override
	public String upadteproductquantity(int userid, int productid,int quantity) {
		// TODO Auto-generated method stub
		int updatedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			updatedrows = cartdao.updateCartQuantity(connection, userid,productid,quantity);
			
			if (updatedrows > 0) {
				return "cart upadted successfully";
			} else {
				return "can't update";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

		
		
	}
	
	@Override
	public String removeAllCartProducts(int userid) {
		// TODO Auto-generated method stub
		int deletedrows;
		try (Connection connection = CommonUtil.getconnection()) {
			deletedrows = cartdao.removeAllCartProducts(connection, userid);
			System.out.println("Rows : " + deletedrows);
			if (deletedrows > 0) {
				return "Products has been removed from your cart successfully";
			} else {
				return "Product can't remov your cart.";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}
