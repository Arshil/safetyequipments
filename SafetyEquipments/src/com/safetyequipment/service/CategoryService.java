package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Category;

public interface CategoryService {

	public String addcategory(Category category);

	public List<Category> selectCategorylist();

	String removeCategoryRecord(String id);

	Category getselectedcategory(String category_id);


	public String updatecategory(Category category);


	

}
