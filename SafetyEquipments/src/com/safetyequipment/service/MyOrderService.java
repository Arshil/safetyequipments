package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.MyOrder;

public interface MyOrderService {

	public List<MyOrder> myOrderList(int userid);

	public List<MyOrder> allOrderList();

	public MyOrder selectedMyOrder(String orderkey);

}
