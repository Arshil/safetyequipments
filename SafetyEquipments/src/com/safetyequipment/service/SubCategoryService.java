package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.SubCategory;

public interface SubCategoryService {

	public String insertSubcayegory(SubCategory subcategory);

	public List<SubCategory> selectedSubCategory();

	public SubCategory selectedsubcategory(int id);

	public String editsubcategory(SubCategory subcategory);

	public String deletesubcategory(int id);

	public List<SubCategory> selectedcategorySubCategory(int categoryid);

}
