package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Product;

public interface CartService {

	public String addProductToCart(Cart cart);

	public List<Cart> selectedusercartlist(int userid);

	public String deletecartproduct(int cartid);
	
	public String removeProductFromCart(int userid, int productid);

	public String upadteproductquantity(int userid, int productid, int quantity);

	public String removeAllCartProducts(int userid);

}
