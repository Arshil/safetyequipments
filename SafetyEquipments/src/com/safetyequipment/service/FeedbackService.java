package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Feedback;

public interface FeedbackService {

	public String insertFeedback(Feedback feedback);

	public List<Feedback> allFeedbackList();

}
