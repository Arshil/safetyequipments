package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Complaint;

public interface ComplaintService {

	public String addComplaint(Complaint complaint);

	public String deleteComplaint(int complaintId);

	public List<Complaint> allComplaintList();

	public String updateComplaint(Complaint complaint);

	public Complaint selectedComplaint(int complaintid);

	public List<Complaint> selectedProductComplaintList(int productid);

	public String registerUserComplaint(Complaint complaint);

	public List<Complaint> userAllComplaints(int userid);

	public Complaint selectedUserComplaint(int usercomplaintid);

	public List<Complaint> complaintListSelectedProduct(int productid);

	public String editUserComplaint(Complaint complaint);

	public List<Complaint> allUserComplaints();

	public Complaint editSelectedUserComplaint(int usercomplaintid);

	public String editComplaintStatus(Complaint complaint);

}
