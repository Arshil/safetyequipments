package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.beans.User;

public interface ShippingService {

	public String addShippingOrder(Shipping shipping);

	public String updatePaymentStatus(String orderkey);

	public Shipping selectedOrder(String orderkey, int userid);

	public List<Product> productListForComplaint(Shipping shipping);

	public Shipping selecedShipping(String orderkey);

	public String updateShippingStatus(Shipping shipping);

	public String cancelOrder(Shipping shipping,User userobj);

	public String addShipping(Shipping shipping);

	public String editShipping(Shipping shipping);

	public double[] totalSelling();

}
