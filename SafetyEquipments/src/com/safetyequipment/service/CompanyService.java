package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Company;

public interface CompanyService {

	public String addcompany(Company company);

	public List<Company> selectcompanylist();

	public String removecompanyRecord(String parameter);

	public Company getselectedcompany(String parameter);

	public String updatecompany(Company company);

	public List<Company> getCompanyListOfSelectedProduct(int productid);

	

}
