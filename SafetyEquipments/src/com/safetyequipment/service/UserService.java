package com.safetyequipment.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.safetyequipment.beans.Contact;
import com.safetyequipment.beans.User;

public interface UserService {

	public String userregister(User user) throws ClassNotFoundException, SQLException;

	public User authenticate(String email, String password);

	public String modifyUserDetails(User user);

	public List<User> allUserList();

	public String deleteuser(int userid);

	public User selecteduser(int userid);

	public String edituser(User user);

	public String selecteduserpassword(int userid);

	public String userpassswordchange(int userid, String password);

	public String selecteduserforgotpassword(String emailid);

	public String validateEmail(String userEmail);

	public String validateuserregistrationEmail(String userEmail);

	public String validateuserlogin(String emailid, String password);

	public String addInquiry(Contact contact);

	public List<Contact> allInquiriesList();

	public String activeUser(int userid);

	public String updateUserAddress(User user);

	public String updateUserPincode(User user);

	public String updateUserMobile(User user);

}
