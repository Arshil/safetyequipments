package com.safetyequipment.service;

import java.util.ArrayList;
import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.OrderDetails;
import com.safetyequipment.beans.Product;

public interface ProductService {

	public String insertProduct(Product product);

	public List<Product> selectedproductList();

	public Product selectedproduct(int id);

	public String editproduct(Product product);

	public String deleteproduct(int id);

	public List<Product> getProductList(String subcategoryid);

	public List<Product> getProductsOfSelectedCategory(String parameter);

	public ArrayList<Product> searchproduct(String parameter);

	public String updateProductQuantity(List<Cart> cartlist);

	public String addProductQuantity(List<OrderDetails> orderDetailslist);

}
