package com.safetyequipment.service;

import java.util.ArrayList;

import com.safetyequipment.beans.Category;

public interface GetCategoryService {

	ArrayList<Category> getcategorylist();

}
