package com.safetyequipment.service;

import java.util.List;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Order;

public interface OrderService {

	public int registerOrderDetails(Order order);

	public int[] saveOrderDetails(List<Cart> cartlist, int ordersummeryid);

}
