package com.safetyequipment.service;

import com.safetyequipment.beans.Payment;

public interface PaymentService {

	public String registerPayment(Payment payment);

	public String registerCODPayment(Payment payment);

}
