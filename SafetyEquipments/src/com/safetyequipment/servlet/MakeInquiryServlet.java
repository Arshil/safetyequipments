package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Contact;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class MakeInquiryServlet
 */
public class MakeInquiryServlet extends HttpServlet {
	UserService service =new  UserRegisterServiceImpl();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MakeInquiryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Contact contact = new Contact();
		contact.setFname(request.getParameter("firstName"));
		contact.setLname(request.getParameter("lastName"));
		contact.setEmail(request.getParameter("emailid"));
		contact.setInquirydesc(request.getParameter("message"));
		contact.setSubject(request.getParameter("subject"));
		String message = service.addInquiry(contact);
		//RequestDispatcher requestDispatcher = request.getRequestDispatcher();
		//requestDispatcher.forward(request, response);
		response.sendRedirect("contact.jsp");
		
	}

}
