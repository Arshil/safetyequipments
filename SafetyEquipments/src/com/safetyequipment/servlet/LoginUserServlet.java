package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.User;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class LoginUserServlet
 */
public class LoginUserServlet extends HttpServlet {
	UserService userService = new UserRegisterServiceImpl();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
//		useremail : uname,
//		password : upass
		String message = userService.validateuserlogin(request.getParameter("useremail"),request.getParameter("password"));
		//System.out.println("user email : "+request.getParameter("useremail"));
		//System.out.println("user password : "+request.getParameter("password"));
		System.out.println(message);
		response.setContentType("text/plain");
		response.getWriter().write(message);
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		System.out.println("Login post called");
		String email  =  request.getParameter("email");
		String password = request.getParameter("password");
		
		User user = userService.authenticate(email,password);
		
		if(null!=user)
		{
			//request.setAttribute("user",user);
			HttpSession httpSession = request.getSession(true);
			httpSession.setAttribute("userlogin", user);
			if(user.getUsertype().equalsIgnoreCase("A"))
			{
				response.sendRedirect("admindashboard.jsp");
			}
			else
			{
			response.sendRedirect("index.jsp");
			}
//			RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
//			System.out.println("session created");
//			dispatcher.forward(request, response);
		}
		else
		{
			response.getWriter().append("Invalid Creditentials");
			//System.out.println("Invalid Creditentials");
			RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
			dispatcher.forward(request, response);
			
		}	
	}
}
