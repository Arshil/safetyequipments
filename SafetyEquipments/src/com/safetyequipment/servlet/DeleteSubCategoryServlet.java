package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;

/**
 * Servlet implementation class DeleteSubCategoryServlet
 */
public class DeleteSubCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SubCategoryService subcategoryservice = new SubCategoryServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteSubCategoryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		String message = subcategoryservice.deletesubcategory(Integer.parseInt(request.getParameter("sub_category_id")));
		if (message.equals("Deleted Sub Category successfully")) {
				response.sendRedirect("SubCategoryListServlet");
		} else {
			System.out.println(message);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
	}

}
