package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.CartService;
import com.safetyequipment.service.impl.CartServiceImpl;

/**
 * Servlet implementation class UpdateCartQuantityServlet
 */
public class UpdateCartQuantityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       CartService cartService = new  CartServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCartQuantityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String greetings = "Hello ";
		String message = cartService.upadteproductquantity(Integer.parseInt(request.getParameter("userID").trim()),Integer.parseInt(request.getParameter("productID").trim()),Integer.parseInt(request.getParameter("productQTY").trim()));
		System.out.println(message);
		response.setContentType("text/plain");
		response.getWriter().write(greetings);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
