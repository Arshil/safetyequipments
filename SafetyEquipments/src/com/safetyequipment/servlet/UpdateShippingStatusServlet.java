package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Payment;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.service.PaymentService;
import com.safetyequipment.service.ShippingService;
import com.safetyequipment.service.impl.PaymentServiceImpl;
import com.safetyequipment.service.impl.ShippingServiceImpl;

/**
 * Servlet implementation class UpdateShippingStatusServlet
 */
public class UpdateShippingStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     ShippingService shippingService =  new ShippingServiceImpl(); 
     PaymentService paymentService = new PaymentServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateShippingStatusServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Shipping shipping = shippingService.selecedShipping(request.getParameter("orderkey"));
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editshippingstatus.jsp");
		request.setAttribute("shipping",shipping);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Shipping shipping  = new Shipping();
		//orderid paymentstatus shippingstatus
		shipping.setOrderkey(request.getParameter("orderid"));
		shipping.setPaymentstutus(request.getParameter("paymentstatus"));
		shipping.setShippingstatus(request.getParameter("shippingstatus"));
		if((request.getParameter("paymentpaid").equals("pending"))&&(request.getParameter("paymentstatus").equals("completed")))
		{
			Payment payment   = new Payment();
			payment.setCurrency("INR");
			payment.setOrderid(request.getParameter("orderid"));
			payment.setPaymentmode("COD");
			payment.setStatus("TXN_SUCCESS");
			payment.setTxnamount(Double.parseDouble(request.getParameter("orderamount")));
			payment.setTxnid(request.getParameter("orderid").substring(5));
			System.out.println("txn id : "+payment.getTxnid());
			String insertedPayment  =  paymentService.registerCODPayment(payment);
			if(insertedPayment.equals("success"))
			{
				String message = shippingService.updateShippingStatus(shipping);
				response.sendRedirect("AllOrderListServlet");
			}
			
			//orderamount
		}
		else
		{
			String message = shippingService.updateShippingStatus(shipping);
			response.sendRedirect("AllOrderListServlet");
		}
		
	}

}
