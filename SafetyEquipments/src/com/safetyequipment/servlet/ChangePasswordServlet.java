package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class ChangePasswordServlet
 */
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserRegisterServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangePasswordServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		// confirmnewpassword,newpassword,userid,currentpassword
		System.out.println("do post called in changepasswordservlet");
		String currentpassword = request.getParameter("currentpassword");

		System.out.println(currentpassword);
		System.out.println("user id : " + Integer.parseInt(request.getParameter("userid")));
		String ans = userService.selecteduserpassword(Integer.parseInt(request.getParameter("userid")));
		if (currentpassword.equals(ans)) {

			String message = userService.userpassswordchange(Integer.parseInt(request.getParameter("userid")),
					request.getParameter("confirmnewpassword"));
			if (message.equals("Password Changed Successfully")) {
				response.sendRedirect("index.jsp");
				// request.setAttribute("userid",request.getParameter("userid"));
			} else {
				// System.out.println("Sorry you can't change your password.");
				response.sendRedirect("index.jsp");
			}

			// System.out.println("true");
			// request.setAttribute("userid",Integer.parseInt(request.getParameter("userid")));
			// RequestDispatcher requestDispatcher =
			// request.getRequestDispatcher("newpassword.jsp");
			// requestDispatcher.forward(request, response);
		} else {
			// request.setAttribute("userid",Integer.parseInt(request.getParameter("userid")));

			response.getWriter()
					.append("Sorry you have entered invalid current password.Please enter valid cureent password.");
		}
		// System.out.println("id :"+id);
		// System.out.println("current Password :"+currentpassword);
	}

}
