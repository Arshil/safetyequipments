package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class CheckUserRegistrationServlet
 */
public class CheckUserRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     UserService userService = new   UserRegisterServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckUserRegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String useremailid = request.getParameter("useremail");
		
		System.out.println("user email "+useremailid);
		if(useremailid.equals(""))
		{
			System.out.println("please enter email id");
			response.setContentType("text/plain");
			response.getWriter().write("please enter email id");
		}
		else
		{
		String message = userService.validateuserregistrationEmail(useremailid);
		
		System.out.println(message);
		response.setContentType("text/plain");
		response.getWriter().write(message);
		}
//		if(message.equals("Email id is already registered.Please enter unregistered and valid email id"))
//		{
//			response.setContentType("text/plain");
//			response.getWriter().write(message);
//		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
