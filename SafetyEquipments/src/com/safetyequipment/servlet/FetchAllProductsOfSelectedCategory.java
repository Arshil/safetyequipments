package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Product;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.impl.ProductServiceImpl;

/**
 * Servlet implementation class FetchAllProductsOfSelectedCategory
 */
public class FetchAllProductsOfSelectedCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProductService productService = new ProductServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FetchAllProductsOfSelectedCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		List<Product> productsofselectedcategory = productService.getProductsOfSelectedCategory(request.getParameter("category_id"));
		
	}

}
