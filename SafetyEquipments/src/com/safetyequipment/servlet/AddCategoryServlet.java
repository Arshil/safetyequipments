package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.service.impl.CategoryServiceImpl;
import javax.servlet.http.Part;

/**
 * Servlet implementation class AddCategoryServlet
 */
public class AddCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	CategoryService categoryService = new CategoryServiceImpl();
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public AddCategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
	
		System.out.println("dopost called");
		
		InputStream is= null;
		Category category = new Category();
		
		System.out.println("category name " + request.getParameter("categoryname"));
		System.out.println("category descr  " +request.getParameter("categorydescription"));
		
		
		
		category.setCategoryname(request.getParameter("categoryname"));		
		category.setDescription(request.getParameter("categorydescription"));
		Part image = request.getPart("categoryimage");
	
		if(null!=image)
		{
			is=image.getInputStream(); 
			category.setCategoryimage(is);
		}
	
		String message = categoryService.addcategory(category);
		System.out.println(message);
		if(message.equals("category added"))
		{
			response.sendRedirect("GetCategoryListServlet");
		}
		else
		{
			response.getWriter().append("category not added");
	
		}


	
	}

}
