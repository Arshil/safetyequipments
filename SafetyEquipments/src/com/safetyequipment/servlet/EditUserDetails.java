package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;

import com.safetyequipment.beans.User;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class EditUserDetails
 */

public class EditUserDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	User user = new User();
	UserService userService = new UserRegisterServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		HttpSession  httpSession = request.getSession(false);
		User user = new User();
		
		System.out.println("User name in  edit " + request.getParameter("firstname"));
		String imagestring = ((User)httpSession.getAttribute("userlogin")).getImagestring(); 
		System.out.println("User name in  edit " + request.getParameter("firstname"));
		InputStream is = null;
		System.out.println("User id in  edit " + request.getParameter("userid"));
		user.setUserid(Integer.parseInt(request.getParameter("userid")));
		user.setFirstname(request.getParameter("firstname"));
		System.out.println("fname " + request.getParameter("firstname"));
		user.setLastname(request.getParameter("lastname"));
		System.out.println("lname:" + request.getParameter("lastname"));
		user.setEmailid(request.getParameter("emailid")); 
		System.out.println("email:" + request.getParameter("emailid"));
		user.setGender(request.getParameter("gender"));
		System.out.println("gender:" + request.getParameter("gender"));
		user.setMobileno(request.getParameter("mobile"));
		System.out.println("mobile:" + request.getParameter("mobile"));
		user.setAddress(request.getParameter("address"));
		System.out.println("address:" + request.getParameter("address"));
		user.setPincode(request.getParameter("pincode"));
		System.out.println("pincode:" + request.getParameter("pincode"));
		Part image = request.getPart("profileimage");
		Part imagecopy = request.getPart("profileimage");

		
		if (null!=image && image.getSize() > 0 ) {
			System.out.println("size of image:" + image.getSize());
			is = image.getInputStream();
			user.setUserimage(imagecopy.getInputStream());
			try(ByteArrayOutputStream buffer = new ByteArrayOutputStream()){
				int nRead;
				byte[] data = new byte[16384];
				while((nRead=is.read(data,0,data.length))!=-1)
				{
					buffer.write(data,0,nRead);
				}
				user.setImagestring(Base64.getEncoder().encodeToString(buffer.toByteArray()));
			}
		}
		else
		{
			user.setImagestring(imagestring);
		}
		String message = userService.modifyUserDetails(user);

		httpSession.setAttribute("userlogin", user);
		
		System.out.println(message);
		if(message.equals("update success"))
		{
			response.sendRedirect("UserLogOutServlet");
		}
		else
		{
			response.getWriter().append("updation fail");
		}


	}

}
