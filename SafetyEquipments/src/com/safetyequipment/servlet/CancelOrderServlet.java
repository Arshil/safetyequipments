package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.beans.User;
import com.safetyequipment.service.MyOrderService;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.ShippingService;
import com.safetyequipment.service.impl.MyOrderServiceImpl;
import com.safetyequipment.service.impl.ProductServiceImpl;
import com.safetyequipment.service.impl.ShippingServiceImpl;

/**
 * Servlet implementation class CancelOrderServlet
 */
public class CancelOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    ShippingService shippingService = new ShippingServiceImpl();    
    MyOrderService myOrderService  = new MyOrderServiceImpl(); 
    ProductService productService = new ProductServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelOrderServlet() {
        super();
        // TODO Auto-generated constructor stubz
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession httpSessionuser = request.getSession(false);
		User userobj = (User) httpSessionuser.getAttribute("userlogin"); 
		MyOrder myOrder = myOrderService.selectedMyOrder(request.getParameter("orderkey"));
		//products quantity added successfully
		String addedQuantity = productService.addProductQuantity(myOrder.getOrderdetailslist());
		if(addedQuantity.equals("products quantity added successfully"))
		{
			 String message = shippingService.cancelOrder(myOrder.getShipping(), userobj);
			 if(message.equals("order has been cancelled"))
			 {
				 response.sendRedirect("MyOrderListServlet");
			 }
			 else
			 {
				 System.out.println(message);
			 }
		}
		else
		{
			System.out.println(addedQuantity);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
