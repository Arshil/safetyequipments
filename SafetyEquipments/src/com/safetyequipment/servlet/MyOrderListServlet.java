package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.User;
import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.service.MyOrderService;
import com.safetyequipment.service.impl.MyOrderServiceImpl;

/**
 * Servlet implementation class MyOrderListServlet
 */
public class MyOrderListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	MyOrderService myOrderService = new MyOrderServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyOrderListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession httpSessionuser = request.getSession(false);
		User userobj = (User) httpSessionuser.getAttribute("userlogin");
		
		List<MyOrder> myOrderList = new ArrayList<MyOrder>();
			myOrderList = myOrderService.myOrderList(userobj.getUserid());
		request.setAttribute("myorderlist", myOrderList);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("myorder.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("MYORDERLISTSERVLET CALLED");
		System.out.println("MyOrderListServlet do post called");
		HttpSession httpSessionuser = request.getSession(false);
		User userobj = (User) httpSessionuser.getAttribute("userlogin");
		
		List<MyOrder> myOrderList = new ArrayList<MyOrder>();
//		int userid = Integer.parseInt(request.getParameter("userid"));
//		if (request.getParameter("userid") != null) {
//			myOrderList = myOrderService.myOrderList(userid);
//		} else {
			myOrderList = myOrderService.myOrderList(userobj.getUserid());
		//}

		request.setAttribute("myorderlist", myOrderList);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("myorder.jsp");
		requestDispatcher.forward(request, response);

	}

}
