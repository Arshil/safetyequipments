package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.ShippingService;
import com.safetyequipment.service.impl.ShippingServiceImpl;

/**
 * Servlet implementation class TotalSellingServlet
 */
public class TotalSellingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       ShippingService shippingService = new ShippingServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TotalSellingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		double selling[] = shippingService.totalSelling();
		for (int i = 0; i < selling.length; i++) {
			System.out.println(" Month "+(i+1)+" : "+selling[i]);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
