package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Company;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.impl.CompanyServiceImpl;

/**
 * Servlet implementation class UpdateCompanyDetailsServlet
 */
public class UpdateCompanyDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	CompanyService companyService = new CompanyServiceImpl();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCompanyDetailsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	
		Company company =  companyService.getselectedcompany(request.getParameter("company_id"));
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editCompany.jsp");
		request.setAttribute("company", company);
		requestDispatcher.forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
