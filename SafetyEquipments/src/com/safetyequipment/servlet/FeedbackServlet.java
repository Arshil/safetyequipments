package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.Feedback;
import com.safetyequipment.beans.User;
import com.safetyequipment.service.FeedbackService;
import com.safetyequipment.service.impl.FeedbackServiceImpl;

/**
 * Servlet implementation class FeedbackServlet
 */
public class FeedbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	FeedbackService feedbackService = new FeedbackServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeedbackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Feedback feedback = new Feedback();
		
		HttpSession httpSession = request.getSession(false);
		User user = (User)httpSession.getAttribute("userlogin"); 
		feedback.setDescription(request.getParameter("feedback"));
		feedback.setUseremailid(user.getEmailid());
		feedback.setRatings((Integer.parseInt(request.getParameter("ratings"))));
		feedback.setUserid(user.getUserid());
		String message  = feedbackService.insertFeedback(feedback);
		response.getWriter().append(message);			
	}
}

