package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;

/**
 * Servlet implementation class DeleteComplaintServlet
 */
public class DeleteComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    ComplaintService complaintService = new   ComplaintServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String  message = complaintService.deleteComplaint(Integer.parseInt(request.getParameter("complaintid")));
		if(message.equals("complaint deleted successfully"))
		{
			response.sendRedirect("ComplaintListServlet");
		}
		else
		{
			System.out.println(message);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
