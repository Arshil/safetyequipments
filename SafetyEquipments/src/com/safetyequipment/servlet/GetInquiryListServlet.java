package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Contact;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class GetInquiryListServlet
 */
public class GetInquiryListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      UserService userService = new UserRegisterServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetInquiryListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Contact> allinqurieslist = userService.allInquiriesList();
		//System.out.println("size : "+allinqurieslist.size());
		RequestDispatcher requestDispatcher  = request.getRequestDispatcher("viewinquiries.jsp");
		request.setAttribute("allinqurieslist",allinqurieslist);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
