package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.safetyequipment.beans.Company;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.impl.CompanyServiceImpl;

/**
 * Servlet implementation class GetCompanyListOfSelectedProduct
 */
public class GetCompanyListOfSelectedProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
      CompanyService companyService = new   CompanyServiceImpl();
      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCompanyListOfSelectedProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		 List<Company>  companylist  = companyService.getCompanyListOfSelectedProduct(Integer.parseInt(request.getParameter("productId")));
		 System.out.println("Size of Product List = "+companylist.size());
			Gson gson = new Gson();
			gson.toJson(companylist);
			String jsonString = gson.toJson(companylist);
			System.out.println("JSonString : " + jsonString);
			response.getWriter().append(jsonString);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
