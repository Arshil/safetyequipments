package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Product;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.impl.CategoryServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.ProductServiceImpl;

/**
 * Servlet implementation class GetProductsOfSelectedCategoryServlet
 */
public class GetProductsOfSelectedCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ProductService productService = new  ProductServiceImpl();

	GetCategoryService categoryService = new GetCategoryServiceImpl();		
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetProductsOfSelectedCategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	
		System.out.println("doget getpro called");
		List<Product> productlist = productService.getProductsOfSelectedCategory(request.getParameter("category_id"));
		List<Category> categories = categoryService.getcategorylist();
				
		System.out.println("pro list:" + productlist.size());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("shop.jsp");
		
		request.setAttribute("productlist", productlist);
		request.setAttribute("categorylist", categories);
		
		requestDispatcher.forward(request, response);			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
				
	}

}
