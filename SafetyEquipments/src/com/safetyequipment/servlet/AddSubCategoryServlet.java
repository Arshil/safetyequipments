package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;
import javax.servlet.http.Part;

/**
 * Servlet implementation class AddSubCategoryServlet
 */
public class AddSubCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SubCategoryService subcategoryservice = new SubCategoryServiceImpl();
	GetCategoryService categoryservice = new GetCategoryServiceImpl(); 
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddSubCategoryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Category> categorylist = categoryservice.getcategorylist(); 
		for (int i = 0; i < categorylist.size(); i++) {
			System.out.println("id :"+categorylist.get(i).getCategoryid());
		}
		request.setAttribute("categorylist",categorylist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("addsubcategory.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stubcategoryoption
		
		// doGet(request, response);
		InputStream is = null;
		SubCategory subcategory = new SubCategory();
		System.out.println("servlet level category id :" + request.getParameter("categoryoption"));
		
		subcategory.setCategoryid(Integer.parseInt(request.getParameter("categoryoption")));
		subcategory.setSubcategoryname(request.getParameter("subcategoryname"));
		subcategory.setDescription(request.getParameter("subcategorydescription"));
		System.out.println("category option : " + subcategory.getCategoryid());
		Part image = request.getPart("subcategoryimage");
		System.out.println("size of image:" + image.getSize());
		if (image.getSize() > 0) {
			is = image.getInputStream();
			subcategory.setSubcategoryimage(is);
		}
		String message = subcategoryservice.insertSubcayegory(subcategory);
		if (message.equals("SubCategory added Successfully")) {
			System.out.println(message);
			response.sendRedirect("SubCategoryListServlet");
		} else {
			System.out.println("Can't insert subcategory.");
		}
	}

}
