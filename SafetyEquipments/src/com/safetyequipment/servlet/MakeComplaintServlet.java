package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;

/**
 * Servlet implementation class MakeComplaintServlet
 */
public class MakeComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       ComplaintService complaintService = new ComplaintServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MakeComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Complaint complaint = new Complaint();
		complaint.setComplaintid(Integer.parseInt(request.getParameter("complaintoption")));
		complaint.setUserid(Integer.parseInt(request.getParameter("userid")));
		complaint.setOrderkey(request.getParameter("orderid"));
		complaint.setProductid(Integer.parseInt(request.getParameter("productoption")));
		complaint.setUseremail(request.getParameter("useremail"));
		if(!(request.getParameter("description").equals("")))
		{
			complaint.setDescription(request.getParameter("description"));
		}
		String message = complaintService.registerUserComplaint(complaint);
		response.sendRedirect("makecomplaint.jsp");
	}

}
