package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.safetyequipment.beans.User;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class UpdateUserServlet
 */
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     UserService userService = new UserRegisterServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		User user = userService.selecteduser(Integer.parseInt(request.getParameter("user_id")));
		request.setAttribute("user",user);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editUser.jsp");
		requestDispatcher.forward(request,response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		 
		InputStream is = null;
		User user = new User();
		user.setUserid(Integer.parseInt(request.getParameter("userid")));
		user.setFirstname(request.getParameter("userfirstname"));
		user.setLastname(request.getParameter("userlastname"));
		user.setMobileno(request.getParameter("usermobileno"));
		user.setAddress(request.getParameter("useraddress"));
		user.setPincode(request.getParameter("userpincode"));
		Part image = request.getPart("userprofileimage");
		if (image.getSize() > 0) {
			is = image.getInputStream();
			user.setUserimage(is);
		}
		String message = userService.edituser(user);
		if(message.equals("User Updated Successfully"))
		{
			response.sendRedirect("UserListServlet");
		}
		else
		{
			System.out.println(message);
		}
	}

}
