package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.User;
import com.safetyequipment.service.CartService;
import com.safetyequipment.service.impl.CartServiceImpl;

/**
 * Servlet implementation class UserCartListServlet
 */
public class UserCartListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     CartService cartService = new CartServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCartListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession httpsession = request.getSession(false);
		User user = (User)httpsession.getAttribute("userlogin"); 
		user.setCartlist(cartService.selectedusercartlist(user.getUserid()));
		System.out.println("User Cart List size : "+user.getCartlist().size());
		request.setAttribute("usercartlist",user.getCartlist());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("cart.jsp") ;
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
