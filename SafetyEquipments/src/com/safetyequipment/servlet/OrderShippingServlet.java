package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.Order;
import com.safetyequipment.beans.Payment;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.beans.User;
import com.safetyequipment.dao.ShippingDao;
import com.safetyequipment.dao.impl.ShippingDaoImpl;
import com.safetyequipment.service.CartService;
import com.safetyequipment.service.OrderService;
import com.safetyequipment.service.PaymentService;
import com.safetyequipment.service.ShippingService;
import com.safetyequipment.service.impl.CartServiceImpl;
import com.safetyequipment.service.impl.OrderServiceImpl;
import com.safetyequipment.service.impl.PaymentServiceImpl;
import com.safetyequipment.service.impl.ShippingServiceImpl;

/**
 * Servlet implementation class OrderShippingServlet
 */
public class OrderShippingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PaymentService paymentService = new PaymentServiceImpl();
	OrderService orderService = new OrderServiceImpl();
	CartService cartService = new CartServiceImpl();
	ShippingService shippingService = new ShippingServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OrderShippingServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*
		* saddress : address, 
		 * orderId : orderid, 
		 * amount : amt, userId : uid,
		 *  items : nop,
		 *   ptype : radioValue
		 */
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		if (request.getParameter("ptype").equals("pod")) {

			Shipping shipping = new Shipping();
			shipping.setAddress(request.getParameter("saddress"));
			shipping.setOrderkey(request.getParameter("orderId"));
			shipping.setAmount(Double.parseDouble(request.getParameter("amount")));
			shipping.setUserid(Integer.parseInt(request.getParameter("userId")));
			shipping.setItems(Integer.parseInt(request.getParameter("items")));
			String message = shippingService.addShippingOrder(shipping);
			System.out.println(message);
			if (message.equals("Order inserted for shipping successfully")) {
				Order order = new Order();
				order.setOrderkey(request.getParameter("orderId"));
				order.setUserid(Integer.parseInt(request.getParameter("userId")));
				order.setTransaction_amount(Double.parseDouble(request.getParameter("amount")));
				// int userid = user.getUserid();
				// System.out.println("step 4");
				int ordersummeryid = orderService.registerOrderDetails(order);
				System.out.println("order summery id : " + ordersummeryid);
				// System.out.println("step 5");
				List<Cart> cartlist = cartService.selectedusercartlist(Integer.parseInt(request.getParameter("userId")));
				if (cartlist.size() > 0) {
					// System.out.println("step 6");
					int arr[] = orderService.saveOrderDetails(cartlist, ordersummeryid);
					// System.out.println("step 7");
					if (arr.length > 0) {
						// System.out.println("step 8");
						String deletedCartmsg = cartService
								.removeAllCartProducts(Integer.parseInt(request.getParameter("userId")));
						System.out.println(deletedCartmsg);
						response.setContentType("text/plain");
						response.getWriter().write(deletedCartmsg);
					}
				} else {

				}

			} else {

			}

		} else {
			Shipping shipping = new Shipping();
			shipping.setAddress(request.getParameter("saddress"));
			shipping.setOrderkey(request.getParameter("orderId"));
			shipping.setAmount(Double.parseDouble(request.getParameter("amount")));
			shipping.setUserid(Integer.parseInt(request.getParameter("userId")));
			shipping.setItems(Integer.parseInt(request.getParameter("items")));
			String message = shippingService.addShippingOrder(shipping);
			// System.out.println("address : "+request.getParameter("saddress"));
			// System.out.println("orderid : "+request.getParameter("orderId"));
			System.out.println(message);
			response.setContentType("text/plain");
			response.getWriter().write(message);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
	}

}
