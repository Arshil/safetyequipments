package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Order;
import com.safetyequipment.service.OrderService;
import com.safetyequipment.service.impl.OrderServiceImpl;

/**
 * Servlet implementation class OrderRegisterServlet
 */
public class OrderRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	OrderService orderService = new OrderServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OrderRegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
//		date : d,
//        userId : userid,
//        Txnamt : txnamt
		Date date = new Date();
		// System.out.println("user id : "+);
		// System.out.println("txn amt : "+Integer.parseInt());
		// System.out.println("date : "+date.toString());
		Order order = new Order();
		order.setTransaction_amount(Long.parseLong(request.getParameter("Txnamt")));
		order.setUserid(Integer.parseInt(request.getParameter("userId")));
		//order.setOrderid(request.getParameter("orderId"));
		int oid = orderService.registerOrderDetails(order);
		System.out.println(String.valueOf(oid));
		// String greetings = "Hello " ;
		response.setContentType("text/plain");
		response.getWriter().write(String.valueOf(oid));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);

	}

}
