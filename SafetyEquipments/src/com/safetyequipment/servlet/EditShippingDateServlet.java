package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Shipping;
import com.safetyequipment.service.ShippingService;
import com.safetyequipment.service.impl.ShippingServiceImpl;

/**
 * Servlet implementation class EditShippingDateServlet
 */
public class EditShippingDateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ShippingService  shippingService  = new ShippingServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditShippingDateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Shipping shipping = shippingService.selecedShipping(request.getParameter("orderkey"));
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editshipping.jsp");
		request.setAttribute("shipping",shipping);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		Shipping shipping  = new Shipping();
		shipping.setOrderkey(request.getParameter("orderid"));
		shipping.setShippingdate(request.getParameter("shippingdate"));
		System.out.println("shipping date : "+shipping.getShippingdate());
		String message = shippingService.editShipping(shipping);
		if(message.equals("shipping date edited successfully"))
		{
			response.sendRedirect("AllOrderListServlet");
		}
		else
		{
			System.out.println(message);
		}
	}

}
