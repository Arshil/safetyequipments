package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;

/**
 * Servlet implementation class UpdateComplaintServlet
 */
public class UpdateComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       ComplaintService complaintService = new ComplaintServiceImpl();
       GetCategoryService categoryService = new GetCategoryServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Complaint complaint = complaintService.selectedComplaint(Integer.parseInt(request.getParameter("complaintid")));
		request.setAttribute("complaint",complaint);
		List<Category> categorylist = categoryService.getcategorylist();
		request.setAttribute("categorylist", categorylist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editcomplaint.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Complaint complaint = new  Complaint();
		complaint.setCategoryid(Integer.parseInt(request.getParameter("categoryoption")));
		complaint.setSubcategoryid(Integer.parseInt(request.getParameter("subcategoryoption")));
		complaint.setProductid(Integer.parseInt(request.getParameter("productoption")));
		complaint.setCompanyid(Integer.parseInt(request.getParameter("companyoption")));
		complaint.setName(request.getParameter("complaintname"));
		//complaint.setDescription(request.getParameter("complaintdescription"));
		complaint.setComplaintid(Integer.parseInt(request.getParameter("complaintid")));
		System.out.println(Integer.parseInt(request.getParameter("complaintid")));
		String message = complaintService.updateComplaint(complaint);
		if(message.equals("complaint updated successfully"))
		{
			response.sendRedirect("ComplaintListServlet");
			//response.sendRedirect("");
		}
		else
		{
			System.out.println(message);
		}
	}

}
