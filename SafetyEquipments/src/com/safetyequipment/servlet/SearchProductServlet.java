package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Product;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.impl.CategoryServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.ProductServiceImpl;

/**
 * Servlet implementation class SearchProductServlet
 */
public class SearchProductServlet extends HttpServlet {
	GetCategoryService categoryService = new GetCategoryServiceImpl();
	ProductService productService = new ProductServiceImpl();
	ArrayList<Product> productslist;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println(request.getParameter("search-product"));
		productslist =  productService.searchproduct(request.getParameter("search-product"));
		System.out.println("prosize" + productslist.size());
		
		//List<Product> productlist = productService.getProductsOfSelectedCategory(request.getParameter("category_id"));
		List<Category> categories = categoryService.getcategorylist();
				
		//System.out.println("pro list:" + productlist.size());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("showsearchedproduct.jsp");
		
		request.setAttribute("productlist", productslist);
		request.setAttribute("categorylist", categories);
		
		requestDispatcher.forward(request, response);		
	}

}
