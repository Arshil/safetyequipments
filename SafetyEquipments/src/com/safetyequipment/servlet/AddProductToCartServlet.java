package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.User;
import com.safetyequipment.service.CartService;
import com.safetyequipment.service.impl.CartServiceImpl;

/**
 * Servlet implementation class AddProductToCartServlet
 */
public class AddProductToCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CartService cartService = new CartServiceImpl();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProductToCartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Cart cart = new Cart();	
		HttpSession httpSession = request.getSession(false); 
		User user = (User)httpSession.getAttribute("userlogin");
		cart.setProductid(Integer.parseInt(request.getParameter("productId")));
		user.setCartlist(cartService.selectedusercartlist(user.getUserid()));
		System.out.println("User Cart List size : "+user.getCartlist().size());	
		cart.setUserid(user.getUserid());
		System.out.println("OPERATION :" + request.getParameter("operation"));
		if(request.getParameter("operation").equalsIgnoreCase("Add"))
		{
			String message = cartService.addProductToCart(cart);	
		}
		else{
			String message = cartService.removeProductFromCart(user.getUserid(),cart.getProductid());	
		}
		
		
	}

}
