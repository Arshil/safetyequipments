package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.safetyequipment.beans.Category;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.service.impl.CategoryServiceImpl;

/**
 * Servlet implementation class UpdateCategory
 */
public class UpdateCategory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	CategoryService categoryService = new CategoryServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCategory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
		Category category =  categoryService.getselectedcategory(request.getParameter("category_id"));
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editCategory.jsp");
		//System.out.println("category id :"+category.getCategoryid());
		request.setAttribute("category", category);
		requestDispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);	
		System.out.println("update cdet dopost called");
		Category category =new Category();
		InputStream is = null;
		
		category.setCategoryid(Integer.parseInt(request.getParameter("categoryid")));
		category.setCategoryname(request.getParameter("categoryname"));
		category.setDescription(request.getParameter("categorydescription"));
		
		
		System.out.println("c id :" + request.getParameter("categoryid"));
		System.out.println("cname " + request.getParameter("categoryname"));
		System.out.println("c desc " + request.getParameter("categorydescription"));
		
	
		Part image = request.getPart("categoryimage");
	
		System.out.println("size of image:" + image.getSize());
		if (image.getSize() > 0) 
		{
			is = image.getInputStream();
			category.setCategoryimage(is);
		}
		 String msg = categoryService.updatecategory(category);
		 if(msg.equals("Success"))
		 {
			 //response.getWriter().append("updated Success");
			 response.sendRedirect("GetCategoryListServlet");
			 
		 }
		 else
		 {
			 response.getWriter().append("updated fail");
			 	 
		 }

	}

}
