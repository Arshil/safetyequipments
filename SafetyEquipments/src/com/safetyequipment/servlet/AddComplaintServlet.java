package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Company;
import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.impl.CompanyServiceImpl;
import com.safetyequipment.service.impl.ComplaintServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;

/**
 * Servlet implementation class AddComplaintServlet
 */
public class AddComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ComplaintService complaintService  = new ComplaintServiceImpl();
	GetCategoryService categoryService = new GetCategoryServiceImpl();
     
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Category> categorylist = categoryService.getcategorylist();
		request.setAttribute("categorylist", categorylist);
//		List<Company> companylist = companyService.selectcompanylist();
//		request.setAttribute("companylist",companylist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("addcomplaint.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//doGet(request, response);
		Complaint complaint = new  Complaint();
		complaint.setCategoryid(Integer.parseInt(request.getParameter("categoryoption")));
		complaint.setSubcategoryid(Integer.parseInt(request.getParameter("subcategoryoption")));
		complaint.setProductid(Integer.parseInt(request.getParameter("productoption")));
		complaint.setCompanyid(Integer.parseInt(request.getParameter("companyoption")));
		complaint.setName(request.getParameter("complaintname"));
		String message = complaintService.addComplaint(complaint);
		if(message.equals("complaint inserted successfully"))
		{
			response.sendRedirect("ComplaintListServlet");
			//response.sendRedirect("");
		}
		else
		{
			System.out.println(message);
		}
	}

}
