package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.service.MyOrderService;
import com.safetyequipment.service.impl.MyOrderServiceImpl;

/**
 * Servlet implementation class AllOrderListServlet
 */
public class AllOrderListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      MyOrderService myOrderService = new MyOrderServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllOrderListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//System.out.println("called 1");
		List<MyOrder> allOrderList = myOrderService.allOrderList();
		System.out.println("all order list : "+allOrderList.size());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("viewallorder.jsp");
		request.setAttribute("allOrderList",allOrderList);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
	}

}
