package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Company;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.CompanyServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.ProductServiceImpl;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;

/**
 * Servlet implementation class AddProductServlet
 */
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProductService productservice = new ProductServiceImpl();
	GetCategoryService categoryservice = new GetCategoryServiceImpl(); 
	 SubCategoryService subcategoryservice = new SubCategoryServiceImpl();
	CompanyService companyservice = new  CompanyServiceImpl();
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Category> categorylist = categoryservice.getcategorylist();
		List<SubCategory> subcategorylist = subcategoryservice.selectedSubCategory();
		List<Company> companylist = companyservice.selectcompanylist();
		request.setAttribute("categorylist",categorylist);
		request.setAttribute("subcategorylist",subcategorylist);
		request.setAttribute("companylist",companylist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("addproduct.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		// productprice,productdescription,productimage,productname,companyoption,subcategoryoption,categoryoption
		InputStream is = null;
		Product product = new Product();
		product.setProductname(request.getParameter("productname"));
		product.setDescription(request.getParameter("productdescription"));
		System.out.println(request.getParameter("categoryoption"));
		product.setCategoryid(Integer.parseInt(request.getParameter("categoryoption")));
		System.out.println(request.getParameter("companyoption"));
		product.setCompanyid(Integer.parseInt(request.getParameter("companyoption")));
		System.out.println(request.getParameter("subcategoryoption"));
		product.setSubcategoryid(Integer.parseInt(request.getParameter("subcategoryoption")));
		product.setQuantity(Integer.parseInt(request.getParameter("productquantity")));
		product.setProductprice(Integer.parseInt(request.getParameter("productprice")));
		product.setAboutproduct(request.getParameter("aboutproduct"));
		Part image = request.getPart("productimage");
		if (image.getSize() > 0) {
			is = image.getInputStream();
			product.setProductimage(is);
		}
		String message = productservice.insertProduct(product);
		if (message.equals("Product added Successfully")) {
			//RequestDispatcher requestDispatcher = request.getRequestDispatcher("addproduct.jsp");
			//requestDispatcher.forward(request, response);
			response.sendRedirect("ProductListServlet");
		} else {
			response.getWriter().append(message);
		}
	}

}
