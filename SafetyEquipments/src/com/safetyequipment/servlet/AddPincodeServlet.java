package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.User;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class AddPincodeServlet
 */
public class AddPincodeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService userService = new UserRegisterServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddPincodeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		User user = new User();
		String message = "";  
		user.setUserid(Integer.parseInt(request.getParameter("userid")));
		System.out.println("user id :" + user.getUserid());
		// System.out.println("value of call in servlet :
		// "+request.getParameter("callfor"));
		user.setPincode(request.getParameter("pincode"));
		message = userService.updateUserPincode(user);
		System.out.println("mesage of pincode :"+message);
		response.setContentType("text/plain");
		response.getWriter().write(message);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
