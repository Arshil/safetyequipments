package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;

/**
 * Servlet implementation class AllUserComplaints
 */
public class AllUserComplaints extends HttpServlet {
	private static final long serialVersionUID = 1L;
     ComplaintService complaintService = new ComplaintServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllUserComplaints() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Complaint> complaintlist = complaintService.allUserComplaints();
		request.setAttribute("complaintlist", complaintlist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("viewallusercomplaints.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
