package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Feedback;
import com.safetyequipment.service.FeedbackService;
import com.safetyequipment.service.impl.FeedbackServiceImpl;

/**
 * Servlet implementation class FeedbackListServlet
 */
public class FeedbackListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       FeedbackService feedbackService = new  FeedbackServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeedbackListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("do get called");
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Feedback> feedbacklist = feedbackService.allFeedbackList();
		System.out.println("list received");
		request.setAttribute("feedbacklist",feedbacklist);
		System.out.println("size of feedbacklist : "+feedbacklist.size());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("viewfeedback.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
