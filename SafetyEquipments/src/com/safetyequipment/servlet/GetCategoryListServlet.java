package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;

/**
 * Servlet implementation class GetCategoryListServlet
 */
public class GetCategoryListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	Category category = new Category();
	GetCategoryService categoryService = new GetCategoryServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCategoryListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		ArrayList<Category> categorylist = categoryService.getcategorylist();
		System.out.println("LIST SIZE : " + categorylist.size());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("viewcategorylist.jsp");
		request.setAttribute("categorylist", categorylist);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
