package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;

/**
 * Servlet implementation class GetComplaintListOfSelectedProduct
 */
public class GetComplaintListOfSelectedProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ComplaintService complaintService = new ComplaintServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetComplaintListOfSelectedProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		int pid = Integer.parseInt(request.getParameter("productId"));
		if (pid == 0) {
				response.getWriter().append("select product option");	
		} else {
			List<Complaint> complaintlist = complaintService.selectedProductComplaintList(pid);
			Gson gson = new Gson();
			gson.toJson(complaintlist);
			String jsonString = gson.toJson(complaintlist);
			System.out.println("JSonString : " + jsonString);
			response.getWriter().append(jsonString);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
	}

}
