package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.Shipping;
import com.safetyequipment.service.ShippingService;
import com.safetyequipment.service.impl.ShippingServiceImpl;

/**
 * Servlet implementation class OrderIdValidationServlet
 */
public class OrderIdValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     ShippingService shippingService  = new ShippingServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OrderIdValidationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String orderkey = request.getParameter("orderId");
		int userid = Integer.parseInt(request.getParameter("userId"));
	    response.setContentType("text/plain");
	    Shipping shipping  = shippingService.selectedOrder(orderkey,userid);
	    if(null==shipping)
	    {
	    	response.getWriter().write("Invalid order id");
	    }
	    else 
	    {
	    	if(!(shipping.getPaymentstutus().equals("completed")))
		    {
		    	response.getWriter().write("You can't make complaint because you have not paid payment");
		    }
	    	else if(shipping.getShippingstatus().equals("cancelled"))
		    {
		    	response.getWriter().write("You can't make complaint because your order has been cancelled");
		    }
		    else if(shipping.getShippingstatus().equals("pending"))
		    {
		    	response.getWriter().write("You can't make complaint because your order has not shipped yet");
		    }
		    
		    else
		    {
		    	List<Product> productlist = shippingService.productListForComplaint(shipping);
		    	Gson gson = new Gson();
				gson.toJson(productlist);
				String jsonString = gson.toJson(productlist);
				System.out.println("JSonString : " + jsonString);
				response.getWriter().append(jsonString);
		    }
	    }
//		response.getWriter().write("message received");
	    //invalid order id
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
