package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.service.CartService;
import com.safetyequipment.service.impl.CartServiceImpl;

/**
 * Servlet implementation class DeleteUserCartProduct
 */
public class DeleteUserCartProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CartService cartService  = new CartServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUserCartProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//System.out.println(request.getParameter("cart_id"));
		String message = cartService.deletecartproduct(Integer.parseInt(request.getParameter("cart_id")));
		if(message.equals("Product has been removed from your cart successfully"))
		{
			response.sendRedirect("UserCartListServlet");
		}
		else
		{
			response.getWriter().append("Sorry product can't deleted from your cart.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
