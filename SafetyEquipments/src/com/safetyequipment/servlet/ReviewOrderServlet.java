package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.Cart;
import com.safetyequipment.beans.User;
import com.safetyequipment.service.CartService;
import com.safetyequipment.service.impl.CartServiceImpl;

/**
 * Servlet implementation class ReviewOrderServlet
 */
public class ReviewOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	CartService cartService = new CartServiceImpl();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//response.getWriter().append("Served at: ").append(request.getContextPath());

		HttpSession httpSession = request.getSession(false);
		User user = (User)httpSession.getAttribute("userlogin");
		List<Cart> cartlist = cartService.selectedusercartlist(user.getUserid());
		System.out.println("size of list :"+cartlist.size() );
		request.setAttribute("cartlist",cartlist);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println("do post called");
		List<Cart> cartlist = cartService.selectedusercartlist(Integer.parseInt(request.getParameter("userid")));
		System.out.println("size of list :"+cartlist.size() );
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("revieworder2.jsp"); 
		request.setAttribute("cartlist",cartlist);
		requestDispatcher.forward(request, response);
		//response.getWriter().append("Served at:  userid : "+userid).append(request.getContextPath());
	}

}
