package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;

/**
 * Servlet implementation class EditUserComplaintServlet
 */
public class EditUserComplaintServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     ComplaintService complaintService = new ComplaintServiceImpl();
     //GetCategoryService categoryService = new GetCategoryServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserComplaintServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Complaint complaint = complaintService.selectedUserComplaint(Integer.parseInt(request.getParameter("usercomplaint_id")));
		request.setAttribute("complaint",complaint);
		List<Complaint> productcomplaintlist = complaintService.complaintListSelectedProduct(complaint.getProductid());
		request.setAttribute("productcomplaintlist", productcomplaintlist);
//		List<Category> categorylist = categoryService.getcategorylist();
//		request.setAttribute("categorylist", categorylist);		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editusercomplaint.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Complaint complaint = new Complaint();
		complaint.setUsercomplaintid(Integer.parseInt(request.getParameter("usercomplaintid")));
		System.out.println("user complaint id : "+Integer.parseInt(request.getParameter("usercomplaintid")));
		complaint.setComplaintid(Integer.parseInt(request.getParameter("complaintoption")));
		System.out.println("complaint id : "+Integer.parseInt(request.getParameter("complaintoption")));
		complaint.setDescription(request.getParameter("description"));
		//System.out.println("");
		String message = complaintService.editUserComplaint(complaint);
		if(message.equals("complaint edited successfully"))
		{
			response.sendRedirect("UserComplaintListServlet");
		}
		else
		{
			System.out.println(message);
		}
	}

}
