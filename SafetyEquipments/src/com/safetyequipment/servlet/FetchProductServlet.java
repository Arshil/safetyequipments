package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.User;
import com.safetyequipment.service.CartService;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.impl.CartServiceImpl;
import com.safetyequipment.service.impl.CategoryServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.ProductServiceImpl;

/**
 * Servlet implementation class FetchProductServlet
 */
public class FetchProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    CartService cartService = new CartServiceImpl();
	ProductService productService = new ProductServiceImpl();
    GetCategoryService categoryService = new GetCategoryServiceImpl();
	/**
     * @see HttpServlet#HttpServlet()
     */
    public FetchProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Product> products = 	productService.getProductList(request.getParameter("sub_category_id"));
		List<Category> categories = categoryService.getcategorylist();
		
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("shop.jsp");
		request.setAttribute("productlist", products);
		request.setAttribute("categorylist", categories);
				
//		HttpSession httpsession = request.getSession(false);
//		if(null!=httpsession)
//		{
//	
//			User user = (User)httpsession.getAttribute("userlogin"); 
//			user.setCartlist(cartService.selectedusercartlist(user.getUserid()));
//			System.out.println("In fetch User Cart List size : "+user.getCartlist().size());
//			if(user!=null)
//			{
//				request.setAttribute("usercartlist",user.getCartlist());
//			}	
//		}
//		
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
