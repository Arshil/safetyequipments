package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Company;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.impl.CompanyServiceImpl;

/**
 * Servlet implementation class UpdateCompanyServlet
 */
public class UpdateCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	CompanyService companyService = new CompanyServiceImpl();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Company company =  companyService.getselectedcompany(request.getParameter("company_id"));
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editCompany.jsp");
		request.setAttribute("company", company);
		requestDispatcher.forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		System.out.println("update co. dopost called");
		Company company = new Company();
		

		company.setCompanyid(Integer.parseInt(request.getParameter("companyid")));
		company.setCompanyname(request.getParameter("companyname"));
		company.setCompanycontact(request.getParameter("companycontact"));
		company.setCompanyemail(request.getParameter("companyemailid"));
		company.setCompanyaddress(request.getParameter("companyaddress"));
		
		
		
		
		System.out.println("servlet c id :" + request.getParameter("companyid"));
		System.out.println("cname " + request.getParameter("companyname"));
		System.out.println("c add " + request.getParameter("companyaddress"));
		
	
		 String msg = companyService.updatecompany(company);
		 if(msg.equals("Success"))
		 {
			 //response.getWriter().append("updated Success");
			 response.sendRedirect("GetCompanyList");
			 
		 }
		 else
		 {
			 response.getWriter().append("updated fail");
			 	 
		 }
	}

		
		
		
	}


