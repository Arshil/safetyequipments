package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;

/**
 * Servlet implementation class SubCategoryMasterData
 */
public class SubCategoryMasterData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SubCategoryService subcategoryservice = new SubCategoryServiceImpl(); 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubCategoryMasterData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("do get called");
		System.out.println(Integer.parseInt(request.getParameter("categoryId")));
		List<SubCategory> subcategorylist = subcategoryservice.selectedcategorySubCategory(Integer.parseInt(request.getParameter("categoryId")));
		System.out.println("Size of List = "+subcategorylist.size());
		Gson gson = new Gson();
		gson.toJson(subcategorylist);
		String jsonString = gson.toJson(subcategorylist);
		System.out.println("JSonString : " + jsonString);
		response.getWriter().append(jsonString);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
	}

}
