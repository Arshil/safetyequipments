package com.safetyequipment.servlet;
import java.util.List;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.service.CategoryService;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.CategoryServiceImpl;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;


/**
 * Servlet implementation class SubCategoryListServlet
 */
public class SubCategoryListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SubCategoryService subcategoryservice =new   SubCategoryServiceImpl();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubCategoryListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<SubCategory> subcategorylist  = subcategoryservice.selectedSubCategory();
		request.setAttribute("subcategorylist",subcategorylist);
		RequestDispatcher requestdispatcher = request.getRequestDispatcher("viewsubcategory.jsp");
		requestdispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		
	}

}
