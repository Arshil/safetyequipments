package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.Part;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.Company;
import com.safetyequipment.beans.Product;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.ProductService;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.CompanyServiceImpl;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.ProductServiceImpl;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;

/**
 * Servlet implementation class UpdateProductServlet
 */
public class UpdateProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       ProductService productservice = new ProductServiceImpl();
       GetCategoryService categoryservice = new   GetCategoryServiceImpl();
       SubCategoryService subcategoryservice = new SubCategoryServiceImpl();
       CompanyService companyservice = new  CompanyServiceImpl();
       	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Product product = productservice.selectedproduct(Integer.parseInt(request.getParameter("product_id")));
		List<Category> categorylist = categoryservice.getcategorylist();
		List<Company> companylist = companyservice.selectcompanylist();
		request.setAttribute("product",product);
		request.setAttribute("categorylist",categorylist);
		request.setAttribute("companylist",companylist);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editproduct.jsp");
		requestDispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		//System.out.println("do post called");
		InputStream is = null;
		Product product = new Product();
		product.setProductid(Integer.parseInt(request.getParameter("id")));
		System.out.println("id"+Integer.parseInt(request.getParameter("id")));
		product.setCategoryid(Integer.parseInt(request.getParameter("categoryoption")));
		product.setSubcategoryid(Integer.parseInt(request.getParameter("subcategoryoption")));
		product.setCompanyid(Integer.parseInt(request.getParameter("companyoption")));
		product.setProductprice(Integer.parseInt(request.getParameter("productprice")));
		product.setProductname(request.getParameter("productname"));
		product.setDescription(request.getParameter("productdescription"));
		product.setQuantity(Integer.parseInt(request.getParameter("productquantity")));
		product.setAboutproduct(request.getParameter("aboutproduct"));
		Part image = request.getPart("productimage");
		if (image.getSize() > 0) {
			is = image.getInputStream();
			product.setProductimage(is);
		}
		String message = productservice.editproduct(product);
		if(message.equals("Product Updated Successfully"))
		{
			response.sendRedirect("ProductListServlet");
		}
		else
		{
			System.out.println(message);
		}
		
	}

}
