package com.safetyequipment.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.safetyequipment.beans.Company;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.impl.CompanyServiceImpl;

/**
 * Servlet implementation class AddCompanyServlet
 */
public class AddCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	CompanyService companyService = new CompanyServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		
		Company company = new Company();
		company.setCompanyname(request.getParameter("companyname"));
		company.setCompanycontact(request.getParameter("companycontact"));
		company.setCompanyemail(request.getParameter("companyemailid"));
	
		company.setCompanyaddress(request.getParameter("companyaddress"));
		
		String message = companyService.addcompany(company);
		System.out.println(message);
		if(message.equals("company added"))
		{
			//response.getWriter().append("company added");
			response.sendRedirect("GetCompanyList");
			
		}
		else
		{
			response.getWriter().append("company not added");
	
		}	
	}
}
