package com.safetyequipment.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.User;
import com.safetyequipment.service.UserService;
import com.safetyequipment.service.impl.UserRegisterServiceImpl;

/**
 * Servlet implementation class UserRegServlet
 */
public class UserRegServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	UserService userservice = new UserRegisterServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	
		System.out.println("dopost called");
		User user = new User();

		user.setEmailid(request.getParameter("emailid"));
		System.out.println("Email id :" + user.getEmailid());
		user.setFirstname(request.getParameter("firstname"));
		user.setLastname(request.getParameter("lastname"));
		user.setPassword(request.getParameter("confirmpassword"));
		// String confirmpassword = request.getParameter("confirmpassword");
		String message;
		try {
			message = userservice.userregister(user);
			if (message.equals("Registration success")) {
				System.out.println("Registration succeessful");
				RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
				dispatcher.forward(request, response);
			} else {
				System.out.println("Registration failed.");
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}

	}
}


