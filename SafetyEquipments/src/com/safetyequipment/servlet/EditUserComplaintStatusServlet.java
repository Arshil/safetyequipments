package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Complaint;
import com.safetyequipment.service.ComplaintService;
import com.safetyequipment.service.impl.ComplaintServiceImpl;

/**
 * Servlet implementation class EditUserComplaintStatusServlet
 */
public class EditUserComplaintStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      ComplaintService complaintService = new ComplaintServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserComplaintStatusServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		Complaint complaint = complaintService.editSelectedUserComplaint(Integer.parseInt(request.getParameter("usercomplaint_id")));
		request.setAttribute("complaint",complaint);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("editcomplaintstatus.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		Complaint complaint = new Complaint();
		complaint.setUsercomplaintid(Integer.parseInt(request.getParameter("usercomplaintid")));
		complaint.setActivestatus(request.getParameter("complaintstatus"));
		String message = complaintService.editComplaintStatus(complaint);
		if(message.equals("complaint status updated succeessfully"))
		{
			response.sendRedirect("AllUserComplaints");
		}
		else
		{
			System.out.println(message);
		}
	}

}
