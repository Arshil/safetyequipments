package com.safetyequipment.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.safetyequipment.beans.Category;
import com.safetyequipment.beans.SubCategory;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;

/**
 * Servlet implementation class UpdateSubCategoryServlet
 */
public class UpdateSubCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     SubCategoryService subcategoryservice = new  SubCategoryServiceImpl(); 
     GetCategoryService categoryservice = new   GetCategoryServiceImpl();
     
     /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateSubCategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("id : "+request.getParameter("sub_category_id"));
		SubCategory subCategory = subcategoryservice.selectedsubcategory(Integer.parseInt(request.getParameter("sub_category_id")));
		List<Category> categorylist = categoryservice.getcategorylist();
		request.setAttribute("subcategory",subCategory);
		request.setAttribute("categorylist",categorylist);
		RequestDispatcher requestDispatcher  = request.getRequestDispatcher("editsubcategory.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 //id,categoryoption,subcategoryname,subcategoryimage,subcategorydescription
		InputStream is = null;
		SubCategory subcategory = new SubCategory();
		System.out.println("id :"+ request.getParameter("id"));
		subcategory.setSubcategoryid(Integer.parseInt(request.getParameter("id")));
		
		subcategory.setCategoryid(Integer.parseInt(request.getParameter("categoryoption")));
		subcategory.setSubcategoryname(request.getParameter("subcategoryname"));
		subcategory.setDescription(request.getParameter("subcategorydescription"));
		Part image = request.getPart("subcategoryimage");
		if (image.getSize() > 0) {
			is = image.getInputStream();
			subcategory.setSubcategoryimage(is);
		}
		String message = subcategoryservice.editsubcategory(subcategory);
		if(message.equals("Sub Category Updated Successfully"))
		{
			response.sendRedirect("SubCategoryListServlet");
		}
		else
		{
			System.out.println(message);
		}
	}

}
