package com.safetyequipment.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.MyOrder;
import com.safetyequipment.service.MyOrderService;
import com.safetyequipment.service.impl.MyOrderServiceImpl;

/**
 * Servlet implementation class ViewOrderServlet
 */
public class ViewOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       MyOrderService  myOrderService = new MyOrderServiceImpl(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("step 1 order key : "+request.getParameter("orderkey"));
		MyOrder  myOrder = myOrderService.selectedMyOrder(request.getParameter("orderkey"));
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("viewmyorder.jsp");
		request.setAttribute("myorder",myOrder);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
