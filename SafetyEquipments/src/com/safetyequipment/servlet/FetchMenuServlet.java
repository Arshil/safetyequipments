package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Category;
import com.safetyequipment.service.GetCategoryService;
import com.safetyequipment.service.SubCategoryService;
import com.safetyequipment.service.impl.GetCategoryServiceImpl;
import com.safetyequipment.service.impl.SubCategoryServiceImpl;

/**
 * Servlet implementation class FetchMenuServlet
 */
public class FetchMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	GetCategoryService getCategoryService = new GetCategoryServiceImpl();
	SubCategoryService subCategoryService = new SubCategoryServiceImpl(); 
	/**
     * @see HttpServlet#HttpServlet()
     */
    public FetchMenuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		List<Category> categorylist =	getCategoryService.getcategorylist();
	
		System.out.println("category List size :" + categorylist.size());	
		for (Category category : categorylist) 
		{
			category.setSubcategorylist(subCategoryService.selectedcategorySubCategory(category.getCategoryid()));			
		}
		request.setAttribute("categorylist",categorylist);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("header.jsp");
//		 requestDispatcher.forward(request, response);                     
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	
		
	}

}
