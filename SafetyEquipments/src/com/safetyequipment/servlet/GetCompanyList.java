package com.safetyequipment.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.safetyequipment.beans.Company;
import com.safetyequipment.service.CompanyService;
import com.safetyequipment.service.impl.CompanyServiceImpl;

/**
 * Servlet implementation class GetCompanyList
 */
public class GetCompanyList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	CompanyService companyService = new  CompanyServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCompanyList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		ArrayList<Company> companylist =  (ArrayList<Company>) companyService.selectcompanylist();
		System.out.println("LIST SIZE : " + companylist.size());
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("viewcompany.jsp");
		request.setAttribute("companylist", companylist);
		requestDispatcher.forward(request, response);

	
	
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
